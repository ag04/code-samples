package org.xebialabs.assessment.spaceship.domain.model.spaceship;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("B_CLASS")
public class BClass extends Spaceship {

	public BClass() {
		super(SpaceshipType.B_CLASS);
	}
}
