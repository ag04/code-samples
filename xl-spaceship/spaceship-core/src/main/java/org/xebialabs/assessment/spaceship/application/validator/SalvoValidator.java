package org.xebialabs.assessment.spaceship.application.validator;

import org.xebialabs.assessment.spaceship.application.GameStrategyProvider;
import org.xebialabs.assessment.spaceship.domain.model.game.Game;
import org.xebialabs.assessment.spaceship.domain.shared.GridPosition;

import java.util.Set;

public class SalvoValidator {

	private final GameStrategyProvider gameStrategyProvider;

	public SalvoValidator(GameStrategyProvider gameStrategyProvider) {
		this.gameStrategyProvider = gameStrategyProvider;
	}

	public void validate(Set<GridPosition> salvo, Game game) {
		long expectedSalvoSize = gameStrategyProvider.calculateSalvoSize(game);
		if (salvo.size() != expectedSalvoSize) {
			throw new IllegalArgumentException("Salvo size should be " + expectedSalvoSize);
		}
	}
}
