package org.xebialabs.assessment.spaceship.domain.shared;

import java.io.Serializable;
import java.util.Objects;

public class GridPosition implements Serializable {

	private Integer x;
	private Integer y;

	public static GridPosition create(int x, int y) {
		GridPosition position = new GridPosition();
		position.x = x;
		position.y = y;
		return position;
	}

	public Integer getX() {
		return x;
	}

	public void setX(Integer x) {
		this.x = x;
	}

	public Integer getY() {
		return y;
	}

	public void setY(Integer y) {
		this.y = y;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}

		if (!(o instanceof GridPosition)) {
			return false;
		}

		GridPosition position = (GridPosition) o;

		return Objects.equals(getX(), position.getX()) && Objects.equals(getY(), position.getY());
	}

	@Override
	public int hashCode() {
		return Objects.hash(getX(), getY());
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder();
		sb.append(Integer.toHexString(x).toUpperCase()).append("x").append(Integer.toHexString(y).toUpperCase());
		return sb.toString();
	}
}
