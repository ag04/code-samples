import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Button } from 'react-bootstrap';
import Sidebar from 'react-sidebar';

import GameListComponent from './game-list/game-list.component';
import GameViewComponent from './game-view/game-view.component';
import { initGameList } from '../store/actions/game-list.actions';
import {
  createGame,
  displayGame,
  createFormChanged,
  selectedSalvoChanged,
  shootSelectedSalvo,
  turnAutopilotOn
} from '../store/actions/game.actions';

import './app.css';
import { CreateForm, GameDetails, RuleSet } from '../types';

const sidebarStyle = {
  sidebar: {
    width: 400,
    padding: '20px 10px'
  },
  content: {
    padding: '20px 40px'
  }
};

class AppContainer extends Component {
  static propTypes = {
    games: PropTypes.array,
    error: PropTypes.bool,
    gameDetails: GameDetails,
    createGameErrorMessage: PropTypes.string,
    gameErrorMessage: PropTypes.string,
    ruleSet: RuleSet,
    onCreateGame: PropTypes.func.isRequired,
    onGameSelected: PropTypes.func.isRequired,
    createForm: CreateForm.isRequired,
    onCreateFormChanged: PropTypes.func.isRequired,
    selectedSalvo: PropTypes.array.isRequired,
    onSelectedSalvoChanged: PropTypes.func.isRequired,
    onShootSalvo: PropTypes.func.isRequired,
    onAutopilotClicked: PropTypes.func.isRequired,
    onRefreshButtonClicked: PropTypes.func.isRequired,
    onRefreshListButtonClicked: PropTypes.func.isRequired
  };

  componentDidMount() {
    this.props.onInit();
  }

  state = {
    sidebarDocked: true
  };

  onSetSidebarOpen = () => {
    this.setState({
      sidebarDocked: !this.state.sidebarDocked
    });
  };

  render() {
    const {
      games,
      error,
      gameDetails,
      ruleSet,
      createForm,
      selectedSalvo,
      onCreateGame,
      onGameSelected,
      onCreateFormChanged,
      onSelectedSalvoChanged,
      onShootSalvo,
      onAutopilotClicked,
      onRefreshButtonClicked,
      onRefreshListButtonClicked,
      createGameErrorMessage,
      gameErrorMessage
    } = this.props;
    const { sidebarDocked } = this.state;
    return (
      <Sidebar
        sidebar={
          <GameListComponent
            games={games}
            error={error}
            onGameSelected={onGameSelected}
            onRefreshButtonClicked={onRefreshListButtonClicked}
          />
        }
        docked={sidebarDocked}
        styles={sidebarStyle}
      >
        <Button
          bsStyle="primary"
          className="sidebar-button"
          onClick={() => this.onSetSidebarOpen()}
        >
          {sidebarDocked ? (
            <i className="arrow left" />
          ) : (
            <i className="arrow right" />
          )}
        </Button>
        <GameViewComponent
          gameDetails={gameDetails}
          ruleSet={ruleSet}
          onCreateGame={onCreateGame}
          onCreateFormChanged={onCreateFormChanged}
          createForm={createForm}
          selectedSalvo={selectedSalvo}
          onSelectedSalvoChanged={onSelectedSalvoChanged}
          onShootSalvo={onShootSalvo}
          onAutopilotClicked={onAutopilotClicked}
          onRefreshButtonClicked={onRefreshButtonClicked}
          createGameErrorMessage={createGameErrorMessage}
          gameErrorMessage={gameErrorMessage}
        />
      </Sidebar>
    );
  }
}

const mapStateToProps = state => {
  return {
    games: state.gameList.games,
    error: state.gameList.error,
    gameLocation: state.game.gameLocation,
    gameDetails: state.game.gameDetails,
    ruleSet: state.game.ruleSet,
    createForm: state.game.createForm,
    selectedSalvo: state.game.selectedSalvo,
    createGameErrorMessage: state.game.createGameErrorMessage,
    gameErrorMessage: state.game.gameErrorMessage
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onInit: () => dispatch(initGameList()),
    onCreateGame: game => dispatch(createGame(game)),
    onGameSelected: gameId => dispatch(displayGame(gameId)),
    onCreateFormChanged: createForm => dispatch(createFormChanged(createForm)),
    onSelectedSalvoChanged: selectedSalvo =>
      dispatch(selectedSalvoChanged(selectedSalvo)),
    onShootSalvo: salvo => dispatch(shootSelectedSalvo(salvo)),
    onAutopilotClicked: gameId => dispatch(turnAutopilotOn(gameId)),
    onRefreshButtonClicked: gameId => dispatch(displayGame(gameId)),
    onRefreshListButtonClicked: () => dispatch(initGameList())
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AppContainer);
