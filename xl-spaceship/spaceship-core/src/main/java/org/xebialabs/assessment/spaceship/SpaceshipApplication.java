package org.xebialabs.assessment.spaceship;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.xebialabs.assessment.spaceship.configuration.ApplicationProperties;

@ComponentScan
@EnableAutoConfiguration
@EnableConfigurationProperties({ ApplicationProperties.class })
public class SpaceshipApplication {
	public static void main(String[] args) {
		SpringApplication.run(SpaceshipApplication.class, args);
	}
}
