package org.xebialabs.assessment.spaceship.domain.model.game.grid;

import org.xebialabs.assessment.spaceship.domain.model.spaceship.Rotation;
import org.xebialabs.assessment.spaceship.domain.model.spaceship.Spaceship;
import org.xebialabs.assessment.spaceship.domain.model.spaceship.SpaceshipFactory;
import org.xebialabs.assessment.spaceship.domain.model.spaceship.SpaceshipPositionOutOfBounds;
import org.xebialabs.assessment.spaceship.domain.model.spaceship.SpaceshipType;
import org.xebialabs.assessment.spaceship.domain.shared.GridPosition;

import java.util.HashSet;
import java.util.Set;

public class DummyGridFactory implements GridFactory {

	private final SpaceshipFactory spaceshipFactory;

	public DummyGridFactory(SpaceshipFactory spaceshipFactory) {
		this.spaceshipFactory = spaceshipFactory;
	}

	private static GridPosition wingerPosition = GridPosition.create(0, 0);
	private static GridPosition anglePosition = GridPosition.create(12, 0);
	private static GridPosition aClassPosition = GridPosition.create(12, 13);
	private static GridPosition bClassPosition = GridPosition.create(0, 13);
	private static GridPosition sClassPosition = GridPosition.create(7, 7);

	@Override
	public Grid create() {
		Set<Spaceship> spaceships = new HashSet<>();
		try {
			spaceships.add(createWinger());
			spaceships.add(createAngle());
			spaceships.add(createAClass());
			spaceships.add(createBClass());
			spaceships.add(createSClass());
		}
		catch (SpaceshipPositionOutOfBounds e) {

		}
		return Grid.createGrid(spaceships);
	}

	private Spaceship createSClass() throws SpaceshipPositionOutOfBounds {
		return spaceshipFactory.create(SpaceshipType.S_CLASS, Rotation.STRAIGHT, sClassPosition);
	}

	private Spaceship createBClass() throws SpaceshipPositionOutOfBounds {
		return spaceshipFactory.create(SpaceshipType.B_CLASS, Rotation.STRAIGHT, bClassPosition);

	}

	private Spaceship createAClass() throws SpaceshipPositionOutOfBounds {
		return spaceshipFactory.create(SpaceshipType.A_CLASS, Rotation.STRAIGHT, aClassPosition);

	}

	private Spaceship createAngle() throws SpaceshipPositionOutOfBounds {
		return spaceshipFactory.create(SpaceshipType.ANGLE, Rotation.STRAIGHT, anglePosition);

	}

	private Spaceship createWinger() throws SpaceshipPositionOutOfBounds {
		return spaceshipFactory.create(SpaceshipType.WINGER, Rotation.STRAIGHT, wingerPosition);
	}

}
