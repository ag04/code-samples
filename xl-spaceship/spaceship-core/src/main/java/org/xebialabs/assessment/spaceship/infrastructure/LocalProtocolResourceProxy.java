package org.xebialabs.assessment.spaceship.infrastructure;

import com.google.common.base.Joiner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.xebialabs.assessment.spaceship.application.GameFinishedException;
import org.xebialabs.assessment.spaceship.application.GameInstanceNotFound;
import org.xebialabs.assessment.spaceship.application.GameService;
import org.xebialabs.assessment.spaceship.application.IllegalMoveByOpponentException;
import org.xebialabs.assessment.spaceship.application.implementation.dto.GameDTO;
import org.xebialabs.assessment.spaceship.application.implementation.dto.SalvoDTO;
import org.xebialabs.assessment.spaceship.application.mapper.GameDTOMapper;
import org.xebialabs.assessment.spaceship.application.mapper.SalvoMapper;
import org.xebialabs.assessment.spaceship.domain.shared.GridPosition;
import org.xebialabs.assessment.spaceship.infrastructure.command.CreateGameCommand;
import org.xebialabs.assessment.spaceship.infrastructure.command.CreateGameResult;
import org.xebialabs.assessment.spaceship.infrastructure.command.FireSalvoCommand;
import org.xebialabs.assessment.spaceship.infrastructure.command.FireSalvoResult;

import java.util.Set;

@Component
public class LocalProtocolResourceProxy implements ProtocolResourceProxy {
	private static final Logger LOGGER = LoggerFactory.getLogger(LocalProtocolResourceProxy.class);

	private final GameDTOMapper gameDTOMapper;
	private final SalvoMapper salvoMapper;

	private final GameService gameService;

	public LocalProtocolResourceProxy(GameDTOMapper gameDTOMapper, SalvoMapper salvoMapper, GameService gameService) {

		this.gameDTOMapper = gameDTOMapper;
		this.salvoMapper = salvoMapper;
		this.gameService = gameService;
	}

	@Override
	public CreateGameResult createGame(CreateGameCommand command) {
		LOGGER.debug("Received create game command: {}", command);
		GameDTO gameDTO = gameDTOMapper.createGameCommandToGameDTO(command);
		gameDTO = gameService.create(gameDTO);
		CreateGameResult createGameResult = gameDTOMapper.gameDTOToCreateGameResult(gameDTO);
		LOGGER.info("Processed create game command with result: {}", createGameResult);
		return createGameResult;
	}

	@Override
	public FireSalvoResult fireSalvo(String gameId, FireSalvoCommand command) throws IllegalMoveByOpponentException, GameInstanceNotFound {
		LOGGER.debug("Received Opponent salvo shot on gameId: {}: [{}]", gameId, Joiner.on(", ").join(command.getSalvo()));
		Set<GridPosition> positions = salvoMapper.fireSalvoCommandToGridPositions(command);
		FireSalvoResult result;
		try {
			SalvoDTO salvoDTO = gameService.fireOpponentSalvo(gameId, positions);
			result = salvoMapper.salvoDtoToFireSalvoResult(salvoDTO);

		}
		catch (GameFinishedException e) {
			SalvoDTO salvoDTO = e.getSalvoResponse();
			result = salvoMapper.salvoDtoToFireSalvoResult(salvoDTO);
			result.setFinished(true);
		}

		LOGGER.info("Processed Opponent salvo shot on gameId: {}: [{}, playerTurn: {}, win: {}]",
				gameId, Joiner.on(", ").withKeyValueSeparator(": ").join(result.getSalvoResult()),
				result.getGameState().getPlayerTurn(), result.getGameState().getWon());
		return result;
	}

}
