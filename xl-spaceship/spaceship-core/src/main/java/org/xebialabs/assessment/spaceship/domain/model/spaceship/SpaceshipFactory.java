package org.xebialabs.assessment.spaceship.domain.model.spaceship;

import org.xebialabs.assessment.spaceship.domain.shared.GridPosition;

import java.util.Set;

public interface SpaceshipFactory {

	Spaceship create(SpaceshipType type, Rotation rotation, GridPosition gridPosition) throws SpaceshipPositionOutOfBounds;

	Set<GridPosition> calculateSpaceshipCoordinates(Spaceship spaceship);

}
