import React from 'react';
import PropTypes from 'prop-types';
import { Row, Col } from 'react-bootstrap';
import GameGrid from './game-grid';

const buildGridViewRows = props => {
  return (
    <>
      <Row>
        <Col lg={12}>
          <h4 className="board-title">
            {props.gridTitle}
            {props.buttonGroup}
          </h4>
        </Col>
      </Row>
      <Row>
        <Col lg={12}>
          <GameGrid
            gridData={props.gridData}
            selectedSalvo={props.selectedSalvo}
            cellClickedCallback={props.cellClickedCallback}
          />
        </Col>
      </Row>
    </>
  );
};

buildGridViewRows.propTypes = {
  gridTitle: PropTypes.string.isRequired,
  gridData: PropTypes.array.isRequired,
  selectedSalvo: PropTypes.array,
  cellClickedCallback: PropTypes.func
};

export default buildGridViewRows;
