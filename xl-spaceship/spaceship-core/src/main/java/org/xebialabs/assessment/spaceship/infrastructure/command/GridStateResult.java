package org.xebialabs.assessment.spaceship.infrastructure.command;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotNull;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class GridStateResult {

	@NotNull
	@JsonProperty("self")
	private UserGrid slefUserGrid;

	@NotNull
	@JsonProperty("opponent")
	private UserGrid opponentUserGrid;

	@NotNull
	@JsonProperty("game")
	private GameState gameState;

	public UserGrid getSlefUserGrid() {
		return slefUserGrid;
	}

	public void setSlefUserGrid(UserGrid slefUserGrid) {
		this.slefUserGrid = slefUserGrid;
	}

	public UserGrid getOpponentUserGrid() {
		return opponentUserGrid;
	}

	public void setOpponentUserGrid(UserGrid opponentUserGrid) {
		this.opponentUserGrid = opponentUserGrid;
	}

	public GameState getGameState() {
		return gameState;
	}

	public void setGameState(GameState gameState) {
		this.gameState = gameState;
	}
}
