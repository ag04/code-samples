import axios from '../../axios-default';

import { BASE_USER_RESOURCE_URI } from '../../utils/constants';

export const getGameList = () => {
  return axios.get(BASE_USER_RESOURCE_URI).then(response => response.data);
};
