package org.xebialabs.assessment.spaceship.application.implementation.dto;

import org.xebialabs.assessment.spaceship.domain.model.game.Game;

public class FireResultDTO {

	private Game game;
	private SalvoDTO salvoDTO;
	private boolean finished;

	public static FireResultDTO create(Game game, SalvoDTO salvoDTO, boolean finished) {
		FireResultDTO fireResultDTO = new FireResultDTO();
		fireResultDTO.game = game;
		fireResultDTO.salvoDTO = salvoDTO;
		fireResultDTO.finished = finished;
		return fireResultDTO;
	}

	public Game getGame() {
		return game;
	}

	public SalvoDTO getSalvoDTO() {
		return salvoDTO;
	}

	public boolean isFinished() {
		return finished;
	}
}
