#!/bin/bash

PWD=`/bin/pwd`
[ -z "$JAVA_HOME" ] && JAVA_COMMAND=`which java`

if env | grep -q ^JAVA_HOME=
then
  echo JAVA_HOME is existing..
  $JAVA_COMMAND=$JAVA_HOME/bin/java
  echo Using java $JAVA_COMMAND
else
  echo WARNING: JAVA_HOME is non-existing using default $JAVA_COMMAND
fi

$JAVA_COMMAND -jar $PWD/spaceship-core/build/libs/spaceship-core-1.0-SNAPSHOT.jar