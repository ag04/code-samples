package org.xebialabs.assessment.spaceship.interfaces.user;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;
import org.xebialabs.assessment.spaceship.application.GameInstanceNotFound;
import org.xebialabs.assessment.spaceship.application.IllegalMoveByOpponentException;
import org.xebialabs.assessment.spaceship.infrastructure.GameProtocolError;
import org.xebialabs.assessment.spaceship.infrastructure.UserResourceProxy;
import org.xebialabs.assessment.spaceship.infrastructure.command.CreateUserGameCommand;
import org.xebialabs.assessment.spaceship.infrastructure.command.FireSalvoCommand;
import org.xebialabs.assessment.spaceship.infrastructure.command.FireSalvoResult;
import org.xebialabs.assessment.spaceship.infrastructure.command.GridStateResult;
import org.xebialabs.assessment.spaceship.infrastructure.command.RuleSetResult;
import org.xebialabs.assessment.spaceship.util.ApplicationConstants;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@RestController
@RequestMapping(ApplicationConstants.PATH_PREFIX + "/user/game")
public class UserResource {


	private static final Logger LOGGER = LoggerFactory.getLogger(UserResource.class);

	private final UserResourceProxy userResourceProxy;

	public UserResource(UserResourceProxy userResourceProxy) {
		this.userResourceProxy = userResourceProxy;
	}

	@PutMapping("/{gameId}/fire")
	public ResponseEntity<FireSalvoResult> fireSalvo(
			@Valid @RequestBody FireSalvoCommand command, @NotNull @PathVariable String gameId) throws IllegalMoveByOpponentException, GameInstanceNotFound {
		FireSalvoResult result = userResourceProxy.fireSalvo(gameId, command);
		if (result.isFinished()) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(result);
		}
		return ResponseEntity.ok(result);
	}

	@GetMapping
	public ResponseEntity<List<String>> gameList() {
		List<String> gameIds = userResourceProxy.getAllGamesIds();
		return ResponseEntity.ok(gameIds);
	}

	@GetMapping("/{gameId}/rule-set")
	public ResponseEntity<RuleSetResult> getGameRuleSet(@NotNull @PathVariable String gameId) throws GameInstanceNotFound {
		RuleSetResult result = userResourceProxy.getGameRuleSet(gameId);
		return ResponseEntity.ok(result);
	}

	@GetMapping("/{gameId}")
	public ResponseEntity<GridStateResult> getGridState(@NotNull @PathVariable String gameId) throws GameInstanceNotFound {

		GridStateResult result = userResourceProxy.getGridState(gameId);
		return ResponseEntity.ok(result);
	}

	@PostMapping("/{gameId}/auto")
	public ResponseEntity<Void> goAuto(@NotNull @PathVariable String gameId) throws GameInstanceNotFound {

		userResourceProxy.startAutopilot(gameId);
		return ResponseEntity.ok().build();
	}

	@PostMapping("/new")
	public ResponseEntity<String> createGame(@NotNull @RequestBody CreateUserGameCommand command, UriComponentsBuilder builder) throws GameProtocolError {

		String gameId = userResourceProxy.createGame(command);

		UriComponents uriComponents = builder.path(ApplicationConstants.PATH_PREFIX + "/user/game/{uuid}").buildAndExpand(gameId);
		return ResponseEntity
				.status(HttpStatus.SEE_OTHER)
				.contentType(MediaType.TEXT_HTML)
				.header("Location", uriComponents.getPath())
				.body("A new game has been created at " + uriComponents.getPath());
	}

	@ExceptionHandler({ GameProtocolError.class })
	public ResponseEntity<String> handleException(GameProtocolError e) {
		LOGGER.error("Catching exception:", e.getMessage());
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
	}

	@ExceptionHandler({ GameInstanceNotFound.class })
	public ResponseEntity<String> handleException(GameInstanceNotFound e) {
		LOGGER.error("Catching exception:", e.getMessage());
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Game instance not found");
	}

	@ExceptionHandler({ IllegalMoveByOpponentException.class })
	public ResponseEntity<String> handleException(IllegalMoveByOpponentException e) {
		LOGGER.error("Catching exception:", e.getMessage());
		return ResponseEntity.status(HttpStatus.CONFLICT).body("Not a players turn");
	}

	@ExceptionHandler({ IllegalArgumentException.class })
	public ResponseEntity<String> handleException(IllegalArgumentException e) {
		LOGGER.error("Catching exception:", e.getMessage());
		return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
	}

	@ExceptionHandler({ IllegalStateException.class })
	public ResponseEntity<String> handleException(IllegalStateException e) {
		LOGGER.error("Unexpected exception:", e.getMessage());
		return ResponseEntity.status(HttpStatus.BAD_GATEWAY).body(e.getMessage());
	}

	@ExceptionHandler({ Exception.class })
	public ResponseEntity<String> handleException(Exception e) {
		LOGGER.error("Unexpected exception:", e);
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error managing game protocol");
	}
}
