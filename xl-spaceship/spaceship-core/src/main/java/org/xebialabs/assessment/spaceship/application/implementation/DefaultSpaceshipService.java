package org.xebialabs.assessment.spaceship.application.implementation;

import org.springframework.stereotype.Component;
import org.xebialabs.assessment.spaceship.application.SpaceshipService;
import org.xebialabs.assessment.spaceship.domain.model.spaceship.Spaceship;
import org.xebialabs.assessment.spaceship.domain.model.spaceship.SpaceshipFactory;
import org.xebialabs.assessment.spaceship.domain.shared.GridPosition;

import java.util.Set;

@Component
public class DefaultSpaceshipService implements SpaceshipService {

	private final SpaceshipFactory spaceshipFactory;

	public DefaultSpaceshipService(SpaceshipFactory spaceshipFactory) {
		this.spaceshipFactory = spaceshipFactory;
	}

	@Override
	public Set<GridPosition> getSpaceshipFootprint(Spaceship spaceship) {
		return spaceshipFactory.calculateSpaceshipCoordinates(spaceship);
	}
}
