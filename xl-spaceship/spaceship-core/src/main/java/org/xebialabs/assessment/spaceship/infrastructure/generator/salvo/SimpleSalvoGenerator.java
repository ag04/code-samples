package org.xebialabs.assessment.spaceship.infrastructure.generator.salvo;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;
import org.xebialabs.assessment.spaceship.application.GameStrategyProvider;
import org.xebialabs.assessment.spaceship.domain.shared.GridPosition;
import org.xebialabs.assessment.spaceship.infrastructure.generator.parameter.ParametersGenerator;

import java.util.Map;
import java.util.Set;

@Component
@ConditionalOnProperty(name = "spaceship.autopilot.type", havingValue = "simple", matchIfMissing = true)
public class SimpleSalvoGenerator extends AbstractSalvoGenerator {

	private static final int ALGORITHM_THRESHOLD = 190;

	public SimpleSalvoGenerator(GameStrategyProvider gameStrategyProvider, ParametersGenerator parametersGenerator) {
		super(gameStrategyProvider, parametersGenerator);
	}

	@Override
	protected GridPosition nextShotPosition(Map<String, String> shotResults, Set<GridPosition> currentSalvo, GridPosition lastHit) {
		if (shotResults.keySet().size() > ALGORITHM_THRESHOLD) {
			return getSequentialGridPosition(shotResults, currentSalvo);
		}
		GridPosition newShot;
		do {
			newShot = parametersGenerator.getPosition();
		} while (shotResults.get(newShot.toString()) != null || currentSalvo.contains(newShot));
		return newShot;
	}
}
