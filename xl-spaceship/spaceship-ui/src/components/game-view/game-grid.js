import React from 'react';
import PropTypes from 'prop-types';
import { Table } from 'react-bootstrap';

const resolveCellClass = (character, alreadySelectedMark) => {
  if (alreadySelectedMark !== -1) {
    return 'badge-selected';
  }
  switch (character) {
    case 'X':
      return 'badge-danger';
    case '.':
    default:
      return 'badge-info';
    case '-':
      return 'badge-secondary';
    case '*':
      return 'badge-success';
  }
};

const createGridCell = (
  data,
  selectedFields,
  rowIndex,
  columnIndex,
  onCellClicked
) => {
  const alreadySelectedMark = selectedFields
    ? selectedFields.findIndex(
        field => field.x === rowIndex && field.y === columnIndex
      )
    : -1;
  const renderContent =
    alreadySelectedMark === -1 ? <span>&nbsp;</span> : <span>&#x2718;</span>;
  return (
    <td
      key={`${rowIndex} + '-' + ${columnIndex}`}
      className={`badge badge-pill ${resolveCellClass(
        data,
        alreadySelectedMark
      )}`}
      onClick={() => onCellClicked(rowIndex, columnIndex, data)}
    >
      {renderContent}
    </td>
  );
};

const renderTableRows = (gridData, selectedFields, onCellClicked) => {
  const rowList = gridData.map((rowString, rowIndex) => {
    const rowList = rowString.split('');
    const row = rowList.map((data, columnIndex) =>
      createGridCell(data, selectedFields, rowIndex, columnIndex, onCellClicked)
    );
    return <tr key={rowIndex}>{row}</tr>;
  });
  return rowList;
};

const buildGrid = props => {
  return (
    <Table className="table-board">
      <tbody>
        {renderTableRows(
          props.gridData,
          props.selectedSalvo,
          props.cellClickedCallback
        )}
      </tbody>
    </Table>
  );
};

buildGrid.propTypes = {
  selectedSalvo: PropTypes.array,
  gridData: PropTypes.array,
  cellClickedCallback: PropTypes.func
};

export default buildGrid;
