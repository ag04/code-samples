package org.xebialabs.assessment.spaceship.application.validator;

import org.xebialabs.assessment.spaceship.application.IllegalMoveByOpponentException;
import org.xebialabs.assessment.spaceship.domain.model.game.Game;


public class GameStateValidator {

	public void validate(Game game) throws IllegalMoveByOpponentException {
		if (game.isMyTurn()) {
			throw new IllegalMoveByOpponentException();
		}
	}

}
