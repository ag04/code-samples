package org.xebialabs.assessment.spaceship.infrastructure.command;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.List;

import javax.validation.constraints.NotNull;

public class FireSalvoCommand {

	@JsonIgnore
	private String gameId;

	@NotNull
	private List<String> salvo;

	public String getGameId() {
		return gameId;
	}

	public void setGameId(String gameId) {
		this.gameId = gameId;
	}

	public List<String> getSalvo() {
		return salvo;
	}

	public void setSalvo(List<String> salvo) {
		this.salvo = salvo;
	}
}
