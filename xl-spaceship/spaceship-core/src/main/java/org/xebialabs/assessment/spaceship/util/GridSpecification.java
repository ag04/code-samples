package org.xebialabs.assessment.spaceship.util;

public final class GridSpecification {
	public static final int GRID_HEIGHT = 16;
	public static final int GRID_WIDTH = 16;

	private static final char[][] EMPTY_GRID;

	private GridSpecification() { }

	static {
		String row = "................";
		EMPTY_GRID = new char[16][16];
		for (int i = 0; i < 16; i++) {
			EMPTY_GRID[i] = row.toCharArray();
		}
	}

	public static char[][] getEmptyGridRows() {
		char[][] copy = EMPTY_GRID.clone();
		for (int i = 0; i < copy.length; i++) {
			copy[i] = copy[i].clone();
		}
		return copy.clone();
	}

}
