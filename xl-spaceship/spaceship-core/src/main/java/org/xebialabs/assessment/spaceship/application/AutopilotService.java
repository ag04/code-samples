package org.xebialabs.assessment.spaceship.application;

public interface AutopilotService {

	void startAutopilot(String gameId) throws GameInstanceNotFound;

	void triggerAutopilotSalvo(String gameId) throws GameInstanceNotFound;
}
