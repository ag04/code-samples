package org.xebialabs.assessment.spaceship.domain.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.xebialabs.assessment.spaceship.domain.model.game.grid.Grid;

@SuppressWarnings("unused")
@Repository
public interface GridRepository extends CrudRepository<Grid, Long> {

}
