package org.xebialabs.assessment.spaceship.domain.model.game.grid;

import com.google.common.collect.Sets;

import org.xebialabs.assessment.spaceship.domain.model.spaceship.Spaceship;
import org.xebialabs.assessment.spaceship.domain.model.spaceship.SpaceshipFactory;
import org.xebialabs.assessment.spaceship.domain.model.spaceship.SpaceshipPositionOutOfBounds;
import org.xebialabs.assessment.spaceship.domain.model.spaceship.SpaceshipType;
import org.xebialabs.assessment.spaceship.domain.shared.GridPosition;
import org.xebialabs.assessment.spaceship.infrastructure.generator.parameter.ParametersGenerator;

import java.util.HashSet;
import java.util.Set;

public class RandomSetGridFactory implements GridFactory {

	private final SpaceshipFactory spaceshipFactory;
	private final ParametersGenerator parametersGenerator;

	public RandomSetGridFactory(SpaceshipFactory spaceshipFactory, ParametersGenerator parametersGenerator) {
		this.spaceshipFactory = spaceshipFactory;
		this.parametersGenerator = parametersGenerator;
	}

	@Override
	public Grid create() {
		Set<Spaceship> spaceships = new HashSet<>();
		Set<GridPosition> positions = new HashSet<>();

		addSpaceShip(spaceships, positions, SpaceshipType.B_CLASS);
		addSpaceShip(spaceships, positions, SpaceshipType.S_CLASS);
		addSpaceShip(spaceships, positions, SpaceshipType.WINGER);
		addSpaceShip(spaceships, positions, SpaceshipType.A_CLASS);
		addSpaceShip(spaceships, positions, SpaceshipType.ANGLE);

		return Grid.createGrid(spaceships);
	}

	private void addSpaceShip(Set<Spaceship> spaceships, Set<GridPosition> positions, SpaceshipType type) {
		Spaceship spaceship;
		do {
			try {
				spaceship = spaceshipFactory.create(type, parametersGenerator.getRotation(), parametersGenerator.getPosition());
			}
			catch (SpaceshipPositionOutOfBounds ex) {
				spaceship = null;
			}
		}
		while (spaceship == null || !addSpaceship(positions, spaceship));
		spaceships.add(spaceship);
	}

	private boolean addSpaceship(Set<GridPosition> spaceships, Spaceship spaceship) {
		Set<GridPosition> spaceshipFootprint = spaceshipFactory.calculateSpaceshipCoordinates(spaceship);
		if (Sets.intersection(spaceships, spaceshipFootprint).isEmpty()) {
			spaceships.addAll(spaceshipFootprint);
			return true;
		}
		return false;
	}


}
