import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { applyMiddleware, combineReducers, createStore } from 'redux';
import 'babel-polyfill';
import createSagaMiddleware from 'redux-saga';

import AppComponent from './components/app.container';

import './styles/bootstrap.min.css';
import './styles/index.css';
import gameListReducer from './store/reducers/game-list.reducer';
import gameReducer from './store/reducers/game.reducer';
import { watchGameAction, watchGameList } from './store/sagas';

const reducers = combineReducers({
  game: gameReducer,
  gameList: gameListReducer
});

const sagaMiddleware = createSagaMiddleware();

const store = createStore(reducers, applyMiddleware(sagaMiddleware));

sagaMiddleware.run(watchGameList);
sagaMiddleware.run(watchGameAction);

ReactDOM.render(
  <Provider store={store}>
    <AppComponent />
  </Provider>,
  document.getElementById('root')
);
