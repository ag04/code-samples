import React from 'react';
import { mount } from 'enzyme';
import {
  Alert,
  Button,
  ListGroup,
  ListGroupItem,
  PageHeader
} from 'react-bootstrap';
import '../../setupTests';

import * as constants from '../../testConstants';
import GameListComponent from '../../../../../components/game-list/game-list.component';

describe('Game List component without error', () => {
  const gameList = constants.GAME_LIST;
  const error = false;
  const onGameSelected = jest.fn();
  const onRefreshListButtonClicked = jest.fn();

  const gameListComponent = mount(
    <GameListComponent
      games={gameList}
      error={error}
      onGameSelected={onGameSelected}
      onRefreshButtonClicked={onRefreshListButtonClicked}
    />
  );

  let header;
  it('it should have header', () => {
    header = gameListComponent.find(PageHeader);
    expect(header.exists()).toBeTruthy();
  });
  it('it should have refresh button', () => {
    const button = header.find(Button);
    expect(button.exists()).toBeTruthy();
  });

  let list;
  it('it should have list group', () => {
    list = gameListComponent.find(ListGroup);
    expect(header.exists()).toBeTruthy();
  });
  it('it should have list items', () => {
    const listItem = list.find(ListGroupItem);
    expect(listItem.exists()).toBeTruthy();
  });
});

describe('Game List component with empty list', () => {
  const gameList = null;
  const error = false;
  const onGameSelected = jest.fn();
  const onRefreshListButtonClicked = jest.fn();

  const gameListComponent = mount(
    <GameListComponent
      games={gameList}
      error={error}
      onGameSelected={onGameSelected}
      onRefreshButtonClicked={onRefreshListButtonClicked}
    />
  );

  it('it should have empty list alert', () => {
    const header = gameListComponent.find(Alert);
    expect(header.exists()).toBeTruthy();
  });
});

describe('Game List component with error', () => {
  const gameList = null;
  const error = true;
  const onGameSelected = jest.fn();
  const onRefreshListButtonClicked = jest.fn();

  const gameListComponent = mount(
    <GameListComponent
      games={gameList}
      error={error}
      onGameSelected={onGameSelected}
      onRefreshButtonClicked={onRefreshListButtonClicked}
    />
  );

  it('it should have error alert', () => {
    const header = gameListComponent.find(Alert);
    expect(header.exists()).toBeTruthy();
  });
});
