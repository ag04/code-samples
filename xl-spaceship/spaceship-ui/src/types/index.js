import PropTypes from 'prop-types';

export const GameDetails = PropTypes.shape({
  self: PropTypes.shape({
    user_id: PropTypes.string.isRequired,
    board: PropTypes.array.isRequired
  }),
  opponent: PropTypes.shape({
    user_id: PropTypes.string.isRequired,
    board: PropTypes.array.isRequired
  }),
  game: PropTypes.shape({
    won: PropTypes.string,
    player_turn: PropTypes.string
  })
});

export const RuleSet = PropTypes.shape({
  gameId: PropTypes.string.isRequired,
  rules: PropTypes.string.isRequired,
  salvoSize: PropTypes.number.isRequired,
  autopilotOn: PropTypes.bool.isRequired
});

export const CreateForm = PropTypes.shape({
  hostname: PropTypes.string,
  port: PropTypes.number,
  rule: PropTypes.string
});
