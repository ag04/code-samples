import * as actionTypes from '../actions/actionTypes';

const initialState = {
  gameLocation: null,
  gameDetails: null,
  createForm: {
    hostname: null,
    port: null,
    rule: null
  },
  selectedSalvo: [],
  createGameErrorMessage: null,
  gameErrorMessage: null
};

const game = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.SET_GAME_LOCATION:
      return {
        ...state,
        gameLocation: action.gameLocation
      };
    case actionTypes.CREATED_GAME:
      return {
        ...state,
        gameLocation: action.gameLocation
      };
    case actionTypes.DISPLAYING_GAME:
      return {
        ...state,
        gameDetails: action.gameDetails,
        ruleSet: action.ruleSet,
        selectedSalvo: [],
        createGameErrorMessage: null,
        gameErrorMessage: null
      };
    case actionTypes.CREATE_FORM_CHANGED:
      return {
        ...state,
        createForm: action.createForm
      };
    case actionTypes.SELECTED_SALVO_CHANGED:
      return {
        ...state,
        selectedSalvo: action.selectedSalvo
      };
    case actionTypes.CREATE_GAME_FAILED:
      return {
        ...state,
        createGameErrorMessage: action.errorMessage
      };
    case actionTypes.CREATE_GAME_FAILED_ON_REMOTE:
      return {
        ...state,
        createGameErrorMessage: action.errorMessage
      };
    case actionTypes.DISPLAY_GAME_FAILED:
      return {
        ...state,
        gameErrorMessage: action.errorMessage
      };
    default:
      return state;
  }
};

export default game;
