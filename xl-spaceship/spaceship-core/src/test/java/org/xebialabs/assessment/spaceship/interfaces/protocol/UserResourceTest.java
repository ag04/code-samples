package org.xebialabs.assessment.spaceship.interfaces.protocol;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.xebialabs.assessment.spaceship.application.AutopilotService;
import org.xebialabs.assessment.spaceship.application.GameFinishedException;
import org.xebialabs.assessment.spaceship.application.GameInstanceNotFound;
import org.xebialabs.assessment.spaceship.application.GameService;
import org.xebialabs.assessment.spaceship.application.IllegalMoveByOpponentException;
import org.xebialabs.assessment.spaceship.application.implementation.dto.GameDTO;
import org.xebialabs.assessment.spaceship.application.implementation.dto.SalvoDTO;
import org.xebialabs.assessment.spaceship.application.mapper.GameDTOMapper;
import org.xebialabs.assessment.spaceship.application.mapper.GameDTOMapperImpl;
import org.xebialabs.assessment.spaceship.application.mapper.GridStateMapper;
import org.xebialabs.assessment.spaceship.application.mapper.SalvoMapper;
import org.xebialabs.assessment.spaceship.domain.model.spaceship.DefaultSpaceshipFactory;
import org.xebialabs.assessment.spaceship.domain.model.spaceship.Rotation;
import org.xebialabs.assessment.spaceship.domain.model.spaceship.SpaceshipFactory;
import org.xebialabs.assessment.spaceship.domain.model.spaceship.SpaceshipPositionOutOfBounds;
import org.xebialabs.assessment.spaceship.domain.model.spaceship.SpaceshipType;
import org.xebialabs.assessment.spaceship.domain.shared.GridPosition;
import org.xebialabs.assessment.spaceship.infrastructure.GameProtocolError;
import org.xebialabs.assessment.spaceship.infrastructure.LocalUserResourceProxy;
import org.xebialabs.assessment.spaceship.infrastructure.UserResourceProxy;
import org.xebialabs.assessment.spaceship.interfaces.user.UserResource;
import org.xebialabs.assessment.spaceship.util.ApplicationConstants;

import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.BDDMockito.doNothing;
import static org.mockito.BDDMockito.doThrow;
import static org.mockito.BDDMockito.when;
import static org.mockito.Mockito.mock;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { GameDTOMapperImpl.class, SalvoMapper.class })
@WebAppConfiguration
public class UserResourceTest {
	public static final MediaType APPLICATION_JSON_UTF8 = new MediaType(MediaType.APPLICATION_JSON.getType(), MediaType.APPLICATION_JSON.getSubtype(),
			Charset.forName("utf8"));

	MockMvc mockMvc;

	GameService gameService;
	AutopilotService autopilotService;

	@Autowired
	GameDTOMapper gameDTOMapper;

	@Autowired
	SalvoMapper salvoMapper;

	UserResourceProxy userResourceProxy;
	UserResource userResource;

	GridStateMapper gridStateMapper;

	@Before
	public void setUp() throws Exception {

		gameService = mock(GameService.class);
		autopilotService = mock(AutopilotService.class);
		gridStateMapper = new GridStateMapper();
		userResourceProxy = new LocalUserResourceProxy(salvoMapper, gameDTOMapper, gameService, autopilotService, gridStateMapper);

		userResource = new UserResource(userResourceProxy);
		this.mockMvc = MockMvcBuilders.standaloneSetup(userResource).build();
	}

	@Test
	public void createUserGame_created() throws Exception, GameProtocolError {
		when(gameService.createUserGame(any(GameDTO.class))).thenReturn("this-is-game-id");
		MvcResult result = mockMvc.perform(post(ApplicationConstants.PATH_PREFIX + "/user/game/new").contentType(APPLICATION_JSON_UTF8).content(
				"{\n" + "\"spaceship_protocol\": {\n" + "\"hostname\": \"10.10.0.2\",\n" + "\"port\": 9000\n" + "},\n"
						+ "\"rules\": \"super-charge\"\n" + "}")).andExpect(status().isSeeOther()).andReturn();

		Assert.assertEquals("A new game has been created at /xl-spaceship/user/game/this-is-game-id", result.getResponse().getContentAsString());
		Assert.assertEquals(303, result.getResponse().getStatus());
		Assert.assertEquals("/xl-spaceship/user/game/this-is-game-id", result.getResponse().getHeader("Location"));
		Assert.assertEquals(MediaType.TEXT_HTML.toString(), result.getResponse().getContentType());
	}

	@Test
	public void createUserGame_communicationError() throws Exception, GameProtocolError {
		when(gameService.createUserGame(any(GameDTO.class))).thenThrow(GameProtocolError.class);
		MvcResult result = mockMvc.perform(post(ApplicationConstants.PATH_PREFIX + "/user/game/new").contentType(APPLICATION_JSON_UTF8).content(
				"{\n" + "\"spaceship_protocol\": {\n" + "\"hostname\": \"10.10.0.2\",\n" + "\"port\": 9000\n" + "},\n"
						+ "\"rules\": \"super-charge\"\n" + "}")).andExpect(status().isBadRequest()).andReturn();
	}

	@Test
	public void fireSalvo_aSuccessShot() throws Exception {
		SalvoDTO salvoDTO = new SalvoDTO();
		Map<String, String> shotResults = new HashMap<>();
		shotResults.put("0x0", "hit");
		salvoDTO.setShotResults(shotResults);
		salvoDTO.setPlayerTurn("zeko");
		when(gameService.fireUserSalvo(any(String.class), any(Set.class))).thenReturn(salvoDTO);
		mockMvc.perform(put(ApplicationConstants.PATH_PREFIX + "/user/game/game-id-goes-here/fire").contentType(APPLICATION_JSON_UTF8)
				.content("{\"salvo\": [\"0x0\"]}")).andExpect(status().isOk()).andExpect(content().contentType(APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("$.salvo.0x0", is("hit"))).andExpect(jsonPath("$.game.player_turn", is("zeko")));
	}

	@Test
	public void fireSalvo_gameFinished() throws Exception {
		SalvoDTO salvoDTO = new SalvoDTO();
		Map<String, String> shotResults = new HashMap<>();
		shotResults.put("0x0", "miss");
		salvoDTO.setShotResults(shotResults);
		salvoDTO.setWon("zeko");
		GameFinishedException exception = new GameFinishedException(salvoDTO);
		when(gameService.fireUserSalvo(any(String.class), any(Set.class))).thenThrow(exception);
		mockMvc.perform(put(ApplicationConstants.PATH_PREFIX + "/user/game/game-id-goes-here/fire").contentType(APPLICATION_JSON_UTF8)
				.content("{\"salvo\": [\"0x0\"]}")).andExpect(status().isNotFound()).andExpect(content().contentType(APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("$.salvo.0x0", is("miss"))).andExpect(jsonPath("$.game.won", is("zeko")));
	}

	@Test
	public void fireSalvo_gameInstanceNotFound() throws Exception {
		GameInstanceNotFound exception = new GameInstanceNotFound();

		when(gameService.fireUserSalvo(any(String.class), any(Set.class))).thenThrow(exception);
		mockMvc.perform(put(ApplicationConstants.PATH_PREFIX + "/user/game/game-id-goes-here/fire").contentType(APPLICATION_JSON_UTF8)
				.content("{\"salvo\": [\"0x0\"]}")).andExpect(status().isInternalServerError());
	}

	@Test
	public void fireSalvo_illegalMoveByOpponentException() throws Exception {
		IllegalMoveByOpponentException exception = new IllegalMoveByOpponentException();

		when(gameService.fireUserSalvo(any(String.class), any(Set.class))).thenThrow(exception);
		mockMvc.perform(put(ApplicationConstants.PATH_PREFIX + "/user/game/game-id-goes-here/fire").contentType(APPLICATION_JSON_UTF8)
				.content("{\"salvo\": [\"0x0\"]}")).andExpect(status().isConflict());
	}

	@Test
	public void fireSalvo_illegalArgumentException() throws Exception {
		IllegalArgumentException exception = new IllegalArgumentException();

		when(gameService.fireUserSalvo(any(String.class), any(Set.class))).thenThrow(exception);
		mockMvc.perform(put(ApplicationConstants.PATH_PREFIX + "/user/game/game-id-goes-here/fire").contentType(APPLICATION_JSON_UTF8)
				.content("{\"salvo\": [\"0x0\"]}")).andExpect(status().isExpectationFailed());
	}

	@Test
	public void goAuto_success() throws Exception {
		doNothing().when(autopilotService).startAutopilot(isA(String.class));
		mockMvc.perform(post(ApplicationConstants.PATH_PREFIX + "/user/game/game-id-goes-here/auto")).andExpect(status().isOk());
	}

	@Test
	public void goAuto_gameInstanceNotFound() throws Exception {
		doThrow(GameInstanceNotFound.class).when(autopilotService).startAutopilot(isA(String.class));
		mockMvc.perform(post(ApplicationConstants.PATH_PREFIX + "/user/game/game-id-goes-here/auto")).andExpect(status().isInternalServerError());
	}

	@Test
	public void getGridState_success_onGoingGame_noShots() throws Exception {
		GameDTO gameDTO = new GameDTO();
		gameDTO.setOpponentId("marsian");
		gameDTO.setOwnUserId("mirko");
		gameDTO.setOwnBoardState(new HashMap<>());
		Set<GridPosition> spaceshipPositions = createSpaceshipPositions();
		gameDTO.setStarshipPositions(spaceshipPositions);
		gameDTO.setOpponentBoardState(new HashMap<>());
		gameDTO.setPlayerTurn("mirko");
		when(gameService.getGameState(any(String.class))).thenReturn(gameDTO);

		mockMvc.perform(get(ApplicationConstants.PATH_PREFIX + "/user/game/game-id-goes-here")).andExpect(status().isOk())
				.andExpect(jsonPath("$.self.user_id", is("mirko"))).andExpect(jsonPath("$.self.board[0]", is("*.*..........**.")))
				.andExpect(jsonPath("$.self.board[1]", is("*.*..........*.*"))).andExpect(jsonPath("$.self.board[2]", is(".*...........**.")))
				.andExpect(jsonPath("$.self.board[3]", is("*.*..........*.*"))).andExpect(jsonPath("$.self.board[4]", is("*.*..........**.")))
				.andExpect(jsonPath("$.self.board[5]", is("................"))).andExpect(jsonPath("$.self.board[6]", is("................")))
				.andExpect(jsonPath("$.self.board[7]", is("........**......"))).andExpect(jsonPath("$.self.board[8]", is(".......*........")))
				.andExpect(jsonPath("$.self.board[9]", is("........**......"))).andExpect(jsonPath("$.self.board[10]", is("..........*.....")))
				.andExpect(jsonPath("$.self.board[11]", is("........**......"))).andExpect(jsonPath("$.self.board[12]", is("*.............*.")))
				.andExpect(jsonPath("$.self.board[13]", is("*............*.*"))).andExpect(jsonPath("$.self.board[14]", is("*............***")))
				.andExpect(jsonPath("$.self.board[15]", is("***..........*.*"))).andExpect(jsonPath("$.opponent.user_id", is("marsian")))
				.andExpect(jsonPath("$.opponent.board[0]", is("................"))).andExpect(jsonPath("$.opponent.board[1]", is("................")))
				.andExpect(jsonPath("$.opponent.board[2]", is("................"))).andExpect(jsonPath("$.opponent.board[3]", is("................")))
				.andExpect(jsonPath("$.opponent.board[4]", is("................"))).andExpect(jsonPath("$.opponent.board[5]", is("................")))
				.andExpect(jsonPath("$.opponent.board[6]", is("................"))).andExpect(jsonPath("$.opponent.board[7]", is("................")))
				.andExpect(jsonPath("$.opponent.board[8]", is("................"))).andExpect(jsonPath("$.opponent.board[9]", is("................")))
				.andExpect(jsonPath("$.opponent.board[10]", is("................")))
				.andExpect(jsonPath("$.opponent.board[11]", is("................")))
				.andExpect(jsonPath("$.opponent.board[12]", is("................")))
				.andExpect(jsonPath("$.opponent.board[13]", is("................")))
				.andExpect(jsonPath("$.opponent.board[14]", is("................")))
				.andExpect(jsonPath("$.opponent.board[15]", is("................"))).andExpect(jsonPath("$.game.player_turn", is("mirko")));
	}

	@Test
	public void getGridState_success_onGoingGame_withShots() throws Exception {
		GameDTO gameDTO = new GameDTO();
		gameDTO.setOpponentId("marsian");
		gameDTO.setOwnUserId("mirko");
		Map<String, String> ownBoardState = new HashMap<>();
		ownBoardState.put("0x0", "hit");
		ownBoardState.put("0x1", "miss");
		ownBoardState.put("0xF", "miss");
		ownBoardState.put("FxF", "hit");
		gameDTO.setOwnBoardState(ownBoardState);
		Set<GridPosition> spaceshipPositions = createSpaceshipPositions();
		gameDTO.setStarshipPositions(spaceshipPositions);
		Map<String, String> opponentBoardState = new HashMap<>();
		opponentBoardState.put("0xF", "hit");
		opponentBoardState.put("1xF", "miss");
		opponentBoardState.put("Ex0", "miss");
		opponentBoardState.put("Fx0", "hit");
		gameDTO.setOpponentBoardState(opponentBoardState);
		gameDTO.setPlayerTurn("mirko");
		when(gameService.getGameState(any(String.class))).thenReturn(gameDTO);

		mockMvc.perform(get(ApplicationConstants.PATH_PREFIX + "/user/game/game-id-goes-here")).andExpect(status().isOk())
				.andExpect(jsonPath("$.self.user_id", is("mirko"))).andExpect(jsonPath("$.self.board[0]", is("X-*..........**-")))
				.andExpect(jsonPath("$.self.board[1]", is("*.*..........*.*"))).andExpect(jsonPath("$.self.board[2]", is(".*...........**.")))
				.andExpect(jsonPath("$.self.board[3]", is("*.*..........*.*"))).andExpect(jsonPath("$.self.board[4]", is("*.*..........**.")))
				.andExpect(jsonPath("$.self.board[5]", is("................"))).andExpect(jsonPath("$.self.board[6]", is("................")))
				.andExpect(jsonPath("$.self.board[7]", is("........**......"))).andExpect(jsonPath("$.self.board[8]", is(".......*........")))
				.andExpect(jsonPath("$.self.board[9]", is("........**......"))).andExpect(jsonPath("$.self.board[10]", is("..........*.....")))
				.andExpect(jsonPath("$.self.board[11]", is("........**......"))).andExpect(jsonPath("$.self.board[12]", is("*.............*.")))
				.andExpect(jsonPath("$.self.board[13]", is("*............*.*"))).andExpect(jsonPath("$.self.board[14]", is("*............***")))
				.andExpect(jsonPath("$.self.board[15]", is("***..........*.X"))).andExpect(jsonPath("$.opponent.user_id", is("marsian")))
				.andExpect(jsonPath("$.opponent.board[0]", is("...............X"))).andExpect(jsonPath("$.opponent.board[1]", is("...............-")))
				.andExpect(jsonPath("$.opponent.board[2]", is("................"))).andExpect(jsonPath("$.opponent.board[3]", is("................")))
				.andExpect(jsonPath("$.opponent.board[4]", is("................"))).andExpect(jsonPath("$.opponent.board[5]", is("................")))
				.andExpect(jsonPath("$.opponent.board[6]", is("................"))).andExpect(jsonPath("$.opponent.board[7]", is("................")))
				.andExpect(jsonPath("$.opponent.board[8]", is("................"))).andExpect(jsonPath("$.opponent.board[9]", is("................")))
				.andExpect(jsonPath("$.opponent.board[10]", is("................")))
				.andExpect(jsonPath("$.opponent.board[11]", is("................")))
				.andExpect(jsonPath("$.opponent.board[12]", is("................")))
				.andExpect(jsonPath("$.opponent.board[13]", is("................")))
				.andExpect(jsonPath("$.opponent.board[14]", is("-...............")))
				.andExpect(jsonPath("$.opponent.board[15]", is("X..............."))).andExpect(jsonPath("$.game.player_turn", is("mirko")));
	}

	@Test
	public void getGridState_success_onGoingGame_withKillShots() throws Exception {
		GameDTO gameDTO = new GameDTO();
		gameDTO.setOpponentId("marsian");
		gameDTO.setOwnUserId("mirko");
		Map<String, String> ownBoardState = new HashMap<>();
		ownBoardState.put("0x0", "hit");
		ownBoardState.put("0x1", "miss");
		ownBoardState.put("0xF", "miss");
		ownBoardState.put("FxF", "hit");
		ownBoardState.put("Fx0", "hit");
		ownBoardState.put("Ex0", "hit");
		ownBoardState.put("Dx0", "hit");
		ownBoardState.put("Cx0", "hit");
		ownBoardState.put("Fx1", "hit");
		ownBoardState.put("Fx2", "kill");
		gameDTO.setOwnBoardState(ownBoardState);
		Set<GridPosition> spaceshipPositions = createSpaceshipPositions();
		gameDTO.setStarshipPositions(spaceshipPositions);
		Map<String, String> opponentBoardState = new HashMap<>();
		opponentBoardState.put("0xF", "hit");
		opponentBoardState.put("1xF", "miss");
		opponentBoardState.put("FxF", "hit");
		opponentBoardState.put("Fx0", "hit");
		opponentBoardState.put("Ex0", "hit");
		opponentBoardState.put("Dx0", "hit");
		opponentBoardState.put("Cx0", "hit");
		opponentBoardState.put("Fx1", "hit");
		opponentBoardState.put("Fx2", "kill");
		gameDTO.setOpponentBoardState(opponentBoardState);
		gameDTO.setPlayerTurn("mirko");
		when(gameService.getGameState(any(String.class))).thenReturn(gameDTO);

		mockMvc.perform(get(ApplicationConstants.PATH_PREFIX + "/user/game/game-id-goes-here")).andExpect(status().isOk())
				.andExpect(jsonPath("$.self.user_id", is("mirko"))).andExpect(jsonPath("$.self.board[0]", is("X-*..........**-")))
				.andExpect(jsonPath("$.self.board[1]", is("*.*..........*.*"))).andExpect(jsonPath("$.self.board[2]", is(".*...........**.")))
				.andExpect(jsonPath("$.self.board[3]", is("*.*..........*.*"))).andExpect(jsonPath("$.self.board[4]", is("*.*..........**.")))
				.andExpect(jsonPath("$.self.board[5]", is("................"))).andExpect(jsonPath("$.self.board[6]", is("................")))
				.andExpect(jsonPath("$.self.board[7]", is("........**......"))).andExpect(jsonPath("$.self.board[8]", is(".......*........")))
				.andExpect(jsonPath("$.self.board[9]", is("........**......"))).andExpect(jsonPath("$.self.board[10]", is("..........*.....")))
				.andExpect(jsonPath("$.self.board[11]", is("........**......"))).andExpect(jsonPath("$.self.board[12]", is("X.............*.")))
				.andExpect(jsonPath("$.self.board[13]", is("X............*.*"))).andExpect(jsonPath("$.self.board[14]", is("X............***")))
				.andExpect(jsonPath("$.self.board[15]", is("XXX..........*.X"))).andExpect(jsonPath("$.opponent.user_id", is("marsian")))
				.andExpect(jsonPath("$.opponent.board[0]", is("...............X"))).andExpect(jsonPath("$.opponent.board[1]", is("...............-")))
				.andExpect(jsonPath("$.opponent.board[2]", is("................"))).andExpect(jsonPath("$.opponent.board[3]", is("................")))
				.andExpect(jsonPath("$.opponent.board[4]", is("................"))).andExpect(jsonPath("$.opponent.board[5]", is("................")))
				.andExpect(jsonPath("$.opponent.board[6]", is("................"))).andExpect(jsonPath("$.opponent.board[7]", is("................")))
				.andExpect(jsonPath("$.opponent.board[8]", is("................"))).andExpect(jsonPath("$.opponent.board[9]", is("................")))
				.andExpect(jsonPath("$.opponent.board[10]", is("................")))
				.andExpect(jsonPath("$.opponent.board[11]", is("................")))
				.andExpect(jsonPath("$.opponent.board[12]", is("X...............")))
				.andExpect(jsonPath("$.opponent.board[13]", is("X...............")))
				.andExpect(jsonPath("$.opponent.board[14]", is("X...............")))
				.andExpect(jsonPath("$.opponent.board[15]", is("XXX............X"))).andExpect(jsonPath("$.game.player_turn", is("mirko")));
	}

	@Test
	public void getGridState_success_won() throws Exception {
		GameDTO gameDTO = new GameDTO();
		gameDTO.setOpponentId("marsian");
		gameDTO.setOwnUserId("mirko");
		Map<String, String> ownBoardState = new HashMap<>();
		gameDTO.setOwnBoardState(ownBoardState);
		Set<GridPosition> spaceshipPositions = createSpaceshipPositions();
		gameDTO.setStarshipPositions(spaceshipPositions);
		Map<String, String> opponentBoardState = new HashMap<>();
		gameDTO.setOpponentBoardState(opponentBoardState);
		gameDTO.setWon("mirko");
		when(gameService.getGameState(any(String.class))).thenReturn(gameDTO);

		mockMvc.perform(get(ApplicationConstants.PATH_PREFIX + "/user/game/game-id-goes-here")).andExpect(status().isOk())
				.andExpect(jsonPath("$.self.user_id", is("mirko"))).andExpect(jsonPath("$.opponent.user_id", is("marsian")))
				.andExpect(jsonPath("$.game.won", is("mirko")));
	}

	private Set<GridPosition> createSpaceshipPositions() throws SpaceshipPositionOutOfBounds {
		Set<GridPosition> spaceshipPositions = new HashSet<>();
		GridPosition wingerPosition = GridPosition.create(0, 0);
		GridPosition anglePosition = GridPosition.create(12, 0);
		GridPosition aClassPosition = GridPosition.create(12, 13);
		GridPosition bClassPosition = GridPosition.create(0, 13);
		GridPosition sClassPosition = GridPosition.create(7, 7);

		SpaceshipFactory spaceshipFactory = new DefaultSpaceshipFactory();
		spaceshipPositions.addAll(spaceshipFactory
				.calculateSpaceshipCoordinates(spaceshipFactory.create(SpaceshipType.WINGER, Rotation.STRAIGHT, wingerPosition)));
		spaceshipPositions.addAll(spaceshipFactory
				.calculateSpaceshipCoordinates(spaceshipFactory.create(SpaceshipType.ANGLE, Rotation.STRAIGHT, anglePosition)));
		spaceshipPositions.addAll(spaceshipFactory
				.calculateSpaceshipCoordinates(spaceshipFactory.create(SpaceshipType.A_CLASS, Rotation.STRAIGHT, aClassPosition)));
		spaceshipPositions.addAll(spaceshipFactory
				.calculateSpaceshipCoordinates(spaceshipFactory.create(SpaceshipType.B_CLASS, Rotation.STRAIGHT, bClassPosition)));
		spaceshipPositions.addAll(spaceshipFactory
				.calculateSpaceshipCoordinates(spaceshipFactory.create(SpaceshipType.S_CLASS, Rotation.STRAIGHT, sClassPosition)));
		return spaceshipPositions;
	}
}
