package org.xebialabs.assessment.spaceship.domain.model.spaceship;

public enum SpaceshipType {
	WINGER, ANGLE, A_CLASS, B_CLASS, S_CLASS
}
