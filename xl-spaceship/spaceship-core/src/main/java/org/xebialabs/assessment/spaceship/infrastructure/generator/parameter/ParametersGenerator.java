package org.xebialabs.assessment.spaceship.infrastructure.generator.parameter;

import org.xebialabs.assessment.spaceship.domain.model.spaceship.Rotation;
import org.xebialabs.assessment.spaceship.domain.shared.GridPosition;

public interface ParametersGenerator {

	Rotation getRotation();

	GridPosition getPosition();

	boolean getMyTurn();
}
