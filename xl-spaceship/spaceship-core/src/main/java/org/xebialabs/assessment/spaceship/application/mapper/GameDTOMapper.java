package org.xebialabs.assessment.spaceship.application.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
import org.xebialabs.assessment.spaceship.application.implementation.dto.GameDTO;
import org.xebialabs.assessment.spaceship.infrastructure.command.CreateGameCommand;
import org.xebialabs.assessment.spaceship.infrastructure.command.CreateGameResult;
import org.xebialabs.assessment.spaceship.infrastructure.command.CreateUserGameCommand;

@Mapper(componentModel = "spring")
public interface GameDTOMapper {

	GameDTOMapper INSTANCE = Mappers.getMapper(GameDTOMapper.class);

	@Mapping(source = "userId", target = "opponentId")
	@Mapping(source = "fullName", target = "opponentFullName")
	@Mapping(source = "spaceshipProtocol.hostname", target = "hostname")
	@Mapping(source = "spaceshipProtocol.port", target = "port")
	GameDTO createGameCommandToGameDTO(CreateGameCommand command);

	@Mapping(source = "gameId", target = "gameId")
	@Mapping(source = "ownUserId", target = "userId")
	@Mapping(source = "ownFullName", target = "fullName")
	CreateGameResult gameDTOToCreateGameResult(GameDTO game);

	@Mapping(source = "spaceshipProtocol.hostname", target = "hostname")
	@Mapping(source = "spaceshipProtocol.port", target = "port")
	GameDTO createUserGameCommandToGameDTO(CreateUserGameCommand command);

	@Mapping(source = "opponentId", target = "userId")
	@Mapping(source = "opponentFullName", target = "fullName")
	@Mapping(source = "hostname", target = "spaceshipProtocol.hostname")
	@Mapping(source = "port", target = "spaceshipProtocol.port")
	CreateGameCommand gameDTOToCreateGameCommand(GameDTO gameDTO);

	@Mapping(source = "gameId", target = "gameId")
	@Mapping(source = "userId", target = "opponentId")
	@Mapping(source = "fullName", target = "opponentFullName")
	GameDTO createGameResultToGameDTO(CreateGameResult body);
}
