package org.xebialabs.assessment.spaceship.application;

import org.xebialabs.assessment.spaceship.domain.model.game.grid.Grid;

public interface GridService {

	Grid createEmptyGrid();

	Grid create();

	String[] drawGrid(Grid opponentGrid);
}
