package org.xebialabs.assessment.spaceship.infrastructure.connector;

import org.xebialabs.assessment.spaceship.application.implementation.dto.GameDTO;
import org.xebialabs.assessment.spaceship.application.implementation.dto.SalvoDTO;
import org.xebialabs.assessment.spaceship.domain.shared.GridPosition;
import org.xebialabs.assessment.spaceship.infrastructure.GameProtocolError;

import java.util.Set;

public interface ProtocolConnector {
	SalvoDTO fireSalvo(String gameId, String hostname, Long port, Set<GridPosition> salvo);

	GameDTO createGame(String remoteHostname, Long remotePort, GameDTO gameDTO) throws GameProtocolError;
}
