package org.xebialabs.assessment.spaceship.application;

import org.xebialabs.assessment.spaceship.domain.model.game.Game;

public interface GameStrategyProvider {

	int calculateSalvoSize(Game game);

	boolean calculateUserTurn(Game game, int thisTurnKills);
}
