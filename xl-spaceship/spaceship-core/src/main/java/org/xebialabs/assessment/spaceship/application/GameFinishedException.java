package org.xebialabs.assessment.spaceship.application;

import org.xebialabs.assessment.spaceship.application.implementation.dto.SalvoDTO;

public class GameFinishedException extends Exception {
	private final SalvoDTO salvoResponse;

	public GameFinishedException(SalvoDTO salvoResponse) {
		this.salvoResponse = salvoResponse;
	}

	public SalvoDTO getSalvoResponse() {
		return salvoResponse;
	}
}
