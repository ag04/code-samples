package org.xebialabs.assessment.spaceship.configuration;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.xebialabs.assessment.spaceship.domain.model.game.DefaultGameFactory;
import org.xebialabs.assessment.spaceship.domain.model.game.GameFactory;
import org.xebialabs.assessment.spaceship.domain.model.game.grid.DummyGridFactory;
import org.xebialabs.assessment.spaceship.domain.model.game.grid.GridFactory;
import org.xebialabs.assessment.spaceship.domain.model.game.grid.RandomSetGridFactory;
import org.xebialabs.assessment.spaceship.domain.model.spaceship.DefaultSpaceshipFactory;
import org.xebialabs.assessment.spaceship.domain.model.spaceship.SpaceshipFactory;
import org.xebialabs.assessment.spaceship.infrastructure.generator.parameter.ParametersGenerator;
import org.xebialabs.assessment.spaceship.infrastructure.generator.parameter.RandomParametersGenerator;

@Configuration
public class FactoriesConfiguration {

	@Bean
	public ParametersGenerator parametersGenerator() {
		return new RandomParametersGenerator();
	}

	@Bean
	public SpaceshipFactory spaceshipFactory() {
		return new DefaultSpaceshipFactory();
	}

	@Bean("gridFactory")
	@ConditionalOnProperty(name = "spaceship.grid-factory.type", havingValue = "dummy", matchIfMissing = true)
	public GridFactory dummyGridFactory(SpaceshipFactory spaceshipFactory) {
		return new DummyGridFactory(spaceshipFactory);
	}

	@Bean("gridFactory")
	@ConditionalOnProperty(name = "spaceship.grid-factory.type", havingValue = "random")
	public GridFactory randomGridFactory(SpaceshipFactory spaceshipFactory, ParametersGenerator parametersGenerator) {
		return new RandomSetGridFactory(spaceshipFactory, parametersGenerator);
	}

	@Bean
	public GameFactory gameFactory(ParametersGenerator parametersGenerator) {
		return new DefaultGameFactory(parametersGenerator);
	}
}
