import reducer from '../../../../store/reducers/game.reducer';
import * as actionTypes from '../../../../store/actions/actionTypes';
import * as constants from '../testConstants';

const initialState = {
  gameLocation: null,
  gameDetails: null,
  createForm: {
    hostname: null,
    port: null,
    rule: null
  },
  selectedSalvo: [],
  createGameErrorMessage: null,
  gameErrorMessage: null
};

describe('Game reducer', () => {
  test('should have initial state', () => {
    const action = { type: 'default' };
    const state = reducer(initialState, action);
    expect(state).toMatchObject(initialState);
  });

  test('should add gameLocation when CREATED_GAME', () => {
    const gameLocation = '/some/url/here';
    const action = {
      type: actionTypes.CREATED_GAME,
      gameLocation: gameLocation
    };
    const state = reducer(initialState, action);
    expect(state.gameLocation).toMatch(gameLocation);
  });

  test('should add gameDetails, ruleSet reset game related fields  when DISPLAYING_GAME', () => {
    const gameDetails = constants.GAME_DETAILS;
    const ruleSet = constants.RULE_SET;
    const action = {
      type: actionTypes.DISPLAYING_GAME,
      gameDetails: gameDetails,
      ruleSet: ruleSet
    };
    const state = reducer(initialState, action);
    expect(state.gameDetails).toMatchObject(gameDetails);
    expect(state.ruleSet).toMatchObject(ruleSet);
    expect(state.selectedSalvo).toMatchObject([]);
    expect(state.createGameErrorMessage).toBeNull();
    expect(state.gameErrorMessage).toBeNull();
  });

  test('should add createForm when CREATE_FORM_CHANGED', () => {
    const createForm = {
      hostname: 'localhost',
      port: 9090,
      rules: 'standard'
    };
    const action = {
      type: actionTypes.CREATE_FORM_CHANGED,
      createForm: createForm
    };
    const state = reducer(initialState, action);
    expect(state.createForm).toMatchObject(createForm);
  });

  test('should add selectedSalvo when SELECTED_SALVO_CHANGED', () => {
    const selectedSalvo = constants.SALVO;
    const action = {
      type: actionTypes.SELECTED_SALVO_CHANGED,
      selectedSalvo: selectedSalvo
    };
    const state = reducer(initialState, action);
    expect(state.selectedSalvo).toMatchObject(selectedSalvo);
  });

  test('should add createGameErrorMessage when CREATE_GAME_FAILED', () => {
    const createGameErrorMessage = 'Error creating!';
    const action = {
      type: actionTypes.CREATE_GAME_FAILED,
      errorMessage: createGameErrorMessage
    };
    const state = reducer(initialState, action);
    expect(state.createGameErrorMessage).toMatch(createGameErrorMessage);
  });

  test('should add createGameErrorMessage when CREATE_GAME_FAILED_ON_REMOTE', () => {
    const createGameErrorMessage = 'Error creating remote game!';
    const action = {
      type: actionTypes.CREATE_GAME_FAILED_ON_REMOTE,
      errorMessage: createGameErrorMessage
    };
    const state = reducer(initialState, action);
    expect(state.createGameErrorMessage).toMatch(createGameErrorMessage);
  });

  test('should add gameErrorMessage when DISPLAY_GAME_FAILED', () => {
    const errorMessage = 'Error!';
    const action = {
      type: actionTypes.DISPLAY_GAME_FAILED,
      errorMessage: errorMessage
    };
    const state = reducer(initialState, action);
    expect(state.gameErrorMessage).toMatch(errorMessage);
  });
});
