package org.xebialabs.assessment.spaceship.domain.model.spaceship;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("S_CLASS")
public class SClass extends Spaceship {

	public SClass() {
		super(SpaceshipType.S_CLASS);
	}

}
