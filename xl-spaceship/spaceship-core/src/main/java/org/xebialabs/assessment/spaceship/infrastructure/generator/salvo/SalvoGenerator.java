package org.xebialabs.assessment.spaceship.infrastructure.generator.salvo;

import org.xebialabs.assessment.spaceship.domain.model.game.Game;
import org.xebialabs.assessment.spaceship.domain.shared.GridPosition;

import java.util.Set;

public interface SalvoGenerator {

	Set<GridPosition> generateSalvo(Game game, GridPosition lastHit);

}
