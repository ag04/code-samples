package org.xebialabs.assessment.spaceship.infrastructure.command;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class UserGrid {
	@JsonProperty("user_id")
	String userId;

	@JsonProperty("board")
	List<String> gridRows;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public List<String> getGridRows() {
		return gridRows;
	}

	public void setGridRows(List<String> gridRows) {
		this.gridRows = gridRows;
	}
}
