package org.xebialabs.assessment.spaceship.infrastructure.generator.salvo;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;
import org.xebialabs.assessment.spaceship.application.GameStrategyProvider;
import org.xebialabs.assessment.spaceship.domain.shared.GridPosition;
import org.xebialabs.assessment.spaceship.infrastructure.generator.parameter.ParametersGenerator;

import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Component
@ConditionalOnProperty(name = "spaceship.autopilot.type", havingValue = "smart")
public class SmartSalvoGenerator extends AbstractSalvoGenerator {

	private static final int ALGORITHM_THRESHOLD = 200;

	public SmartSalvoGenerator(GameStrategyProvider gameStrategyProvider, ParametersGenerator parametersGenerator) {
		super(gameStrategyProvider, parametersGenerator);
	}

	@Override
	protected GridPosition nextShotPosition(Map<String, String> shotResults, Set<GridPosition> currentSalvo, GridPosition lastHit) {
		if (shotResults.keySet().size() > ALGORITHM_THRESHOLD) {
			return getSequentialGridPosition(shotResults, currentSalvo);
		}

		Set<GridPosition> hitCoordinates =
				shotResults.entrySet().stream()
						.filter(e -> e.getValue().equals("hit"))
						.map(Map.Entry::getKey)
						.map(shotCoordinate -> {
							String[] split = shotCoordinate.split("x");
							int x = Integer.parseInt(split[0], 16);
							int y = Integer.parseInt(split[1], 16);
							return GridPosition.create(x, y);
						})
						.collect(Collectors.toSet());

		int hitCount = hitCoordinates.size();
		GridPosition newShot;
		int retryCount = 0;
		GridPosition hitCoordinate = lastHit;
		do {
			if (hitCount > 0) {
				if (retryCount > 9 || hitCoordinate == null) {
					hitCoordinate = pickOneOfHits(hitCoordinates);
					retryCount = 0;
					hitCount--;
				}
				int x = Math.abs((hitCoordinate.getX() + (retryCount % 3 - 1)) % 16);
				int y = Math.abs((hitCoordinate.getY() + (retryCount % 3 - 1)) % 16);
				newShot = GridPosition.create(x, y);
				retryCount++;
			}
			else {
				newShot = parametersGenerator.getPosition();
			}
		} while (shotResults.get(newShot.toString()) != null || currentSalvo.contains(newShot));
		return newShot;
	}

	private GridPosition pickOneOfHits(Set<GridPosition> hitCoordinates) {
		int hitCount = hitCoordinates.size();
		int index = (int) (Math.random() * hitCount);
		return (GridPosition) hitCoordinates.toArray()[index];
	}

}
