import * as actionTypes from '../actions/actionTypes';

const initialState = {
  games: null,
  error: false
};

const gameList = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.SET_GAME_LIST:
      return {
        ...state,
        games: action.games
      };
    case actionTypes.INIT_GAME_LIST_FAILED:
      return {
        ...state,
        error: true
      };
    default:
      return state;
  }
};

export default gameList;
