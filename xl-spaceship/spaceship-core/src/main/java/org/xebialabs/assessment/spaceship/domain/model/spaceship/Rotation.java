package org.xebialabs.assessment.spaceship.domain.model.spaceship;

public enum Rotation {
	STRAIGHT(0, 1), // sin 0, cos 0
	LEFT(1, 0), // sin 90, cos 90
	RIGHT(0, -1), // sin 180, cos 90
	DOWN(-1, 0); // sin 270, cos 270

	private int x;
	private int y;

	Rotation(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public int calculateX(Integer x, Integer y) {
		return x * this.y - y * this.x;
	}

	public int calculateY(Integer x, Integer y) {
		return x * this.x + y * this.y;
	}
}
