package org.xebialabs.assessment.spaceship.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "spaceship", ignoreUnknownFields = true)
public class ApplicationProperties {

	private LocalUser localUser = new LocalUser();
	private GridFactory gridFactory = new GridFactory();

	public void setLocalUser(LocalUser localUser) {
		this.localUser = localUser;
	}

	public LocalUser getLocalUser() {
		return localUser;
	}

	public GridFactory getGridFactory() {
		return gridFactory;
	}

	public void setGridFactory(GridFactory gridFactory) {
		this.gridFactory = gridFactory;
	}

	public static class LocalUser {
		private String userId;
		private String username;
		private String hostname;

		public String getUserId() {
			return userId;
		}

		public void setUserId(String userId) {
			this.userId = userId;
		}

		public String getUsername() {
			return username;
		}

		public void setUsername(String username) {
			this.username = username;
		}

		public String getHostname() {
			return hostname;
		}

		public void setHostname(String hostname) {
			this.hostname = hostname;
		}
	}

	private class GridFactory {

		private String type;

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}
	}
}


