package org.xebialabs.assessment.spaceship.application;

import org.xebialabs.assessment.spaceship.domain.model.game.Opponent;

public interface OpponentService {

	Opponent fetchOrCreateOpponent(String opponentId, String opponentFullName);

}
