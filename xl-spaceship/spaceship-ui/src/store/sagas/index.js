import { takeEvery } from 'redux-saga/effects';

import { initGameListSaga } from './game-list.saga';
import {
  createGameSaga,
  displayGameSaga,
  shootSelectedSalvoSaga,
  turnAutopilotOnSaga
} from './game.saga';
import * as actionType from '../actions/actionTypes';

export function* watchGameList() {
  yield takeEvery(actionType.INIT_GAME_LIST, initGameListSaga);
}

export function* watchGameAction() {
  yield takeEvery(actionType.CREATE_GAME, createGameSaga);
  yield takeEvery(actionType.CREATED_GAME, displayGameSaga);
  yield takeEvery(actionType.DISPLAY_GAME, displayGameSaga);
  yield takeEvery(actionType.TURN_AUTOPILOT_ON, turnAutopilotOnSaga);
  yield takeEvery(actionType.SHOOT_SELECTED_SALVO, shootSelectedSalvoSaga);
}
