import {call, put} from 'redux-saga/effects';

import {createGame, fetchGameData, getGameRuleSet, shootSalvo, turnAutopilotOn} from '../../../../store/services/game.service';
import * as saga from '../../../../store/sagas/game.saga';
import * as actionTypes from '../../../../store/actions/actionTypes';
import {BASE_USER_RESOURCE_URI} from '../../../../utils/constants';

import * as constants from '../testConstants';

describe('Game saga - should create game', () => {
    test('should create game', () => {
        const createGameAction = {
            type: actionTypes.CREATE_GAME,
            game: {
                hostname: 'localhost',
                port: 9090,
                rule: 'standard'
            }
        };
        const createGameCommand = {
            spaceship_protocol: {
                hostname: createGameAction.game.hostname,
                port: createGameAction.game.port
            },
            rules: createGameAction.game.rule
        };
        const createGameResponse = {
            data: constants.GAME_DETAILS,
            gameLocation: BASE_USER_RESOURCE_URI + '/some-game-id'
        };
        const ruleSetResponse = constants.RULE_SET;
        const gamesResponse = constants.GAME_LIST;

        const createGameSaga = saga.createGameSaga(createGameAction);
        expect(createGameSaga.next().value).toMatchObject(
            call(createGame, createGameCommand)
        );
        expect(createGameSaga.next(createGameResponse).value).toMatchObject(
            put({
                type: actionTypes.CREATE_FORM_CHANGED,
                createForm: {
                    hostname: null,
                    port: null,
                    rule: null
                }
            })
        );
        expect(createGameSaga.next().value).toMatchObject(
            put({
                type: actionTypes.SET_GAME_LOCATION,
                gameLocation: BASE_USER_RESOURCE_URI + '/some-game-id'
            })
        );
        createGameSaga.next().value;
        expect(createGameSaga.next(BASE_USER_RESOURCE_URI).value).toMatchObject(
            call(getGameRuleSet, BASE_USER_RESOURCE_URI)
        );
        expect(createGameSaga.next(ruleSetResponse).value).toMatchObject(
            put({
                type: actionTypes.DISPLAYING_GAME,
                gameDetails: createGameResponse.data,
                ruleSet: ruleSetResponse
            })
        );
        expect(createGameSaga.next({data: gamesResponse}).value).toMatchObject(
            put({
                type: actionTypes.INIT_GAME_LIST
            })
        );
        expect(createGameSaga.next().done).toBe(true);
    });
});

describe('Game saga - create with error', () => {
    test('should handle error', () => {
        const createGameAction = {
            type: actionTypes.CREATE_GAME,
            game: {
                hostname: 'localhost',
                port: 9090,
                rule: 'standard'
            }
        };
        const createGameCommand = {
            spaceship_protocol: {
                hostname: createGameAction.game.hostname,
                port: createGameAction.game.port
            },
            rules: createGameAction.game.rule
        };

        const createGameSaga = saga.createGameSaga(createGameAction);
        expect(createGameSaga.next().value).toMatchObject(
            call(createGame, createGameCommand)
        );
        expect(createGameSaga.throw({message: "Error message"}).value).toMatchObject(
            put({
                type: actionTypes.CREATE_GAME_FAILED,
                errorMessage: "Error message"
            })
        );

        expect(createGameSaga.next().done).toBe(true);
    });
});


describe('Game saga - create with error on remote', () => {
    test('should handle error', () => {
        const createGameAction = {
            type: actionTypes.CREATE_GAME,
            game: {
                hostname: 'localhost',
                port: 9090,
                rule: 'standard'
            }
        };
        const createGameCommand = {
            spaceship_protocol: {
                hostname: createGameAction.game.hostname,
                port: createGameAction.game.port
            },
            rules: createGameAction.game.rule
        };

        const createGameSaga = saga.createGameSaga(createGameAction);
        expect(createGameSaga.next().value).toMatchObject(
            call(createGame, createGameCommand)
        );
        expect(createGameSaga.throw({
            response: {
                status: 400
            },
            message: 'Error message'
        }).value).toMatchObject(
            put({
                type: actionTypes.CREATE_GAME_FAILED_ON_REMOTE,
                errorMessage: "Error calling remote localhost:9090"
            })
        );

        expect(createGameSaga.next().done).toBe(true);
    });
});


describe('Game saga', () => {
    test('should shoot salvo', () => {
        const shootSalvoAction = {
            type: actionTypes.SHOOT_SELECTED_SALVO,
            salvo: {
                gameId: 'some-game-id',
                selectedSalvo: ['0x0', 'AxA']
            }
        };

        const shootSelectedSalvoSaga = saga.shootSelectedSalvoSaga(
            shootSalvoAction
        );
        expect(shootSelectedSalvoSaga.next().value).toMatchObject(
            call(shootSalvo, 'some-game-id', ['0x0', 'AxA'])
        );
        expect(shootSelectedSalvoSaga.next('some-game-id').value).toMatchObject(
            put({
                type: actionTypes.DISPLAY_GAME,
                gameLocation: BASE_USER_RESOURCE_URI + '/some-game-id'
            })
        );
        expect(shootSelectedSalvoSaga.next().done).toBe(true);
    });

    test('should shoot salvo with error', () => {
        const shootSalvoAction = {
            type: actionTypes.SHOOT_SELECTED_SALVO,
            salvo: {
                gameId: 'some-game-id',
                selectedSalvo: ['0x0', 'AxA']
            }
        };

        const shootSelectedSalvoSaga = saga.shootSelectedSalvoSaga(
            shootSalvoAction
        );
        expect(shootSelectedSalvoSaga.next().value).toMatchObject(
            call(shootSalvo, 'some-game-id', ['0x0', 'AxA'])
        );
        expect(shootSelectedSalvoSaga.throw({message: 'Error message'}).value).toMatchObject(
            put({
                type: actionTypes.DISPLAY_GAME_FAILED,
                errorMessage: 'Shoot salvo error: Error message'
            })
        );
        expect(shootSelectedSalvoSaga.next().done).toBe(true);
    });

    test('should turn autopilot on', () => {
        const action = {
            type: actionTypes.TURN_AUTOPILOT_ON,
            gameId: 'some-game-id'
        };

        const turnAutopilotOnSaga = saga.turnAutopilotOnSaga(action);
        expect(turnAutopilotOnSaga.next().value).toMatchObject(
            call(turnAutopilotOn, action.gameId)
        );
        expect(turnAutopilotOnSaga.next('some-game-id').value).toMatchObject(
            put({
                type: actionTypes.DISPLAY_GAME,
                gameLocation: BASE_USER_RESOURCE_URI + '/some-game-id'
            })
        );
        expect(turnAutopilotOnSaga.next().done).toBe(true);
    });

    test('should turn autopilot on with error', () => {
        const action = {
            type: actionTypes.TURN_AUTOPILOT_ON,
            gameId: 'some-game-id'
        };

        const turnAutopilotOnSaga = saga.turnAutopilotOnSaga(action);
        expect(turnAutopilotOnSaga.next().value).toMatchObject(
            call(turnAutopilotOn, action.gameId)
        );
        expect(turnAutopilotOnSaga.throw({message: "Error message"}).value).toMatchObject(
            put({
                type: actionTypes.DISPLAY_GAME_FAILED,
                errorMessage: 'Turn autopilot on: Error message'
            })
        );
        expect(turnAutopilotOnSaga.next().done).toBe(true);
    });

    test('should display game', () => {
        const action = {
            type: actionTypes.DISPLAY_GAME,
            gameLocation: BASE_USER_RESOURCE_URI + '/some-game-id'
        };

        const displayGameSaga = saga.displayGameSaga(action);
        expect(displayGameSaga.next().value).toMatchObject(
            call(fetchGameData, BASE_USER_RESOURCE_URI + '/some-game-id')
        );
        expect(displayGameSaga.next(constants.GAME_DETAILS).value).toMatchObject(
            call(getGameRuleSet, BASE_USER_RESOURCE_URI + '/some-game-id')
        );
        expect(displayGameSaga.next(constants.RULE_SET).value).toMatchObject(
            put({
                type: actionTypes.DISPLAYING_GAME,
                gameDetails: constants.GAME_DETAILS,
                ruleSet: constants.RULE_SET
            })
        );
        expect(displayGameSaga.next().done).toBe(true);
    });

    test('should display game with error', () => {
        const action = {
            type: actionTypes.DISPLAY_GAME,
            gameLocation: BASE_USER_RESOURCE_URI + '/some-game-id'
        };

        const displayGameSaga = saga.displayGameSaga(action);
        expect(displayGameSaga.next().value).toMatchObject(
            call(fetchGameData, BASE_USER_RESOURCE_URI + '/some-game-id')
        );
        expect(displayGameSaga.throw({message: 'Error message'}).value).toMatchObject(
            put({
                type: actionTypes.DISPLAY_GAME_FAILED,
                errorMessage: 'Error fetching game: Error message'
            })
        );
        expect(displayGameSaga.next().done).toBe(true);
    });
});
