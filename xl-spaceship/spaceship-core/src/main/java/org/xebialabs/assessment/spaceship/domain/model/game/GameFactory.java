package org.xebialabs.assessment.spaceship.domain.model.game;

import org.xebialabs.assessment.spaceship.domain.model.game.grid.Grid;

public interface GameFactory {

	Game create(String hostname, Long port, Opponent opponent, String gameRule, Grid myGrid, Grid opponentGrid);

}
