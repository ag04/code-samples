package org.xebialabs.assessment.spaceship.infrastructure.command;

import javax.validation.constraints.NotNull;

public class SpaceshipProtocol {
	@NotNull
	private String hostname;

	@NotNull
	private Long port;

	public String getHostname() {
		return hostname;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}

	public Long getPort() {
		return port;
	}

	public void setPort(Long port) {
		this.port = port;
	}
}
