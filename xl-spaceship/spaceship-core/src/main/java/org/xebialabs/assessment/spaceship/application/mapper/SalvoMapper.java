package org.xebialabs.assessment.spaceship.application.mapper;

import org.springframework.stereotype.Service;
import org.xebialabs.assessment.spaceship.application.implementation.dto.SalvoDTO;
import org.xebialabs.assessment.spaceship.domain.shared.GridPosition;
import org.xebialabs.assessment.spaceship.infrastructure.command.FireSalvoCommand;
import org.xebialabs.assessment.spaceship.infrastructure.command.FireSalvoResult;
import org.xebialabs.assessment.spaceship.infrastructure.command.GameState;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class SalvoMapper {

	public Set<GridPosition> fireSalvoCommandToGridPositions(FireSalvoCommand command) {
		Set<GridPosition> gridPositions = new HashSet<>();

		command.getSalvo().forEach(shot -> {
			String[] split = shot.split("x");
			if (split.length == 2) {
				GridPosition position = createGridPositionFromHexCoordinate(split);
				gridPositions.add(position);
			}
			else {
				throw new IllegalArgumentException("Salvo cannot be parsed");
			}
		});

		return gridPositions;
	}

	public FireSalvoResult salvoDtoToFireSalvoResult(SalvoDTO salvoDTO) {
		FireSalvoResult fireSalvoResult = new FireSalvoResult();

		Map<String, String> shotResults = new HashMap<>();
		salvoDTO.getShotResults().forEach((position, result) -> {
			shotResults.put(position, result.toString());
		});
		fireSalvoResult.setSalvoResult(shotResults);

		GameState gameState = new GameState();
		gameState.setPlayerTurn(salvoDTO.getPlayerTurn());
		gameState.setWon(salvoDTO.getWon());
		fireSalvoResult.setGameState(gameState);

		fireSalvoResult.setAutopilot(salvoDTO.isAutopilot());

		return fireSalvoResult;
	}

	public SalvoDTO fireSalvoResultToSalvoDto(FireSalvoResult fireSalvoResult) {
		SalvoDTO salvoDTO = new SalvoDTO();
		salvoDTO.setPlayerTurn(fireSalvoResult.getGameState().getPlayerTurn());
		salvoDTO.setWon(fireSalvoResult.getGameState().getWon());
		salvoDTO.setShotResults(fireSalvoResult.getSalvoResult());
		return salvoDTO;
	}

	public FireSalvoCommand salvoListToFireSalvoCommand(Set<GridPosition> salvo) {
		FireSalvoCommand command = new FireSalvoCommand();
		command.setSalvo(salvo.stream().map(shot -> getHexCoordinates(shot)).collect(Collectors.toList()));
		return command;
	}

	private GridPosition createGridPositionFromHexCoordinate(String[] split) {
		int x = Integer.parseInt(split[0], 16);
		if (x < 0 || x > 15) {
			throw new IndexOutOfBoundsException("Coordinate X is out of bounds");
		}
		int y = Integer.parseInt(split[1], 16);
		if (y < 0 || y > 15) {
			throw new IndexOutOfBoundsException("Coordinate Y is out of bounds");
		}
		return GridPosition.create(x, y);
	}

	private String getHexCoordinates(GridPosition position) {
		StringBuilder builder = new StringBuilder();
		builder.append(Integer.toHexString(position.getX()).toUpperCase()).append('x').append(Integer.toHexString(position.getY()).toUpperCase());
		return builder.toString();
	}

}
