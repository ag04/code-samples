package org.xebialabs.assessment.spaceship.domain.model.spaceship;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("WINGER")
public class Winger extends Spaceship {

	public Winger() {
		super(SpaceshipType.WINGER);
	}

}
