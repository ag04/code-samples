package org.xebialabs.assessment.spaceship.infrastructure.command;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class FireSalvoResult {

	@JsonProperty("salvo")
	private Map<String, String> salvoResult;

	@JsonProperty("game")
	private GameState gameState;

	@JsonIgnore
	private boolean finished;
	@JsonIgnore
	private boolean autopilot;

	public Map<String, String> getSalvoResult() {
		return salvoResult;
	}

	public void setSalvoResult(Map<String, String> salvoResult) {
		this.salvoResult = salvoResult;
	}

	public GameState getGameState() {
		return gameState;
	}

	public void setGameState(GameState gameState) {
		this.gameState = gameState;
	}

	public boolean isFinished() {
		return finished;
	}

	public void setFinished(boolean finished) {
		this.finished = finished;
	}

	public boolean isAutopilot() {
		return autopilot;
	}

	public void setAutopilot(boolean autopilot) {
		this.autopilot = autopilot;
	}
}
