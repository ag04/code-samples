import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Alert,
  Badge,
  Button,
  ButtonGroup,
  Col,
  PageHeader,
  Row
} from 'react-bootstrap';
import GameCreateForm from './game-create-form';
import GameGridView from './game-grid-view-row';

import './game-view.css';
import { CreateForm, GameDetails, RuleSet } from '../../types';

export default class GameViewComponent extends Component {
  static propTypes = {
    onCreateGame: PropTypes.func.isRequired,
    onCreateFormChanged: PropTypes.func.isRequired,
    createForm: CreateForm.isRequired,
    gameDetails: GameDetails,
    createGameErrorMessage: PropTypes.string,
    gameErrorMessage: PropTypes.string,
    ruleSet: RuleSet,
    selectedSalvo: PropTypes.array.isRequired,
    onSelectedSalvoChanged: PropTypes.func.isRequired,
    onShootSalvo: PropTypes.func.isRequired,
    onAutopilotClicked: PropTypes.func.isRequired,
    onRefreshButtonClicked: PropTypes.func.isRequired
  };

  onShootButtonClicked = () => {
    const salvo = {
      gameId: this.props.ruleSet.gameId,
      selectedSalvo: [...this.props.selectedSalvo]
    };
    this.props.onShootSalvo(salvo);
  };

  opponentButtonGroup = ruleSet => {
    let shootButton = null;
    let autoButton = null;
    if (!ruleSet.autopilotOn) {
      if (ruleSet.salvoSize === this.props.selectedSalvo.length) {
        shootButton = (
          <Button
            className="btn btn-outline-primary"
            onClick={this.onShootButtonClicked}
          >
            &#x271C;
          </Button>
        );
      } else {
        shootButton = (
          <Button
            className="btn btn-outline-primary"
            disabled={true}
            onClick={this.onShootButtonClicked}
          >
            &#x271C;
          </Button>
        );
      }
      autoButton = (
        <Button
          className="btn btn-outline-primary"
          onClick={() =>
            this.props.onAutopilotClicked(this.props.ruleSet.gameId)
          }
        >
          &#x2708;
        </Button>
      );
    }
    return (
      <ButtonGroup className="btn-group">
        {shootButton}
        {autoButton}
      </ButtonGroup>
    );
  };

  voidGridCellClicked = (x, y, gridData) => {
    // do nothing - maybe add something smart in future like don't shoot yourself
    return;
  };

  canSelectAnother = () => {
    return this.props.ruleSet.salvoSize > this.props.selectedSalvo.length;
  };

  opponentGridCellClicked = (x, y, data) => {
    if (data === '.') {
      let objectIndex = this.props.selectedSalvo.findIndex(
        field => field.x === x && field.y === y
      );
      if (objectIndex === -1) {
        if (this.canSelectAnother()) {
          let selectedSalvo = [...this.props.selectedSalvo, { x: x, y: y }];
          this.props.onSelectedSalvoChanged(selectedSalvo);
        }
      } else {
        let oldState = [...this.props.selectedSalvo];
        let selectedSalvo = oldState
          .slice(0, objectIndex)
          .concat(oldState.slice(objectIndex + 1, oldState.length));
        this.props.onSelectedSalvoChanged(selectedSalvo);
      }
    }
  };

  render() {
    const {
      onCreateGame,
      onCreateFormChanged,
      gameDetails,
      ruleSet,
      createForm,
      selectedSalvo,
      onRefreshButtonClicked,
      createGameErrorMessage,
      gameErrorMessage
    } = this.props;
    let gameView = (
      <Alert bsStyle="info">
        No games selected. Please select or create new one...
      </Alert>
    );
    if (createGameErrorMessage) {
      gameView = (
        <Alert bsStyle="danger">
          Error creating game: {createGameErrorMessage}
        </Alert>
      );
    } else if (gameDetails) {
      let gameStateInfo = null;
      let gameControlButtons;
      let cellClickedControls;
      let thisTurnAction;

      if (gameErrorMessage) {
        gameStateInfo = (
          <>
            <Alert bsStyle="danger">
              {gameErrorMessage}... try refreshing and trying agian
              <Button
                className="btn btn-outline-success btn-refresh"
                onClick={() => onRefreshButtonClicked(ruleSet.gameId)}
              >
                &#x21BB;
              </Button>
            </Alert>
          </>
        );
      } else if (gameDetails.game.player_turn) {
        if (
          ruleSet.autopilotOn ||
          gameDetails.game.player_turn == gameDetails.opponent.user_id
        ) {
          thisTurnAction = (
            <>
              <Button
                className="btn btn-outline-primary btn-refresh"
                onClick={() => onRefreshButtonClicked(ruleSet.gameId)}
              >
                &#x21BB;
              </Button>
              <br />
              <small>Try refreshing or playing another board...</small>
            </>
          );
          cellClickedControls = this.voidGridCellClicked;
        } else {
          thisTurnAction = (
            <>
              <br />
              <small>
                You have selected{' '}
                <Badge bsClass="badge-pill badge-info">
                  {this.props.selectedSalvo.length}
                </Badge>{' '}
                out of{' '}
                <Badge bsClass="badge-pill badge-success">
                  {ruleSet.salvoSize}
                </Badge>{' '}
                shots
              </small>
            </>
          );
          cellClickedControls = this.opponentGridCellClicked;
        }
        if (!ruleSet.autopilotOn) {
          gameStateInfo = (
            <h5>
              It's{' '}
              <span className="text-primary">
                {gameDetails.game.player_turn}'s
              </span>{' '}
              turn
              {thisTurnAction}
            </h5>
          );
        } else {
          gameStateInfo = (
            <Alert bsStyle="info">
              Autopilot is on just relax - universal answer is 42 (
              {gameDetails.game.player_turn}'s turn) {thisTurnAction}
            </Alert>
          );
        }
        gameControlButtons = this.opponentButtonGroup(ruleSet);
      } else {
        gameStateInfo = (
          <h2>
            Player <span className="text-success">{gameDetails.game.won}</span>{' '}
            won this game
          </h2>
        );
        cellClickedControls = this.voidGridCellClicked;
      }
      gameView = (
        <>
          <Row className="box-mb-20">
            <Col lg={12} className="text-center">
              <h6>Game ID: {ruleSet.gameId}</h6>
              <h3 className="game-title">
                <span className="text-primary">{gameDetails.self.user_id}</span>{' '}
                vs{' '}
                <span className="text-primary">
                  {gameDetails.opponent.user_id}
                </span>
              </h3>
              {gameStateInfo}
            </Col>
          </Row>
          <Row className="text-center">
            <Col xs={12} sm={12} md={6}>
              <GameGridView
                gridTitle="Your grid"
                cellClickedCallback={this.voidGridCellClicked}
                gridData={gameDetails.self.board}
              />
            </Col>

            <Col xs={12} sm={12} md={6}>
              <GameGridView
                gridTitle={`${gameDetails.opponent.user_id}'s grid`}
                gridData={gameDetails.opponent.board}
                selectedSalvo={selectedSalvo}
                buttonGroup={gameControlButtons}
                cellClickedCallback={cellClickedControls}
              />
            </Col>
          </Row>
        </>
      );
    }
    // 2620
    return (
      <div>
        <Row>
          <Col lg={12}>
            <div className="card border-secondary mb-3">
              <div className="card-body">
                <GameCreateForm
                  onCreateGame={onCreateGame}
                  onCreateFormChanged={onCreateFormChanged}
                  createForm={createForm}
                />
              </div>
            </div>
          </Col>
        </Row>
        <Row className="box-mb-20">
          <Col lg={12}>
            <PageHeader>XL Spaceship Console</PageHeader>
          </Col>
        </Row>
        {gameView}
      </div>
    );
  }
}
