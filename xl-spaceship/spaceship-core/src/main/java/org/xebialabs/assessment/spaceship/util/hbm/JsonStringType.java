package org.xebialabs.assessment.spaceship.util.hbm;

import com.vladmihalcea.hibernate.type.json.internal.JacksonUtil;
import com.vladmihalcea.hibernate.type.json.internal.JsonStringSqlTypeDescriptor;
import com.vladmihalcea.hibernate.type.json.internal.JsonTypeDescriptor;

import org.hibernate.HibernateException;
import org.hibernate.type.AbstractSingleColumnStandardBasicType;
import org.hibernate.type.descriptor.WrapperOptions;
import org.hibernate.usertype.DynamicParameterizedType;

import java.io.IOException;
import java.sql.Clob;
import java.sql.SQLException;
import java.util.Properties;

@SuppressWarnings("unused")
public class JsonStringType extends AbstractSingleColumnStandardBasicType<Object> implements DynamicParameterizedType {

	public static final com.vladmihalcea.hibernate.type.json.JsonStringType INSTANCE = new com.vladmihalcea.hibernate.type.json.JsonStringType();

	public JsonStringType() {
		super(JsonStringSqlTypeDescriptor.INSTANCE, new JsonTypeDescriptor() {

			private Class<?> jsonObjectClass;

			@Override
			public void setParameterValues(Properties parameters) {
				this.jsonObjectClass = ((ParameterType) parameters.get(PARAMETER_TYPE)).getReturnedClass();
				super.setParameterValues(parameters);
			}

			@Override
			public <X> Object wrap(X value, WrapperOptions options) {
				if (value == null) {
					return null;
				}
				if (value instanceof Clob) {
					final Clob clob = (Clob) value;
					try {
						return fromStringFix(clob.getSubString(1, (int) clob.length() + 1), this.jsonObjectClass);
					}
					catch (SQLException e) {
						throw new HibernateException("Cannot parse string " + clob, e);
					}
				}
				else {
					return fromString(value.toString());
				}
			}
		});
	}

	public static String toStringFix(Object val) {
		try {
			return JacksonUtil.OBJECT_MAPPER.writeValueAsString(val);
		}
		catch (IOException e) {
			throw new IllegalArgumentException("The given string value: " + val + " cannot be transformed to Json string", e);
		}
	}

	public static <T> T fromStringFix(String string, Class<T> clazz) {
		try {
			return JacksonUtil.OBJECT_MAPPER.readValue(string, clazz);
		}
		catch (IOException e) {
			throw new IllegalArgumentException("The given string value: " + string + " cannot be transformed to Json object", e);
		}
	}

	public String getName() {
		return "json";
	}

	@Override
	protected boolean registerUnderJavaType() {
		return true;
	}

	@Override
	public void setParameterValues(Properties parameters) {
		((JsonTypeDescriptor) getJavaTypeDescriptor()).setParameterValues(parameters);
	}
}
