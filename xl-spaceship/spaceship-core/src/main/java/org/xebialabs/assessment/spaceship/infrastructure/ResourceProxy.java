package org.xebialabs.assessment.spaceship.infrastructure;

import org.xebialabs.assessment.spaceship.application.GameInstanceNotFound;
import org.xebialabs.assessment.spaceship.application.IllegalMoveByOpponentException;
import org.xebialabs.assessment.spaceship.infrastructure.command.FireSalvoCommand;
import org.xebialabs.assessment.spaceship.infrastructure.command.FireSalvoResult;

public interface ResourceProxy {

	FireSalvoResult fireSalvo(String gameId, FireSalvoCommand command) throws IllegalMoveByOpponentException, GameInstanceNotFound;

}
