package org.xebialabs.assessment.spaceship.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.xebialabs.assessment.spaceship.infrastructure.UserResourceProxy;
import org.xebialabs.assessment.spaceship.interfaces.protocol.interceptor.AutopilotInterceptor;

@Configuration
public class RestConfiguration  implements WebMvcConfigurer {

	@Autowired
	private UserResourceProxy userResourceProxy;

	@Bean
	public AutopilotInterceptor autopilotInterceptor() {
		return new AutopilotInterceptor(userResourceProxy);
	}

	@Override
	public void addCorsMappings(CorsRegistry registry) {
		// adding for dev purpose
		registry.addMapping("/xl-spaceship/user/**")
				.allowedOrigins("http://localhost:3001")
				.allowedHeaders("*")
				.allowedMethods(
						HttpMethod.GET.toString(),
						HttpMethod.POST.toString(),
						HttpMethod.PUT.toString(),
						HttpMethod.DELETE.toString(),
						HttpMethod.PATCH.toString(),
						HttpMethod.OPTIONS.toString(),
						HttpMethod.HEAD.toString());
	}

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(autopilotInterceptor()).addPathPatterns("/xl-spaceship/protocol/game/**");
	}
}
