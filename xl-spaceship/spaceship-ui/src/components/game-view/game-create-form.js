import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';

import { Button, Form, FormControl, FormGroup } from 'react-bootstrap';
import { CreateForm } from '../../types';

const RULES_SELECTION = {
  standard: 'Standard',
  desperation: 'Desperation',
  'super-charge': 'Super Charge',
  '1-shot': 'Single Shot',
  '2-shot': 'Double Shot',
  '3-shot': 'Triple Shot',
  '4-shot': 'Quad Shot',
  '5-shot': 'Quint Shot',
  '6-shot': 'Six Shot',
  '7-shot': 'Seven Shot',
  '8-shot': 'Eight Shot',
  '9-shot': 'Nine Shot',
  '10-shot': 'Ten Shot'
};

const createSelectOptions = () => {
  const rowList = Object.entries(RULES_SELECTION).map(([key, value]) => {
    return (
      <option key={key} value={key}>
        {value}
      </option>
    );
  });
  return rowList;
};

export default class GameCreateForm extends Component {
  static propTypes = {
    createForm: CreateForm.isRequired,
    onCreateFormChanged: PropTypes.func.isRequired
  };

  onSubmit = event => {
    event.preventDefault();

    const game = {
      hostname: this.props.createForm.hostname,
      port: this.props.createForm.port,
      rule: this.props.createForm.rule
    };
    this.props.onCreateGame(game);
  };

  inputChangedHandler = (event, inputIdentifier) => {
    const { createForm } = this.props;
    let value;

    if (event.target.type === 'number') {
      value = Number(event.target.value);
    } else {
      value = event.target.value;
    }

    this.props.onCreateFormChanged({
      ...createForm,
      [inputIdentifier]: value
    });
  };

  selectChangedHandler = () => {
    const { createForm } = this.props;
    const rule = this.selectElement.value;

    this.props.onCreateFormChanged({
      ...createForm,
      rule
    });
  };

  componentDidUpdate() {
    if (
      this.messageForm &&
      this.props.createForm.hostname === null &&
      this.props.createForm.port === null &&
      this.props.createForm.rule === null
    ) {
      ReactDOM.findDOMNode(this.messageForm).reset();
    }
  }

  render() {
    return (
      <Form
        className="form-create-game"
        ref={form => (this.messageForm = form)}
        onSubmit={this.onSubmit}
        inline
      >
        <FormGroup controlId="formHorizontalHostname">
          <FormControl
            type="text"
            className="form-control"
            placeholder="Instance hostname"
            onChange={event => this.inputChangedHandler(event, 'hostname')}
          />
        </FormGroup>
        <FormGroup controlId="formHorizontalPort">
          <FormControl
            type="number"
            className="form-control"
            placeholder="Instance port"
            onChange={event => this.inputChangedHandler(event, 'port')}
          />
        </FormGroup>
        <FormGroup controlId="formControlsSelect">
          <FormControl
            componentClass="select"
            className="custom-select"
            placeholder="Rule"
            inputRef={el => (this.selectElement = el)}
            onChange={this.selectChangedHandler}
          >
            {createSelectOptions()}
          </FormControl>
        </FormGroup>
        <FormGroup>
          <Button className="btn btn-outline-primary" type="submit">
            Create Game
          </Button>
        </FormGroup>
      </Form>
    );
  }
}
