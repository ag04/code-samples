package org.xebialabs.assessment.spaceship.application.implementation;

import org.springframework.stereotype.Component;
import org.xebialabs.assessment.spaceship.application.GridService;
import org.xebialabs.assessment.spaceship.domain.model.game.grid.Grid;
import org.xebialabs.assessment.spaceship.domain.model.game.grid.GridFactory;

@Component
public class DefaultGridService implements GridService {

	private final GridFactory gridFactory;

	public DefaultGridService(GridFactory gridFactory) {
		this.gridFactory = gridFactory;
	}

	@Override
	public Grid createEmptyGrid() {
		return gridFactory.createEmptyGrid();
	}

	@Override
	public Grid create() {
		return gridFactory.create();
	}

	@Override
	public String[] drawGrid(Grid grid) {
		// TODO missing implementation for now
		return null;
	}

}
