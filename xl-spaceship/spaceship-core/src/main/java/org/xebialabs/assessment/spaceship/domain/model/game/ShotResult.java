package org.xebialabs.assessment.spaceship.domain.model.game;

public enum ShotResult {
	miss('-'), hit('X'), kill('X');

	private char marker;

	ShotResult(char marker) {
		this.marker = marker;
	}

	public char getMarker() {
		return marker;
	}
}
