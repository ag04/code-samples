package org.xebialabs.assessment.spaceship.application;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.xebialabs.assessment.spaceship.application.implementation.DefaultOpponentService;
import org.xebialabs.assessment.spaceship.domain.model.game.Opponent;
import org.xebialabs.assessment.spaceship.domain.repository.OpponentRepository;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.when;
import static org.mockito.Mockito.mock;

@RunWith(MockitoJUnitRunner.class)
public class OpponentServiceTest {

	OpponentRepository opponentRepository;
	OpponentService opponentService;

	@Before
	public void setUp() {
		opponentRepository = mock(OpponentRepository.class);
		opponentService = new DefaultOpponentService(opponentRepository);
	}

	@Test
	public void testFetchOrCreateOpponent_nonExistingUser() {
		final String testUsername = "username";
		final String testFullName = "Full Name";

		Opponent opponent = new Opponent();
		opponent.setUsername(testUsername);
		opponent.setFullName(testFullName);

		when(opponentRepository.findByUsername(any(String.class))).thenReturn(Optional.empty());
		when(opponentRepository.save(any(Opponent.class))).thenReturn(opponent);

		Opponent retrievedOpponent = opponentService.fetchOrCreateOpponent(testUsername, testFullName);

		Assert.assertEquals(retrievedOpponent.getUsername(), testUsername);
		Assert.assertEquals(retrievedOpponent.getFullName(), testFullName);
	}

	@Test
	public void testFetchOrCreateOpponent_existingUser() {
		final String testUsername = "username";
		final String testFullName = "Full Name";

		Opponent opponent = new Opponent();
		opponent.setUsername(testUsername);
		opponent.setFullName(testFullName);

		when(opponentRepository.findByUsername(any(String.class))).thenReturn(Optional.of(opponent));

		Opponent retrievedOpponent = opponentService.fetchOrCreateOpponent(testUsername, testFullName);

		Assert.assertEquals(retrievedOpponent.getUsername(), testUsername);
		Assert.assertEquals(retrievedOpponent.getFullName(), testFullName);
	}

}
