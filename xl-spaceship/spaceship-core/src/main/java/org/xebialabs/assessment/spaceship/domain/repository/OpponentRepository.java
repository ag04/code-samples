package org.xebialabs.assessment.spaceship.domain.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.xebialabs.assessment.spaceship.domain.model.game.Opponent;

import java.util.Optional;

@SuppressWarnings("unused")
@Repository
public interface OpponentRepository extends CrudRepository<Opponent, Long> {

	Optional<Opponent> findByUsername(String username);

}
