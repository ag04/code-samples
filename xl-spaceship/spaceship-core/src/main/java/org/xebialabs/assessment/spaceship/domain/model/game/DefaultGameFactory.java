package org.xebialabs.assessment.spaceship.domain.model.game;

import org.xebialabs.assessment.spaceship.domain.model.game.grid.Grid;
import org.xebialabs.assessment.spaceship.infrastructure.generator.parameter.ParametersGenerator;

public class DefaultGameFactory implements GameFactory {

	private final ParametersGenerator parametersGenerator;

	public DefaultGameFactory(ParametersGenerator parametersGenerator) {
		this.parametersGenerator = parametersGenerator;
	}

	@Override
	public Game create(String hostname, Long port, Opponent opponent, String gameRule, Grid myGrid, Grid opponentGrid) {
		GameRule rule;
		Integer xNumOfShots = null;
		if (gameRule == null) {
			rule = GameRule.standard;
		}
		else if (gameRule.endsWith("-shot")) {
			String[] split = gameRule.split("-");
			xNumOfShots = Integer.valueOf(split[0]);
			rule = GameRule.Xshot;
		}
		else if (gameRule.equals("super-charge")) {
			rule = GameRule.super_charge;
		}
		else {
			rule = GameRule.valueOf(gameRule);
		}

		return Game.create(hostname, port, opponent, rule, xNumOfShots, myGrid, opponentGrid, parametersGenerator.getMyTurn());
	}

}
