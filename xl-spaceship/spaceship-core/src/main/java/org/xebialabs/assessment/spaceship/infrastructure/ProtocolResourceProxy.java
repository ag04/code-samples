package org.xebialabs.assessment.spaceship.infrastructure;

import org.xebialabs.assessment.spaceship.infrastructure.command.CreateGameCommand;
import org.xebialabs.assessment.spaceship.infrastructure.command.CreateGameResult;

public interface ProtocolResourceProxy extends ResourceProxy {

	CreateGameResult createGame(CreateGameCommand command);

}
