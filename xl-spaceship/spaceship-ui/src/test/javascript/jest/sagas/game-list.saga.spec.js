import {call, put} from 'redux-saga/effects';

import {getGameList} from '../../../../store/services/game-list.service';
import * as saga from '../../../../store/sagas/game-list.saga';
import * as actionTypes from '../../../../store/actions/actionTypes';

import * as constants from '../testConstants';

describe('Game List saga', () => {
    test('should initialize game list', () => {
        const initGameListAction = {
            type: actionTypes.INIT_GAME_LIST
        };
        const gamesResponse = constants.GAME_LIST;
        const initGameListSaga = saga.initGameListSaga(initGameListAction);
        expect(initGameListSaga.next().value).toMatchObject(call(getGameList));
        expect(initGameListSaga.next(gamesResponse).value).toMatchObject(
            put({
                type: actionTypes.SET_GAME_LIST,
                games: gamesResponse
            })
        );
        expect(initGameListSaga.next().done).toBe(true);
    });

    test('should show error', () => {
        const initGameListAction = {
            type: actionTypes.INIT_GAME_LIST
        };
        const gamesResponse = constants.GAME_LIST;
        const initGameListSaga = saga.initGameListSaga(initGameListAction);
        expect(initGameListSaga.next().value).toMatchObject(call(getGameList));
        expect(initGameListSaga.throw({message: 'Error message'}).value).toMatchObject(
            put({
                type: actionTypes.INIT_GAME_LIST_FAILED
            })
        );
        expect(initGameListSaga.next().done).toBe(true);
    });
});
