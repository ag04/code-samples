package org.xebialabs.assessment.spaceship.application.implementation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.xebialabs.assessment.spaceship.application.AutopilotService;
import org.xebialabs.assessment.spaceship.application.GameFinishedException;
import org.xebialabs.assessment.spaceship.application.GameInstanceNotFound;
import org.xebialabs.assessment.spaceship.application.GameService;
import org.xebialabs.assessment.spaceship.application.IllegalMoveByOpponentException;
import org.xebialabs.assessment.spaceship.application.implementation.dto.SalvoDTO;
import org.xebialabs.assessment.spaceship.domain.model.game.Game;
import org.xebialabs.assessment.spaceship.domain.model.game.GameRule;
import org.xebialabs.assessment.spaceship.domain.model.game.ShotResult;
import org.xebialabs.assessment.spaceship.domain.repository.GameRepository;
import org.xebialabs.assessment.spaceship.domain.shared.GridPosition;
import org.xebialabs.assessment.spaceship.infrastructure.generator.salvo.SalvoGenerator;

import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

@Component
public class SimpleAutopilotService implements AutopilotService {

	private static final Logger LOGGER = LoggerFactory.getLogger(SimpleAutopilotService.class);

	private final GameRepository gameRepository;
	private final GameService gameService;

	private final SalvoGenerator salvoGenerator;

	private final Executor executor;

	private final Map<String, GridPosition> lastHitMap = new ConcurrentHashMap<>();

	public SimpleAutopilotService(GameRepository gameRepository, GameService gameService, SalvoGenerator salvoGenerator) {
		this.gameRepository = gameRepository;
		this.gameService = gameService;
		this.salvoGenerator = salvoGenerator;
		this.executor = Executors.newCachedThreadPool();
	}

	@Override
	public void startAutopilot(String gameId) throws GameInstanceNotFound {
		Optional<Game> gameOptional = gameRepository.findGameByUuid(gameId);
		Game game = gameOptional.orElseThrow(GameInstanceNotFound::new);
		LOGGER.info("Initiating autopilot for game {}", game.getUuid());
		game.setOnAutopilot(true);
		gameRepository.save(game);
		triggerAutopilot(game);
	}

	@Override
	public void triggerAutopilotSalvo(String gameId) throws GameInstanceNotFound {
		Optional<Game> gameOptional = gameRepository.findGameByUuid(gameId);
		Game game = gameOptional.orElseThrow(GameInstanceNotFound::new);
		LOGGER.info("Initiating autopilot salvo for game {}", game.getUuid());
		triggerAutopilot(game);
	}

	private void triggerAutopilot(Game game) {
		if (game.isOnAutopilot() && game.isMyTurn() && game.getWinner() == null) {
			executor.execute(() -> {
				LOGGER.debug("Starting salvo execution on autopilot for game {}", game.getUuid());
				try {
					// let it rest for short (just for simulation on my laptop :)
					Thread.sleep(50);
				}
				catch (InterruptedException e) {
					LOGGER.error("unexpected sleep interrupt");
				}

				Set<GridPosition> generatedSalvo = salvoGenerator.generateSalvo(game, lastHitMap.get(game.getUuid()));

				SalvoDTO salvoResult = null;
				try {
					salvoResult = gameService.fireUserSalvo(game.getUuid(), generatedSalvo);

					if (salvoResult.getWon() == null) {
						GridPosition coordinate = salvoResult.getShotResults().entrySet().stream().filter((entry) -> ShotResult.hit.name().equals(entry.getValue())).findFirst().map(entry -> {
							String[] split = entry.getKey().split("x");
							int x = Integer.parseInt(split[0], 16);
							int y = Integer.parseInt(split[1], 16);
							return GridPosition.create(x, y);
						}).orElse(null);

						if (coordinate != null) {
							lastHitMap.put(game.getUuid(), coordinate);
						}
					}
					else {
						lastHitMap.remove(game.getUuid());
					}

					if (game.getGameRule() == GameRule.super_charge) {
						Game newGame = gameRepository.findById(game.getId()).get();
						if (newGame.isMyTurn()) {
							LOGGER.info("Repeating super-charging shot for gameId: {}", game.getUuid());
							triggerAutopilot(newGame);
						}
					}
				}
				catch (IllegalMoveByOpponentException e) {
					LOGGER.warn("Error firing salvo in autopilot game - illegal move: {}", e.getMessage());
					return;
				}
				catch (GameInstanceNotFound gameInstanceNotFound) {
					LOGGER.warn("Error firing salvo in autopilot game - no game instance?");
					return;
				}
				catch (GameFinishedException e) {
					LOGGER.warn("Error firing salvo in autopilot game - illegal move - game finished");
					return;
				}
				LOGGER.info("Autopilot game [{}] salvo shot with result: {}", game.getUuid(), salvoResult);
			});
		}
		else {
			LOGGER.warn("Skipping autopilot trigger game {} state [autopilotFlag: {}, myTurnFlag: {}, gameWinner: {}]",
					game.getUuid(), game.isOnAutopilot(), game.isMyTurn(), game.getWinner());
		}
	}
}
