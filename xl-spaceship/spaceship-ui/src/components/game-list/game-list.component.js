import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Alert,
  Button,
  ListGroup,
  ListGroupItem,
  PageHeader
} from 'react-bootstrap';
import './game-list.css';

const gameEntries = (entryList, onGameSelected) => {
  return entryList.map(entry => (
    <ListGroupItem
      key={entry}
      className="list-group-item list-group-item-action"
      onClick={() => onGameSelected(entry)}
    >
      {entry}
    </ListGroupItem>
  ));
};

export default class GameListComponent extends Component {
  static propTypes = {
    games: PropTypes.array,
    error: PropTypes.bool,
    onGameSelected: PropTypes.func.isRequired,
    onRefreshButtonClicked: PropTypes.func.isRequired
  };

  render() {
    const { games, error, onGameSelected, onRefreshButtonClicked } = this.props;

    let children;
    if (error) {
      children = (
        <Alert bsStyle="danger" className="">
          Failed to fetch games
        </Alert>
      );
    } else if (!Array.isArray(games) || !games.length) {
      children = (
        <Alert bsStyle="info">No created games. Please create one...</Alert>
      );
    } else {
      children = gameEntries(games, onGameSelected);
    }

    return (
      <>
        <PageHeader>
          <small>Games</small>
          <Button
            className="btn btn-outline-primary btn-refresh"
            onClick={onRefreshButtonClicked}
          >
            &#x21BB;
          </Button>
        </PageHeader>
        <hr />
        <ListGroup componentClass="ul">{children}</ListGroup>
      </>
    );
  }
}
