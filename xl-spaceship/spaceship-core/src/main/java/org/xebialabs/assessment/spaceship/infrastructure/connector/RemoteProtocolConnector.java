package org.xebialabs.assessment.spaceship.infrastructure.connector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.xebialabs.assessment.spaceship.application.implementation.dto.GameDTO;
import org.xebialabs.assessment.spaceship.application.implementation.dto.SalvoDTO;
import org.xebialabs.assessment.spaceship.application.mapper.GameDTOMapper;
import org.xebialabs.assessment.spaceship.application.mapper.SalvoMapper;
import org.xebialabs.assessment.spaceship.domain.shared.GridPosition;
import org.xebialabs.assessment.spaceship.infrastructure.GameProtocolError;
import org.xebialabs.assessment.spaceship.infrastructure.command.CreateGameResult;
import org.xebialabs.assessment.spaceship.infrastructure.command.FireSalvoResult;
import org.xebialabs.assessment.spaceship.util.ApplicationConstants;

import java.util.Set;

@Service
public class RemoteProtocolConnector implements ProtocolConnector {

	private static final Logger LOGGER = LoggerFactory.getLogger(RemoteProtocolConnector.class);

	private RestTemplate templateInstance = new RestTemplate();

	@Autowired
	private SalvoMapper salvoMapper;
	@Autowired
	private GameDTOMapper gameDTOMapper;

	private final String PROTOCOL_CONTEXT = ApplicationConstants.PATH_PREFIX + "/protocol/game";


	@Override
	public SalvoDTO fireSalvo(String gameId, String hostname, Long port, Set<GridPosition> salvo) {
		HttpEntity<FireSalvoResult> fireSalvoResult = templateInstance.exchange(
				constructSalvoUri(hostname, port, gameId), HttpMethod.PUT,
				new HttpEntity<>(salvoMapper.salvoListToFireSalvoCommand(salvo)), FireSalvoResult.class);
		SalvoDTO result = salvoMapper.fireSalvoResultToSalvoDto(fireSalvoResult.getBody());

		LOGGER.info("Remote fire salvo resulted with {}", result);
		return result;
	}

	@Override
	public GameDTO createGame(String remoteHostname, Long remotePort, GameDTO gameDTO) throws GameProtocolError {
		try {
			HttpEntity<CreateGameResult> createGameResult = templateInstance.exchange(constructGameUri(remoteHostname, remotePort), HttpMethod.POST,
					new HttpEntity<>(gameDTOMapper.gameDTOToCreateGameCommand(gameDTO)), CreateGameResult.class);

			GameDTO result = gameDTOMapper.createGameResultToGameDTO(createGameResult.getBody());

			LOGGER.info("Remote create game resulted with {}", result);
			return result;
		}
		catch (Exception e) {
			throw new GameProtocolError(e);
		}
	}

	private String constructBaseUri(String hostname, Long port) {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("http://");
		stringBuilder.append(hostname).append(':').append(port).append(PROTOCOL_CONTEXT);
		return stringBuilder.toString();
	}

	private String constructSalvoUri(String hostname, Long port, String gameId) {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(constructBaseUri(hostname, port));
		stringBuilder.append('/').append(gameId);
		return stringBuilder.toString();
	}

	private String constructGameUri(String hostname, Long port) {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(constructBaseUri(hostname, port));
		stringBuilder.append("/new");
		return stringBuilder.toString();
	}
}
