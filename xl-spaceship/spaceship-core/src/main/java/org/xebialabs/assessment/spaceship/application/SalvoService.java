package org.xebialabs.assessment.spaceship.application;

import org.xebialabs.assessment.spaceship.application.implementation.dto.FireResultDTO;
import org.xebialabs.assessment.spaceship.application.implementation.dto.SalvoDTO;
import org.xebialabs.assessment.spaceship.domain.model.game.Game;
import org.xebialabs.assessment.spaceship.domain.shared.GridPosition;

import java.util.Map;
import java.util.Set;

public interface SalvoService {
	FireResultDTO fireOpponentSalvo(Game game, Set<GridPosition> salvo);

	FireResultDTO fireUserSalvo(Game game, Set<GridPosition> salvo);

	SalvoDTO createSalvoResponse(Game game, Map<String, String> salvoShotResults);
}
