import * as actionTypes from './actionTypes';
import { BASE_USER_RESOURCE_URI } from '../../utils/constants';

export const createGame = game => {
  return {
    type: actionTypes.CREATE_GAME,
    game: game
  };
};

export const createGameFailed = error => {
  return {
    type: actionTypes.CREATE_GAME_FAILED,
    errorMessage: error
  };
};

export const createGameFailedOnRemote = message => {
  return {
    type: actionTypes.CREATE_GAME_FAILED_ON_REMOTE,
    errorMessage: message
  };
};

export const createFormChanged = createForm => {
  return {
    type: actionTypes.CREATE_FORM_CHANGED,
    createForm
  };
};

export const resetCreateForm = () => {
  return {
    type: actionTypes.CREATE_FORM_CHANGED,
    createForm: {
      hostname: null,
      port: null,
      rule: null
    }
  };
};

export const selectedSalvoChanged = selectedSalvo => {
  return {
    type: actionTypes.SELECTED_SALVO_CHANGED,
    selectedSalvo
  };
};

export const shootSelectedSalvo = salvo => {
  return {
    type: actionTypes.SHOOT_SELECTED_SALVO,
    salvo
  };
};

export const turnAutopilotOn = gameId => {
  return {
    type: actionTypes.TURN_AUTOPILOT_ON,
    gameId: gameId
  };
};

export const displayGame = gameId => {
  let gameLocation = BASE_USER_RESOURCE_URI + '/' + gameId;
  return {
    type: actionTypes.DISPLAY_GAME,
    gameLocation: gameLocation
  };
};

export const setGameLocation = url => {
  return {
    type: actionTypes.SET_GAME_LOCATION,
    gameLocation: url
  };
};

export const displayingGame = payload => {
  return {
    type: actionTypes.DISPLAYING_GAME,
    gameDetails: payload.gameDetails,
    ruleSet: payload.ruleSet
  };
};

export const displayGameFailed = error => {
  return {
    type: actionTypes.DISPLAY_GAME_FAILED,
    errorMessage: error
  };
};
