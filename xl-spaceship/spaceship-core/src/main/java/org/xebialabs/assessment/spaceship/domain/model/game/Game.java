package org.xebialabs.assessment.spaceship.domain.model.game;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.xebialabs.assessment.spaceship.domain.model.game.grid.Grid;

import java.io.Serializable;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "game")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Game implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@Column(name = "uuid", length = 256, nullable = false)
	private String uuid;

	@Size(min = 1, max = 256)
	@Column(name = "hostname", length = 256, nullable = false)
	private String hostname;

	@NotNull
	@Column(name = "port", nullable = false)
	private Long port;

	@NotNull
	@Column(name = "my_turn", nullable = false)
	private boolean myTurn;

	@Size(min = 1, max = 24)
	@Column(name = "winner", length = 24)
	private String winner;

	@ManyToOne(optional = false)
	@NotNull
	private Opponent opponent;

	@NotNull
	@JoinColumn(name = "opponent_grid_id",  unique = true)
	@OneToOne(cascade = CascadeType.ALL)
	private Grid opponentGrid;

	@NotNull
	@JoinColumn(name = "my_grid_id", unique = true)
	@OneToOne(cascade = CascadeType.ALL)
	private Grid myGrid;

	@Column(name = "game_rule", length = 20, nullable = false)
	@Enumerated(EnumType.STRING)
	private GameRule gameRule;

	@Column(name = "allowed_x_shots")
	private Integer allowedXShots;

	@NotNull
	@Column(name = "on_autopilot", nullable = false)
	private boolean onAutopilot;

	protected Game() { }

	static Game create(String hostname, Long port, Opponent opponent, GameRule rule, Integer allowedShots, Grid myGrid, Grid opponentGrid, boolean myTurn) {
		Game game = new Game();
		game.uuid = UUID.randomUUID().toString();
		game.hostname = hostname;
		game.port = port;
		game.gameRule = rule;
		game.allowedXShots = allowedShots;
		game.opponentGrid = opponentGrid;
		game.myGrid = myGrid;
		game.opponent = opponent;
		game.myTurn = myTurn;
		return game;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getHostname() {
		return hostname;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}

	public Long getPort() {
		return port;
	}

	public void setPort(Long port) {
		this.port = port;
	}

	public Opponent getOpponent() {
		return opponent;
	}

	public void setOpponent(Opponent opponent) {
		this.opponent = opponent;
	}

	public boolean isMyTurn() {
		return myTurn;
	}

	public void setMyTurn(boolean myTurn) {
		this.myTurn = myTurn;
	}

	public String getWinner() {
		return winner;
	}

	public void setWinner(String winner) {
		this.winner = winner;
	}

	public Grid getOpponentGrid() {
		return opponentGrid;
	}

	public void setOpponentGrid(Grid opponentGrid) {
		this.opponentGrid = opponentGrid;
	}

	public Grid getMyGrid() {
		return myGrid;
	}

	public void setMyGrid(Grid myGrid) {
		this.myGrid = myGrid;
	}

	public Integer getAllowedXShots() {
		return allowedXShots;
	}

	public void setAllowedXShots(Integer allowedXShots) {
		this.allowedXShots = allowedXShots;
	}

	public GameRule getGameRule() {
		return gameRule;
	}

	public void setGameRule(GameRule gameRule) {
		this.gameRule = gameRule;
	}

	public boolean isOnAutopilot() {
		return onAutopilot;
	}

	public void setOnAutopilot(boolean onAutopilot) {
		this.onAutopilot = onAutopilot;
	}
}
