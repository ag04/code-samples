import reducer from '../../../../store/reducers/game-list.reducer';

import * as actionTypes from '../../../../store/actions/actionTypes';
import * as constants from '../testConstants';

const initialState = {
  games: null,
  error: false
};

describe('Game List reducer', () => {
  test('should have initial state', () => {
    const action = { type: 'default' };
    const state = reducer(initialState, action);
    expect(state).toMatchObject(initialState);
  });

  test('should add game list when SET_GAME_LIST', () => {
    const games = constants.GAME_LIST;
    const action = { type: actionTypes.SET_GAME_LIST, games: games };
    const state = reducer(initialState, action);
    expect(state.games).toMatchObject(games);
    expect(state.error).toBeFalsy();
  });

  test('should be error when INIT_GAME_LIST_FAILED', () => {
    const action = { type: actionTypes.INIT_GAME_LIST_FAILED };
    const state = reducer(initialState, action);
    expect(state.error).toBeTruthy();
  });
});
