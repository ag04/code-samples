import { call, put } from 'redux-saga/effects';

import { getGameList } from '../services/game-list.service';
import { setGameList, initGameListFailed } from '../actions/game-list.actions';

export function* initGameListSaga(action) {
  try {
    const data = yield call(getGameList);
    yield put(setGameList(data));
  } catch (error) {
    yield put(initGameListFailed());
  }
}
