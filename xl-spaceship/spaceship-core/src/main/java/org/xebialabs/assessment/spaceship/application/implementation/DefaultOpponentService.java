package org.xebialabs.assessment.spaceship.application.implementation;

import org.springframework.stereotype.Component;
import org.xebialabs.assessment.spaceship.application.OpponentService;
import org.xebialabs.assessment.spaceship.domain.model.game.Opponent;
import org.xebialabs.assessment.spaceship.domain.repository.OpponentRepository;

import java.util.Optional;

@Component
public class DefaultOpponentService implements OpponentService {

	private final OpponentRepository opponentRepository;

	public DefaultOpponentService(OpponentRepository opponentRepository) {
		this.opponentRepository = opponentRepository;
	}

	@Override
	public Opponent fetchOrCreateOpponent(String opponentId, String opponentFullName) {
		Optional<Opponent> opponent = opponentRepository.findByUsername(opponentId);

		return opponent.orElseGet(() -> {
						Opponent newOpponent = new Opponent();
						newOpponent.setUsername(opponentId);
						newOpponent.setFullName(opponentFullName);
						return opponentRepository.save(newOpponent);
					});
	}
}
