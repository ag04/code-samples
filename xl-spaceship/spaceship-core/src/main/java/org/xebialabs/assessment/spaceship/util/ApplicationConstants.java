package org.xebialabs.assessment.spaceship.util;

public final class ApplicationConstants {

	private ApplicationConstants() { }

	public static final String PATH_PREFIX = "/xl-spaceship";
}
