package org.xebialabs.assessment.spaceship.infrastructure;

public class GameProtocolError extends Exception {

	public GameProtocolError(Exception e) {
		super(e);
	}

}
