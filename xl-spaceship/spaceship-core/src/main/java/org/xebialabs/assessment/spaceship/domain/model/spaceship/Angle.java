package org.xebialabs.assessment.spaceship.domain.model.spaceship;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("ANGLE")
public class Angle extends Spaceship {

	public Angle() {
		super(SpaceshipType.ANGLE);
	}
}
