import React from 'react';
import { mount } from 'enzyme';
import {
  Alert,
  Button,
  ListGroup,
  ListGroupItem,
  PageHeader
} from 'react-bootstrap';
import '../../setupTests';

import * as constants from '../../testConstants';
import GameViewComponent from '../../../../../components/game-view/game-view.component';
import GameCreateForm from '../../../../../components/game-view/game-create-form';
import GameGridView from '../../../../../components/game-view/game-grid';

const onCreateGame = jest.fn();
const onCreateFormChanged = jest.fn();
const onSelectedSalvoChanged = jest.fn();
const onShootSalvo = jest.fn();
const onAutopilotClicked = jest.fn();
const onRefreshButtonClicked = jest.fn();

describe('Game View component without error', () => {
  const createForm = {
    hostname: null,
    port: null,
    rule: null
  };
  const gameDetails = constants.GAME_DETAILS;
  const ruleSet = constants.RULE_SET;
  const selectedSalvo = [];
  const createGameErrorMessage = null;
  const gameErrorMessage = null;

  const gameListComponent = mount(
    <GameViewComponent
      gameDetails={gameDetails}
      ruleSet={ruleSet}
      onCreateGame={onCreateGame}
      onCreateFormChanged={onCreateFormChanged}
      createForm={createForm}
      selectedSalvo={selectedSalvo}
      onSelectedSalvoChanged={onSelectedSalvoChanged}
      onShootSalvo={onShootSalvo}
      onAutopilotClicked={onAutopilotClicked}
      onRefreshButtonClicked={onRefreshButtonClicked}
      createGameErrorMessage={createGameErrorMessage}
      gameErrorMessage={gameErrorMessage}
    />
  );

  it('it should have create form', () => {
    const createForm = gameListComponent.find(GameCreateForm);
    expect(createForm.exists()).toBeTruthy();
  });
  it('it should have header', () => {
    const header = gameListComponent.find(PageHeader);
    expect(header.exists()).toBeTruthy();
  });
  it('it should have two grids', () => {
    const grids = gameListComponent.find(GameGridView);
    expect(grids.length).toBe(2);
  });
});

describe('Game View component without selected game', () => {
  const createForm = {
    hostname: null,
    port: null,
    rule: null
  };
  const gameDetails = null;
  const ruleSet = null;
  const selectedSalvo = [];
  const createGameErrorMessage = null;
  const gameErrorMessage = null;

  const gameListComponent = mount(
    <GameViewComponent
      gameDetails={gameDetails}
      ruleSet={ruleSet}
      onCreateGame={onCreateGame}
      onCreateFormChanged={onCreateFormChanged}
      createForm={createForm}
      selectedSalvo={selectedSalvo}
      onSelectedSalvoChanged={onSelectedSalvoChanged}
      onShootSalvo={onShootSalvo}
      onAutopilotClicked={onAutopilotClicked}
      onRefreshButtonClicked={onRefreshButtonClicked}
      createGameErrorMessage={createGameErrorMessage}
      gameErrorMessage={gameErrorMessage}
    />
  );

  it('it should show create form', () => {
    const createForm = gameListComponent.find(GameCreateForm);
    expect(createForm.exists()).toBeTruthy();
  });
  it('it should show header', () => {
    const header = gameListComponent.find(PageHeader);
    expect(header.exists()).toBeTruthy();
  });
  it('it should show message no game is selected', () => {
    const alert = gameListComponent.find(Alert);
    expect(alert.exists()).toBeTruthy();
  });
  it('it should have no grids', () => {
    const grids = gameListComponent.find(GameGridView);
    expect(grids.length).toBe(0);
  });
});

describe('Game View component with game create error', () => {
  const createForm = {
    hostname: null,
    port: null,
    rule: null
  };
  const gameDetails = null;
  const ruleSet = null;
  const selectedSalvo = [];
  const createGameErrorMessage = 'Error message';
  const gameErrorMessage = null;

  const gameListComponent = mount(
    <GameViewComponent
      gameDetails={gameDetails}
      ruleSet={ruleSet}
      onCreateGame={onCreateGame}
      onCreateFormChanged={onCreateFormChanged}
      createForm={createForm}
      selectedSalvo={selectedSalvo}
      onSelectedSalvoChanged={onSelectedSalvoChanged}
      onShootSalvo={onShootSalvo}
      onAutopilotClicked={onAutopilotClicked}
      onRefreshButtonClicked={onRefreshButtonClicked}
      createGameErrorMessage={createGameErrorMessage}
      gameErrorMessage={gameErrorMessage}
    />
  );

  it('it should show create form', () => {
    const createForm = gameListComponent.find(GameCreateForm);
    expect(createForm.exists()).toBeTruthy();
  });
  it('it should show header', () => {
    const header = gameListComponent.find(PageHeader);
    expect(header.exists()).toBeTruthy();
  });
  it('it should show message no game is selected', () => {
    const alert = gameListComponent.find(Alert);
    expect(alert.exists()).toBeTruthy();
  });
  it('it should have no grids', () => {
    const grids = gameListComponent.find(GameGridView);
    expect(grids.length).toBe(0);
  });
});

describe('Game View component with game error', () => {
  const createForm = {
    hostname: null,
    port: null,
    rule: null
  };
  const gameDetails = null;
  const ruleSet = null;
  const selectedSalvo = [];
  const createGameErrorMessage = null;
  const gameErrorMessage = 'Error message';

  const gameListComponent = mount(
    <GameViewComponent
      gameDetails={gameDetails}
      ruleSet={ruleSet}
      onCreateGame={onCreateGame}
      onCreateFormChanged={onCreateFormChanged}
      createForm={createForm}
      selectedSalvo={selectedSalvo}
      onSelectedSalvoChanged={onSelectedSalvoChanged}
      onShootSalvo={onShootSalvo}
      onAutopilotClicked={onAutopilotClicked}
      onRefreshButtonClicked={onRefreshButtonClicked}
      createGameErrorMessage={createGameErrorMessage}
      gameErrorMessage={gameErrorMessage}
    />
  );

  it('it should show create form', () => {
    const createForm = gameListComponent.find(GameCreateForm);
    expect(createForm.exists()).toBeTruthy();
  });
  it('it should show header', () => {
    const header = gameListComponent.find(PageHeader);
    expect(header.exists()).toBeTruthy();
  });
  it('it should show message no game is selected', () => {
    const alert = gameListComponent.find(Alert);
    expect(alert.exists()).toBeTruthy();
  });
  it('it should have no grids', () => {
    const grids = gameListComponent.find(GameGridView);
    expect(grids.length).toBe(0);
  });
});
