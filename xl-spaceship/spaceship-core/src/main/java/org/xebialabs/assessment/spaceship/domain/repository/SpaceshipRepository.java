package org.xebialabs.assessment.spaceship.domain.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.xebialabs.assessment.spaceship.domain.model.spaceship.Spaceship;

import java.util.Set;

@SuppressWarnings("unused")
@Repository
public interface SpaceshipRepository extends CrudRepository<Spaceship, Long> {

	Set<Spaceship> findAllByGridId(Long id);

}
