package org.xebialabs.assessment.spaceship.application.mapper;

import org.springframework.stereotype.Service;
import org.xebialabs.assessment.spaceship.application.implementation.dto.GameDTO;
import org.xebialabs.assessment.spaceship.domain.model.game.ShotResult;
import org.xebialabs.assessment.spaceship.domain.shared.GridPosition;
import org.xebialabs.assessment.spaceship.infrastructure.command.GameState;
import org.xebialabs.assessment.spaceship.infrastructure.command.GridStateResult;
import org.xebialabs.assessment.spaceship.infrastructure.command.UserGrid;
import org.xebialabs.assessment.spaceship.util.GridSpecification;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Service
public class GridStateMapper {

	public GridStateResult gameDtoToGridStateResult(GameDTO gameDTO) {
		GridStateResult result = new GridStateResult();

		List<String> selfUserGridRows = createUserGrid(gameDTO.getOwnBoardState(), gameDTO.getStarshipPositions());
		UserGrid selfUserGrid = new UserGrid();
		selfUserGrid.setUserId(gameDTO.getOwnUserId());
		selfUserGrid.setGridRows(selfUserGridRows);
		result.setSlefUserGrid(selfUserGrid);

		List<String> opponentUserGridRows = createUserGrid(gameDTO.getOpponentBoardState(), null);
		UserGrid opponentUserGrid = new UserGrid();
		opponentUserGrid.setUserId(gameDTO.getOpponentId());
		opponentUserGrid.setGridRows(opponentUserGridRows);
		result.setOpponentUserGrid(opponentUserGrid);

		GameState gameState = new GameState();
		gameState.setWon(gameDTO.getWon());
		gameState.setPlayerTurn(gameDTO.getPlayerTurn());
		result.setGameState(gameState);

		return result;
	}


	private List<String> createUserGrid(Map<String, String> shotResultMap, Set<GridPosition> starshipPositions) {
		char[][] array = GridSpecification.getEmptyGridRows();

		if (starshipPositions != null) {
			starshipPositions.stream().forEach(position -> array[position.getX()][position.getY()] = '*');
		}
		shotResultMap.entrySet().stream().forEach(e -> {
			String position = e.getKey();
			String[] splited = position.split("x");
			array[Integer.valueOf(splited[0], 16)][Integer.valueOf(splited[1], 16)] = ShotResult.valueOf(e.getValue()).getMarker();
		});

		List<String> selfGrid = new ArrayList<>();
		for (char[] chars : array) {
			selfGrid.add(new String(chars));
		}
		return selfGrid;
	}
}
