package org.xebialabs.assessment.spaceship.infrastructure;

import org.xebialabs.assessment.spaceship.application.GameInstanceNotFound;
import org.xebialabs.assessment.spaceship.infrastructure.command.CreateUserGameCommand;
import org.xebialabs.assessment.spaceship.infrastructure.command.GridStateResult;
import org.xebialabs.assessment.spaceship.infrastructure.command.RuleSetResult;

import java.util.List;

public interface UserResourceProxy extends ResourceProxy {

	GridStateResult getGridState(String gameId) throws GameInstanceNotFound;

	void startAutopilot(String gameId) throws GameInstanceNotFound;

	void triggerAutopilotSalvo(String gameId);

	String createGame(CreateUserGameCommand command) throws GameProtocolError;

	List<String> getAllGamesIds();

	RuleSetResult getGameRuleSet(String gameId) throws GameInstanceNotFound;
}
