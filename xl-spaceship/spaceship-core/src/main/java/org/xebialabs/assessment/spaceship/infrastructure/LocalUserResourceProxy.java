package org.xebialabs.assessment.spaceship.infrastructure;

import com.google.common.base.Joiner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.xebialabs.assessment.spaceship.application.AutopilotService;
import org.xebialabs.assessment.spaceship.application.GameFinishedException;
import org.xebialabs.assessment.spaceship.application.GameInstanceNotFound;
import org.xebialabs.assessment.spaceship.application.GameService;
import org.xebialabs.assessment.spaceship.application.IllegalMoveByOpponentException;
import org.xebialabs.assessment.spaceship.application.implementation.dto.GameDTO;
import org.xebialabs.assessment.spaceship.application.implementation.dto.SalvoDTO;
import org.xebialabs.assessment.spaceship.application.mapper.GameDTOMapper;
import org.xebialabs.assessment.spaceship.application.mapper.GridStateMapper;
import org.xebialabs.assessment.spaceship.application.mapper.SalvoMapper;
import org.xebialabs.assessment.spaceship.domain.shared.GridPosition;
import org.xebialabs.assessment.spaceship.infrastructure.command.CreateUserGameCommand;
import org.xebialabs.assessment.spaceship.infrastructure.command.FireSalvoCommand;
import org.xebialabs.assessment.spaceship.infrastructure.command.FireSalvoResult;
import org.xebialabs.assessment.spaceship.infrastructure.command.GridStateResult;
import org.xebialabs.assessment.spaceship.infrastructure.command.RuleSetResult;

import java.util.List;
import java.util.Set;

@Component
public class LocalUserResourceProxy implements UserResourceProxy {

	private static final Logger LOGGER = LoggerFactory.getLogger(LocalUserResourceProxy.class);
	private final SalvoMapper salvoMapper;
	private final GameService gameService;

	private final AutopilotService autopilotService;

	private final GameDTOMapper gameDTOMapper;
	private final GridStateMapper gridStateMapper;

	public LocalUserResourceProxy(SalvoMapper salvoMapper, GameDTOMapper gameDTOMapper, GameService gameService, AutopilotService autopilotService,
			GridStateMapper gridStateMapper) {
		this.salvoMapper = salvoMapper;
		this.gameService = gameService;
		this.autopilotService = autopilotService;
		this.gameDTOMapper = gameDTOMapper;
		this.gridStateMapper = gridStateMapper;
	}

	@Override
	public FireSalvoResult fireSalvo(String gameId, FireSalvoCommand command) throws IllegalMoveByOpponentException, GameInstanceNotFound {

		LOGGER.debug("Received User salvo shot on gameId: {}: [{}]", gameId, Joiner.on(", ").join(command.getSalvo()));
		Set<GridPosition> positions = salvoMapper.fireSalvoCommandToGridPositions(command);
		FireSalvoResult result;
		try {
			SalvoDTO salvoDTO = gameService.fireUserSalvo(gameId, positions);
			result = salvoMapper.salvoDtoToFireSalvoResult(salvoDTO);
		}
		catch (GameFinishedException e) {
			SalvoDTO salvoDTO = e.getSalvoResponse();
			result = salvoMapper.salvoDtoToFireSalvoResult(salvoDTO);
			result.setFinished(true);
		}

		LOGGER.info("Processed User salvo shot on gameId: {}: [{}]",
				gameId, Joiner.on(", ").withKeyValueSeparator(": ").join(result.getSalvoResult()));
		return result;
	}

	@Override
	public GridStateResult getGridState(String gameId) throws GameInstanceNotFound {
		GameDTO gameDTO = gameService.getGameState(gameId);

		GridStateResult result = gridStateMapper.gameDtoToGridStateResult(gameDTO);
		return result;
	}

	@Override
	public void startAutopilot(String gameId) throws GameInstanceNotFound {
		autopilotService.startAutopilot(gameId);
	}

	@Override
	public void triggerAutopilotSalvo(String gameId) {
		try {
			autopilotService.triggerAutopilotSalvo(gameId);
		}
		catch (GameInstanceNotFound gameInstanceNotFound) {
			LOGGER.error("Trying to autopilot un-existing game - this is illegal state");
		}
	}

	@Override
	public String createGame(CreateUserGameCommand command) throws GameProtocolError {
		LOGGER.debug("Received Create User Game command: [{}]", command);

		GameDTO gameDTO = gameDTOMapper.createUserGameCommandToGameDTO(command);

		String gameId = gameService.createUserGame(gameDTO);

		LOGGER.info("Processed Create User Game command with result: {}", gameId);
		return gameId;
	}

	@Override
	public List<String> getAllGamesIds() {
		return gameService.getGameIds();
	}

	@Override
	public RuleSetResult getGameRuleSet(String gameId) throws GameInstanceNotFound {
		return gameService.getGameRuleSet(gameId);
	}
}
