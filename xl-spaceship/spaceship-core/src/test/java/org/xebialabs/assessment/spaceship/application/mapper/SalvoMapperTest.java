package org.xebialabs.assessment.spaceship.application.mapper;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.xebialabs.assessment.spaceship.domain.shared.GridPosition;
import org.xebialabs.assessment.spaceship.infrastructure.command.FireSalvoCommand;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

@RunWith(MockitoJUnitRunner.class)
public class SalvoMapperTest {

	SalvoMapper salvoMapper;

	@Before
	public void setUp() {
		this.salvoMapper = new SalvoMapper();
	}

	@Test
	public void fireSalvoCommandToGridPositions_valid() {
		FireSalvoCommand command = new FireSalvoCommand();
		List<String> salvs = Arrays.asList("0x0", "8x4", "DxA", "AxA", "7xF");
		command.setSalvo(salvs);
		Set<GridPosition> positions = salvoMapper.fireSalvoCommandToGridPositions(command);

		Assert.assertNotNull(positions);
		Assert.assertEquals(positions.size(), 5);

		GridPosition gridPosition = GridPosition.create(0, 0);
		Assert.assertTrue(positions.contains(gridPosition));

		gridPosition = GridPosition.create(8, 4);
		Assert.assertTrue(positions.contains(gridPosition));

		gridPosition = GridPosition.create(13, 10);
		Assert.assertTrue(positions.contains(gridPosition));

		gridPosition = GridPosition.create(10, 10);
		Assert.assertTrue(positions.contains(gridPosition));

		gridPosition = GridPosition.create(7, 15);
		Assert.assertTrue(positions.contains(gridPosition));
	}

	@Test(expected = IllegalArgumentException.class)
	public void fireSalvoCommandToGridPositions_invalidSalvo() {
		FireSalvoCommand command = new FireSalvoCommand();
		List<String> salvs = Arrays.asList("00", "8x4", "DxA", "AxA", "7xF");
		command.setSalvo(salvs);
		salvoMapper.fireSalvoCommandToGridPositions(command);
	}

	@Test(expected = NumberFormatException.class)
	public void fireSalvoCommandToGridPositions_invalidHex() {
		FireSalvoCommand command = new FireSalvoCommand();
		List<String> salvs = Arrays.asList("0xZ", "8x4", "DxA", "AxA", "7xF");
		command.setSalvo(salvs);
		salvoMapper.fireSalvoCommandToGridPositions(command);
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void fireSalvoCommandToGridPositions_invalidPostion() {
		FireSalvoCommand command = new FireSalvoCommand();
		List<String> salvs = Arrays.asList("0x10", "8x4", "DxA", "AxA", "7xF");
		command.setSalvo(salvs);
		salvoMapper.fireSalvoCommandToGridPositions(command);
	}
}
