import React from 'react';
import { mount } from 'enzyme';
import { Button, Form, FormGroup, FormControl } from 'react-bootstrap';
import '../../setupTests';

import GameCreateForm from '../../../../../components/game-view/game-create-form';

const onCreateGame = jest.fn();
const onCreateFormChanged = jest.fn();

describe('Game Create Form component', () => {
  const createForm = {
    hostname: null,
    port: null,
    rule: null
  };

  const gameListComponent = mount(
    <GameCreateForm
      onCreateGame={onCreateGame}
      onCreateFormChanged={onCreateFormChanged}
      createForm={createForm}
    />
  );

  it('it should have create form', () => {
    const createForm = gameListComponent.find(Form);
    expect(createForm.exists()).toBeTruthy();
  });
  it('it should have form groups', () => {
    const formGroups = gameListComponent.find(FormGroup);
    expect(formGroups.length).toBe(4);
  });
  it('it should have form controls', () => {
    const formControls = gameListComponent.find(FormControl);
    expect(formControls.length).toBe(3);
  });
  it('it should have button', () => {
    const button = gameListComponent.find(Button);
    expect(button.exists()).toBeTruthy();
  });
});
