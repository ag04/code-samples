package org.xebialabs.assessment.spaceship.interfaces.protocol;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.xebialabs.assessment.spaceship.application.GameInstanceNotFound;
import org.xebialabs.assessment.spaceship.application.IllegalMoveByOpponentException;
import org.xebialabs.assessment.spaceship.infrastructure.ProtocolResourceProxy;
import org.xebialabs.assessment.spaceship.infrastructure.command.CreateGameCommand;
import org.xebialabs.assessment.spaceship.infrastructure.command.CreateGameResult;
import org.xebialabs.assessment.spaceship.infrastructure.command.FireSalvoCommand;
import org.xebialabs.assessment.spaceship.infrastructure.command.FireSalvoResult;
import org.xebialabs.assessment.spaceship.interfaces.protocol.interceptor.AutopilotInterceptor;
import org.xebialabs.assessment.spaceship.util.ApplicationConstants;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@RestController
@RequestMapping(ApplicationConstants.PATH_PREFIX + "/protocol/game")
public class ProtocolResource {

	private static final Logger LOGGER = LoggerFactory.getLogger(ProtocolResource.class);

	private final ProtocolResourceProxy protocolResourceProxy;

	public ProtocolResource(ProtocolResourceProxy protocolResourceProxy) {
		this.protocolResourceProxy = protocolResourceProxy;
	}

	@PostMapping("/new")
	public ResponseEntity<CreateGameResult> createGame(@Valid @RequestBody CreateGameCommand command) {
		return ResponseEntity.status(HttpStatus.CREATED).body(protocolResourceProxy.createGame(command));
	}


	@PutMapping("/{gameId}")
	public ResponseEntity<FireSalvoResult> fireSalvo(
			@Valid @RequestBody FireSalvoCommand command, @NotNull @PathVariable String gameId) throws IllegalMoveByOpponentException, GameInstanceNotFound {
		FireSalvoResult result = protocolResourceProxy.fireSalvo(gameId, command);
		if (result.isFinished()) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(result);
		}

		HttpHeaders responseHeaders = new HttpHeaders();
		if (result.isAutopilot()) {
			responseHeaders.set(AutopilotInterceptor.X_AUTOPILOTED_GAME_ID_HEADER, gameId);
		}
		return new ResponseEntity<>(result, responseHeaders, HttpStatus.OK);
	}

	@ExceptionHandler({ GameInstanceNotFound.class })
	public ResponseEntity<String> handleException(GameInstanceNotFound e) {
		LOGGER.error("Catching exception:", e);
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Game instance not found");
	}

	@ExceptionHandler({ IllegalMoveByOpponentException.class })
	public ResponseEntity<String> handleException(IllegalMoveByOpponentException e) {
		LOGGER.error("Catching exception:", e);
		return ResponseEntity.status(HttpStatus.CONFLICT).body("Not a players turn");
	}

	@ExceptionHandler({ IllegalArgumentException.class })
	public ResponseEntity<String> handleException(IllegalArgumentException e) {
		LOGGER.error("Catching exception:", e);
		return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
	}

	@ExceptionHandler({ Exception.class })
	public ResponseEntity<String> handleException(Exception e) {
		LOGGER.error("Unexpected exception:", e);
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error managing game protocol");
	}
}
