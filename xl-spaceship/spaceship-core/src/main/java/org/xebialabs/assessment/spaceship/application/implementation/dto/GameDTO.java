package org.xebialabs.assessment.spaceship.application.implementation.dto;

import org.xebialabs.assessment.spaceship.domain.shared.GridPosition;

import java.util.Map;
import java.util.Set;
import java.util.StringJoiner;

public class GameDTO {
	private String opponentId;
	private String opponentFullName;

	private String hostname;
	private Long port;

	private String gameId;

	private String ownFullName;
	private String ownUserId;

	private Set<GridPosition> starshipPositions;
	private Map<String, String> ownBoardState;
	private Map<String, String> opponentBoardState;

	private String playerTurn;

	private String starting;
	private String won;

	private String gameRule;

	public GameDTO createdGameDto(String gameId, String starting, String gameRule) {
		this.gameId = gameId;
		this.starting = starting;
		this.gameRule = gameRule;
		return this;
	}

	public String getOpponentId() {
		return opponentId;
	}

	public void setOpponentId(String opponentId) {
		this.opponentId = opponentId;
	}

	public String getOpponentFullName() {
		return opponentFullName;
	}

	public void setOpponentFullName(String opponentFullName) {
		this.opponentFullName = opponentFullName;
	}

	public String getHostname() {
		return hostname;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}

	public Long getPort() {
		return port;
	}

	public void setPort(Long port) {
		this.port = port;
	}

	public String getGameId() {
		return gameId;
	}

	public void setGameId(String gameId) {
		this.gameId = gameId;
	}

	public String getPlayerTurn() {
		return playerTurn;
	}

	public void setPlayerTurn(String playerTurn) {
		this.playerTurn = playerTurn;
	}

	public String getStarting() {
		return starting;
	}

	public void setStarting(String starting) {
		this.starting = starting;
	}

	public String getWon() {
		return won;
	}

	public void setWon(String won) {
		this.won = won;
	}

	public String getOwnUserId() {
		return ownUserId;
	}

	public void setOwnUserId(String ownUserId) {
		this.ownUserId = ownUserId;
	}

	public String getOwnFullName() {
		return ownFullName;
	}

	public void setOwnFullName(String ownFullName) {
		this.ownFullName = ownFullName;
	}

	public Set<GridPosition> getStarshipPositions() {
		return starshipPositions;
	}

	public void setStarshipPositions(Set<GridPosition> starshipPositions) {
		this.starshipPositions = starshipPositions;
	}

	public Map<String, String> getOwnBoardState() {
		return ownBoardState;
	}

	public void setOwnBoardState(Map<String, String> ownBoardState) {
		this.ownBoardState = ownBoardState;
	}

	public Map<String, String> getOpponentBoardState() {
		return opponentBoardState;
	}

	public void setOpponentBoardState(Map<String, String> opponentBoardState) {
		this.opponentBoardState = opponentBoardState;
	}

	public String getGameRule() {
		return gameRule;
	}

	public void setGameRule(String gameRule) {
		this.gameRule = gameRule;
	}

	@Override
	public String toString() {
		return new StringJoiner(", ", GameDTO.class.getSimpleName() + "[", "]").add("opponentId='" + opponentId + "'")
				.add("opponentFullName='" + opponentFullName + "'").add("hostname='" + hostname + "'").add("port=" + port)
				.add("gameId='" + gameId + "'").add("ownUserId='" + ownUserId + "'").add("starshipPositions=" + starshipPositions)
				.add("ownBoardState=" + ownBoardState).add("opponentBoardState=" + opponentBoardState).add("playerTurn='" + playerTurn + "'")
				.add("starting='" + starting + "'").add("won='" + won + "'").add("gameRule='" + gameRule + "'").toString();
	}
}
