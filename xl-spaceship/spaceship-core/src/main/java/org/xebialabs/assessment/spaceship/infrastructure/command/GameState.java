package org.xebialabs.assessment.spaceship.infrastructure.command;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class GameState {

	@JsonProperty("player_turn")
	private String playerTurn;
	private String won;

	public String getPlayerTurn() {
		return playerTurn;
	}

	public void setPlayerTurn(String playerTurn) {
		this.playerTurn = playerTurn;
	}

	public String getWon() {
		return won;
	}

	public void setWon(String won) {
		this.won = won;
	}
}
