package org.xebialabs.assessment.spaceship.domain.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.xebialabs.assessment.spaceship.domain.model.game.Game;

import java.util.Optional;

@SuppressWarnings("unused")
@Repository
public interface GameRepository extends CrudRepository<Game, Long> {

	Optional<Game> findGameByUuid(String uuid);

}
