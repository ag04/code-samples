package org.xebialabs.assessment.spaceship.application.implementation;

import org.springframework.stereotype.Service;
import org.xebialabs.assessment.spaceship.application.GameStrategyProvider;
import org.xebialabs.assessment.spaceship.domain.model.game.Game;
import org.xebialabs.assessment.spaceship.domain.model.game.GameRule;
import org.xebialabs.assessment.spaceship.domain.model.game.ShotResult;

@Service
public class DefaultGameStrategyService implements GameStrategyProvider {

	private final int NUMBER_OF_SHIPS = 5;

	@Override
	public int calculateSalvoSize(Game game) {
		GameRule rule = game.getGameRule();
		if (rule == null) {
			rule = GameRule.standard;
		}

		int numberOfDestroyedShips;
		if (game.isMyTurn()) {
			numberOfDestroyedShips =
					(int) game.getMyGrid().getSpaceships().stream().
									filter(
											spaceship -> spaceship.isDead()
									).count();
		}
		else {
			numberOfDestroyedShips =
					(int) game.getOpponentGrid().getShotResults().values().stream()
							.filter(
									shotResult -> shotResult.equals(ShotResult.kill.name())
							).count();
		}

		switch (rule) {
		case Xshot:
			return game.getAllowedXShots();
		case desperation:
			return 1 + numberOfDestroyedShips;
		case standard:
		case super_charge:
		default:
			return NUMBER_OF_SHIPS - numberOfDestroyedShips;
		}
	}

	@Override
	public boolean calculateUserTurn(Game game, int killsThisTurn) {
		if (game.getGameRule() == GameRule.super_charge && killsThisTurn > 0) {
			return false;
		}
		// my turn is next
		return true;
	}

}
