# XL Spaceship - Battleship game
This is simple show example of peer to peer battleship matchdown based on predefined description of grid, spaceships and set of rules. Two instances of game communicate using defined protocol REST interface.
Protocol interface supports following actions:

* Create new Game
* Fire a Salvo shot on Users grid

User interacts with it's instance using the user REST interface.
User interface supports following actions:

* Create new Game
* Fire a Salvo shot on Opponents grid
* Get Current Game State
* Start Auto-pilot for a Game
* Get Game list
* Get Rule Set for a Game
 
Game is a match between User and Opponent. Game can be created either by User or Opponent. Game has UUID which represents unique match ID shared between XLS instances.

Opponent is defined with it's XLS instance address, userId and full name. 

Each Game contains two grids. One grid describes Users spaceship constelation on a 16x16 square board with set of shot results shoot by Opponent. Other grid describes Opponents 16x16 square board with set of shot results that user shot to it's Opponent. 

User and Opponent take turns in making Salvo shots onto each other based on current game rules. Each shoot inside Salvo results with either miss, hit or kill result. Salvo shot size is defined with game rules.

## Technologies
Technologies used for implementing backend part of this game are Java (and Open Source) based. Fronted is implemented using React.

Here is quick overview of main (not all of them) used frameworks and libraries with their versions used for implementation.

Frontend:

* Ract (16.6.0)
* Ract Redux (5.1.0)
* Ract Bootstrap (0.32.4)
* Redux (4.0.1)
* Redux Saga (0.16.2)
* Axios (0.18.0)
* Webpack (4.19.1)
* Jest (3.8.1)
* Enzyme (3.7.0)


Backend:

* Spring Boot [Spring Web, Spring Data, Spring REST] (2.1.0)
* Hibernate/Hibernate Types
* H2
* Map Struct (1.2.0)
* Google Guava (23.5)
* JUnit (4.12)
* Mockito

### Tech Concepts
Whole application has to be run as simple standalone application. This is accomplished by using Spring Boot whith embeded Tomcat container and H2 database.

To accomplish web application featrues Spring Boot Web, REST, Data JPA and Autoconfigure starters are used.
Spring Boot autodiscovery also provides automatic and out of the box Web UI deployment.

Why H2? Well, acutally implementation is quite DB agnostic and ready to run on any other Database. Since we have to have standalone instance running for the assesment, H2 as embeded database was quite good choice.

Why not In-Memory solution? It seems like much easier approach - and it is. However, JPA support provides future growth possiblity for this to be more robust and "restartable" application.

## Modules
Project is devided into two modules:

* spaceship-core - application backend started as Spring Boot standalone application
* spaceship-ui - React based Frontend build and deployed as a JAR inside Spring Boot "fat-JAR"

## How to build and run
Prerequisit to build and run XLS instance is that you have Java8 installed and in PATH of your Linux/iOS machine.
Basic build is done by running:

`./build.sh`

in project root directory
This script runs `gradle` build with `bootJar` task. This task creates a "fat-JAR" which contains all needed libraries to run standalone XLS instance.
Run XLS instance by starting following script:

`./run.sh`.

This script starts application with default profile.

### Configuration

Following XLS instance configuration paramters can be tempered with to start different XLS instances:

* `--server.port`: change XLS instance port (this should then be changed for GUI too) [default: 8080]
* `--spaceship.grid-factory.type`: can be `random` or `dummy` [default: random]
* `--spaceship.autopilot.type`: can be `smart` or `simple` [default: smart]
* `--spaceship.local-user.user-id`: local user identificator [default: wec]
* `--spaceship.local-user.username`: local user full name [default: "Wile E Coyote"]
* `--spaceship.local-user.hostname`: hostname on which instance is run [default: localhost]

Except the default profile following profiles can be user by applying following switch `--profile=PROFILE_NAME`:

* `xls` (runs on port 8080)
* `et` (runs on port 9090 - can't use GUI)
### Tests
Test are run using gradle test task or when "fat-JAR" is built
## Interfaces
XLS instance has two REST interfaces (user and protocol described in specification) and a Graphical User Interface. 

Graphical user interface covers all user REST interface features with nice, easy to use and simple Web based Graphical User Interface.


