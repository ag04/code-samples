package org.xebialabs.assessment.spaceship.domain.grid;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.xebialabs.assessment.spaceship.domain.model.game.grid.Grid;
import org.xebialabs.assessment.spaceship.domain.model.game.grid.RandomSetGridFactory;
import org.xebialabs.assessment.spaceship.domain.model.spaceship.DefaultSpaceshipFactory;
import org.xebialabs.assessment.spaceship.domain.model.spaceship.SpaceshipFactory;
import org.xebialabs.assessment.spaceship.infrastructure.generator.parameter.ParametersGenerator;
import org.xebialabs.assessment.spaceship.infrastructure.generator.parameter.RandomParametersGenerator;

@RunWith(MockitoJUnitRunner.class)
public class RandomSetGridFacotryTest {

	SpaceshipFactory spaceshipFactory;
	ParametersGenerator parametersGenerator;

	RandomSetGridFactory randomSetGridFactory;

	@Before
	public void setUp() {
		spaceshipFactory = new DefaultSpaceshipFactory();
		parametersGenerator = new RandomParametersGenerator();

		randomSetGridFactory = new RandomSetGridFactory(spaceshipFactory, parametersGenerator);
	}

	/*
	 * 	DISCLAIMER - not a smart thing to test randomness but what the hell
	 *	- let's do it anyway if it fails we know something went wrong somehow?!
	 */
	@Test
	public void testCreate() {
		Grid grid = randomSetGridFactory.create();

		Assert.assertEquals(grid.getSpaceships().size(), 5);
		Assert.assertTrue(grid.getShotResults().isEmpty());
	}
}
