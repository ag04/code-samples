package org.xebialabs.assessment.spaceship.infrastructure.command;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.StringJoiner;

import javax.validation.constraints.NotNull;


public class CreateGameCommand extends CreateUserGameCommand {
	@NotNull
	@JsonProperty("user_id")
	private String userId;

	@JsonProperty("full_name")
	private String fullName;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	@Override
	public String toString() {
		return new StringJoiner(", ", CreateGameCommand.class.getSimpleName() + "[", "]").add("userId='" + userId + "'")
				.add("fullName='" + fullName + "'").add("spaceshipProtocol=" + spaceshipProtocol).add("gameRule='" + gameRule + "'").toString();
	}
}
