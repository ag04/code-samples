package org.xebialabs.assessment.spaceship.application;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.xebialabs.assessment.spaceship.application.implementation.DefaultGridService;
import org.xebialabs.assessment.spaceship.domain.model.game.grid.Grid;
import org.xebialabs.assessment.spaceship.domain.model.game.grid.GridFactory;
import org.xebialabs.assessment.spaceship.domain.model.game.grid.RandomSetGridFactory;
import org.xebialabs.assessment.spaceship.domain.model.spaceship.DefaultSpaceshipFactory;
import org.xebialabs.assessment.spaceship.domain.model.spaceship.Rotation;
import org.xebialabs.assessment.spaceship.domain.model.spaceship.SpaceshipFactory;
import org.xebialabs.assessment.spaceship.domain.shared.GridPosition;
import org.xebialabs.assessment.spaceship.infrastructure.generator.parameter.ParametersGenerator;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

@RunWith(MockitoJUnitRunner.class)
public class GridServiceTest {

	GridFactory gridFactory;
	GridService gridService;

	SpaceshipFactory spaceshipFactory;
	ParametersGenerator parametersGenerator;

	@Before
	public void setUp() {
		parametersGenerator = mock(ParametersGenerator.class);
		spaceshipFactory = new DefaultSpaceshipFactory();
		gridFactory = new RandomSetGridFactory(spaceshipFactory, parametersGenerator);
		gridService = new DefaultGridService(gridFactory);
	}

	@Test
	public void testCreateEmptyGrid() {
		Grid testGrid = gridService.createEmptyGrid();

		Assert.assertNotNull(testGrid.getShotResults());
		Assert.assertTrue(testGrid.getShotResults().isEmpty());

		Assert.assertNotNull(testGrid.getSpaceships());
		Assert.assertTrue(testGrid.getSpaceships().isEmpty());
	}

	@Test
	public void testCreate() {
		GridPosition wingerPosition = GridPosition.create(0, 0);
		GridPosition anglePosition = GridPosition.create(12, 0);
		GridPosition aClassPosition = GridPosition.create(12, 13);
		GridPosition bClassPosition = GridPosition.create(0, 13);
		GridPosition sClassPosition = GridPosition.create(7, 7);

		given(parametersGenerator.getRotation()).willReturn(Rotation.STRAIGHT);
		given(parametersGenerator.getPosition()).willReturn(bClassPosition, sClassPosition, wingerPosition, aClassPosition, anglePosition);

		Grid testGrid = gridService.create();

		Assert.assertNotNull(testGrid.getShotResults());
		Assert.assertTrue(testGrid.getShotResults().isEmpty());

		Assert.assertNotNull(testGrid.getSpaceships());
		Assert.assertTrue(testGrid.getSpaceships().size() == 5);
	}
}
