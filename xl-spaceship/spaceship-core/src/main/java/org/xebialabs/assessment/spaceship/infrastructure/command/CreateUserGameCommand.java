package org.xebialabs.assessment.spaceship.infrastructure.command;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.StringJoiner;

import javax.validation.constraints.NotNull;

public class CreateUserGameCommand {

	@NotNull
	@JsonProperty("spaceship_protocol")
	protected SpaceshipProtocol spaceshipProtocol;

	@JsonProperty("rules")
	@JsonInclude(JsonInclude.Include.NON_NULL)
	protected String gameRule;

	public SpaceshipProtocol getSpaceshipProtocol() {
		return spaceshipProtocol;
	}

	public void setSpaceshipProtocol(SpaceshipProtocol spaceshipProtocol) {
		this.spaceshipProtocol = spaceshipProtocol;
	}

	public String getGameRule() {
		return gameRule;
	}

	public void setGameRule(String gameRule) {
		this.gameRule = gameRule;
	}

	@Override
	public String toString() {
		return new StringJoiner(", ", CreateUserGameCommand.class.getSimpleName() + "[", "]").add("spaceshipProtocol=" + spaceshipProtocol)
				.add("gameRule='" + gameRule + "'").toString();
	}
}
