package org.xebialabs.assessment.spaceship.application.implementation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.xebialabs.assessment.spaceship.application.GameStrategyProvider;
import org.xebialabs.assessment.spaceship.application.SalvoService;
import org.xebialabs.assessment.spaceship.application.SpaceshipService;
import org.xebialabs.assessment.spaceship.application.implementation.dto.FireResultDTO;
import org.xebialabs.assessment.spaceship.application.implementation.dto.SalvoDTO;
import org.xebialabs.assessment.spaceship.application.validator.SalvoValidator;
import org.xebialabs.assessment.spaceship.configuration.ApplicationProperties;
import org.xebialabs.assessment.spaceship.domain.model.game.Game;
import org.xebialabs.assessment.spaceship.domain.model.game.ShotResult;
import org.xebialabs.assessment.spaceship.domain.model.spaceship.Spaceship;
import org.xebialabs.assessment.spaceship.domain.shared.GridPosition;
import org.xebialabs.assessment.spaceship.infrastructure.connector.ProtocolConnector;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Component
public class DefaultSalvoService implements SalvoService {

	private final SpaceshipService spaceshipService;
	private final GameStrategyProvider gameStrategyProvider;
	private final SalvoValidator salvoValidator;

	private final ProtocolConnector protocolConnector;

	private final ApplicationProperties applicationProperties;

	@Autowired
	public DefaultSalvoService(SpaceshipService spaceshipService, GameStrategyProvider gameStrategyProvider, ProtocolConnector protocolConnector,
			ApplicationProperties applicationProperties) {
		this.spaceshipService = spaceshipService;
		this.gameStrategyProvider = gameStrategyProvider;
		this.protocolConnector = protocolConnector;
		this.applicationProperties = applicationProperties;
		salvoValidator = new SalvoValidator(this.gameStrategyProvider);
	}

	@Override
	public FireResultDTO fireOpponentSalvo(Game game, Set<GridPosition> salvo) {
		salvoValidator.validate(salvo, game);
		Map<String, String> shotResults = new HashMap<>();
		List<GridPosition> remainingSalvo = new ArrayList<>();
		Map<String, String> shotPositions = game.getMyGrid().getShotResults();

		markDoubleShotsAsMiss(salvo, shotResults, remainingSalvo, shotPositions);


		Set<Spaceship> updatedSpaceships = new HashSet<>();
		int totalKills = 0;
		int thisTurnKills = 0;
		Set<Spaceship> spaceships = game.getMyGrid().getSpaceships();
		for (Spaceship spaceship : spaceships) {
			if (spaceship.isDead()) {
				totalKills++;
			}
			else {
				Set<GridPosition> currentSpaceshipDamage = new HashSet<>();
				Set<GridPosition> spaceshipFootprint = spaceshipService.getSpaceshipFootprint(spaceship);
				for (GridPosition shot : remainingSalvo) {
					if (spaceshipFootprint.contains(shot)) {
						// it's a hit
						currentSpaceshipDamage.add(shot);
						spaceship.addDamagedPositions(shot);

						ShotResult finalHitResult = ShotResult.hit;
						spaceship.setDead(spaceship.getDamagedPositions().size() == spaceshipFootprint.size());
						if (spaceship.isDead()) {
							finalHitResult = ShotResult.kill;
							totalKills++;
							thisTurnKills++;
						}
						shotResults.put(shot.toString(), finalHitResult.toString());
					}
				}
				remainingSalvo.removeAll(currentSpaceshipDamage);
			}
			updatedSpaceships.add(spaceship);
		}
		markAllTheRestAsMiss(shotResults, remainingSalvo);
		game.getMyGrid().setSpaceships(updatedSpaceships);
		game.getMyGrid().addShotPositions(shotResults);
		game.setMyTurn(gameStrategyProvider.calculateUserTurn(game, thisTurnKills));
		boolean finished = false;
		if (totalKills == spaceships.size()) {
			finished = true;
			game.setWinner(game.getOpponent().getUsername());
		}

		SalvoDTO salvoDTO = createSalvoResponse(game, shotResults);
		return FireResultDTO.create(game, salvoDTO, finished);
	}

	@Override
	public FireResultDTO fireUserSalvo(Game game, Set<GridPosition> salvo) {
		salvoValidator.validate(salvo, game);


		SalvoDTO salvoDTO = protocolConnector.fireSalvo(game.getUuid(), game.getHostname(), game.getPort(), salvo);

		// write result to opponent grid
		Map<String, String> shotPositions = game.getOpponentGrid().getShotResults();
		salvoDTO.getShotResults().forEach(shotPositions::putIfAbsent);
		game.getOpponentGrid().setShotResults(shotPositions);

		boolean finished = false;
		if (salvoDTO.getPlayerTurn() != null) {
			if (salvoDTO.getPlayerTurn().equals(applicationProperties.getLocalUser().getUserId())) {
				game.setMyTurn(true);
			}
			else {
				game.setMyTurn(false);
			}
		}
		else if (salvoDTO.getWon() != null) {
			finished = true;
			game.setWinner(salvoDTO.getWon());
		}
		else {
			throw new IllegalStateException("Response from remote protocol endpoint not valid");
		}

		return FireResultDTO.create(game, salvoDTO, finished);
	}

	@Override
	public SalvoDTO createSalvoResponse(Game game, Map<String, String> salvoShotResults) {
		SalvoDTO salvoDTO = new SalvoDTO();
		salvoDTO.setShotResults(salvoShotResults);

		if (game.getWinner() != null) {
			salvoDTO.setWon(game.getWinner());
		}
		else if (game.isMyTurn()) {
			salvoDTO.setPlayerTurn(applicationProperties.getLocalUser().getUserId());
			salvoDTO.setAutopilot(game.isOnAutopilot());
		}
		else {
			salvoDTO.setPlayerTurn(game.getOpponent().getUsername());
			salvoDTO.setAutopilot(game.isOnAutopilot());
		}

		return salvoDTO;
	}

	private void markDoubleShotsAsMiss(Set<GridPosition> salvo, Map<String, String> shotResults, List<GridPosition> remainingSalvo,
			Map<String, String> shotPositions) {
		for (GridPosition shot : salvo) {
			if (shotPositions.get(shot.toString()) != null && shotPositions.get(shot.toString()).equals(ShotResult.miss.name())) {
				shotResults.put(shot.toString(), ShotResult.miss.name());
			}
			else {
				remainingSalvo.add(shot);
			}
		}
	}

	private void markAllTheRestAsMiss(Map<String, String> shotResults, List<GridPosition> remainingSalvo) {
		for (GridPosition shot : remainingSalvo) {
			shotResults.put(shot.toString(), ShotResult.miss.name());
		}
	}
}
