import axios from '../../axios-default';

import { BASE_USER_RESOURCE_URI } from '../../utils/constants';

export const createGame = createGameCommand => {
  return axios
    .post(BASE_USER_RESOURCE_URI + '/new', createGameCommand)
    .then(response => {
      return {
        data: response.data,
        gameLocation: response.request.responseURL
      };
    });
};

export const getGameRuleSet = gameLocation => {
  return axios.get(gameLocation + '/rule-set').then(response => response.data);
};

export const shootSalvo = (gameId, selectedSalvo) => {
  const url = BASE_USER_RESOURCE_URI + '/' + gameId + '/fire';
  const salvo = {
    salvo: selectedSalvo.map(
      value =>
        value.x.toString(16).toUpperCase() +
        'x' +
        value.y.toString(16).toUpperCase()
    )
  };
  return axios.put(url, salvo);
};

export const turnAutopilotOn = gameId => {
  const url = BASE_USER_RESOURCE_URI + '/' + gameId + '/auto';
  return axios.post(url);
};

export const fetchGameData = gameLocation => {
  return axios.get(gameLocation).then(response => response.data);
};
