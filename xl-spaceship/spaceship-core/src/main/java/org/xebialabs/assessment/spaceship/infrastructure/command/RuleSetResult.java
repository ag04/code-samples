package org.xebialabs.assessment.spaceship.infrastructure.command;

public class RuleSetResult {
	private String gameId;
	private String rules;
	private Integer salvoSize;
	private boolean autopilotOn;

	public String getGameId() {
		return gameId;
	}

	public void setGameId(String gameId) {
		this.gameId = gameId;
	}

	public String getRules() {
		return rules;
	}

	public void setRules(String rules) {
		this.rules = rules;
	}

	public Integer getSalvoSize() {
		return salvoSize;
	}

	public void setSalvoSize(Integer salvoSize) {
		this.salvoSize = salvoSize;
	}

	public boolean isAutopilotOn() {
		return autopilotOn;
	}

	public void setAutopilotOn(boolean autopilotOn) {
		this.autopilotOn = autopilotOn;
	}
}
