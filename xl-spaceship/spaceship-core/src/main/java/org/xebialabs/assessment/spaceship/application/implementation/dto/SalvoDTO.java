package org.xebialabs.assessment.spaceship.application.implementation.dto;

import java.util.Map;

public class SalvoDTO {

	private Map<String, String> shotResults;
	private String playerTurn;
	private String won;

	private boolean autopilot;

	public Map<String, String> getShotResults() {
		return shotResults;
	}

	public void setShotResults(Map<String, String> shotResults) {
		this.shotResults = shotResults;
	}

	public String getPlayerTurn() {
		return playerTurn;
	}

	public void setPlayerTurn(String playerTurn) {
		this.playerTurn = playerTurn;
	}

	public String getWon() {
		return won;
	}

	public void setWon(String won) {
		this.won = won;
	}

	public boolean isAutopilot() {
		return autopilot;
	}

	public void setAutopilot(boolean autopilot) {
		this.autopilot = autopilot;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("SalvoDTO{");
		sb.append("shotResults=").append(shotResults);
		sb.append(", playerTurn='").append(playerTurn).append('\'');
		sb.append(", won='").append(won).append('\'');
		sb.append(", autopilot=").append(autopilot);
		sb.append('}');
		return sb.toString();
	}


}
