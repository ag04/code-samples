package org.xebialabs.assessment.spaceship;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpaceshipApplication.class)
public class SpaceshipApplicationTests {

	@Test
	public void contextLoads() {
	}

}
