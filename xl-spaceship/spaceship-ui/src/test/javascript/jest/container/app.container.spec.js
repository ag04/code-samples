import React from 'react';
import { shallow } from 'enzyme';
import '../setupTests';

import AppContainer from '../../../../components/app.container';
import {
  createGame,
  displayGame,
  createFormChanged,
  selectedSalvoChanged,
  shootSelectedSalvo,
  turnAutopilotOn
} from '../../../../store/actions/game.actions';
import { initGameList } from '../../../../store/actions/game-list.actions';

describe('Application container', () => {
  const createStore = state => ({
    dispatch: jest.fn(),
    getState: () => state,
    subscribe: () => {}
  });

  test('should map state to props correcty', () => {
    const gameInitialState = {
      gameLocation: null,
      gameDetails: null,
      createForm: {
        hostname: null,
        port: null,
        rule: null
      },
      selectedSalvo: [],
      createGameErrorMessage: null,
      gameErrorMessage: null
    };
    const gameListInitialState = {
      games: [],
      error: false
    };
    const store = createStore({
      gameList: gameListInitialState,
      game: gameInitialState
    });
    const container = shallow(<AppContainer store={store} />);
    expect(Object.entries(container.props())).toEqual(
      expect.arrayContaining(Object.entries(gameInitialState))
    );
    expect(Object.entries(container.props())).toEqual(
      expect.arrayContaining(Object.entries(gameListInitialState))
    );
  });

  test('should map dispatch to props correctly', () => {
    const gameInitialState = {
      gameLocation: null,
      gameDetails: null,
      createForm: {
        hostname: null,
        port: null,
        rule: null
      },
      selectedSalvo: [],
      createGameErrorMessage: null,
      gameErrorMessage: null
    };
    const gameListInitialState = {
      games: null,
      error: false
    };
    const store = createStore({
      gameList: gameListInitialState,
      game: gameInitialState
    });

    const container = shallow(<AppContainer store={store} />);
    const containerProps = container.props();
    containerProps.onInit();
    expect(store.dispatch).toBeCalledWith(initGameList());
    containerProps.onRefreshListButtonClicked();
    expect(store.dispatch).toBeCalledWith(initGameList());
    containerProps.onCreateGame();
    expect(store.dispatch).toBeCalledWith(createGame());
    containerProps.onGameSelected();
    expect(store.dispatch).toBeCalledWith(displayGame());
    containerProps.onCreateFormChanged();
    expect(store.dispatch).toBeCalledWith(createFormChanged());
    containerProps.onSelectedSalvoChanged();
    expect(store.dispatch).toBeCalledWith(selectedSalvoChanged());
    containerProps.onShootSalvo();
    expect(store.dispatch).toBeCalledWith(shootSelectedSalvo());
    containerProps.onAutopilotClicked();
    expect(store.dispatch).toBeCalledWith(turnAutopilotOn());
    containerProps.onRefreshButtonClicked();
    expect(store.dispatch).toBeCalledWith(displayGame());
    containerProps.onRefreshButtonClicked();
    expect(store.dispatch).toBeCalledWith(displayGame());
  });
});
