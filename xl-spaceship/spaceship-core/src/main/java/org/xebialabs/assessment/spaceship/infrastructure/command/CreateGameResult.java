package org.xebialabs.assessment.spaceship.infrastructure.command;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.StringJoiner;

import javax.validation.constraints.NotNull;

public class CreateGameResult {
	@NotNull
	@JsonProperty("user_id")
	private String userId;

	@JsonProperty("full_name")
	private String fullName;

	@JsonProperty("game_id")
	private String gameId;

	private String starting;

	@JsonProperty("rules")
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String gameRule;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getGameId() {
		return gameId;
	}

	public void setGameId(String gameId) {
		this.gameId = gameId;
	}

	public String getStarting() {
		return starting;
	}

	public void setStarting(String starting) {
		this.starting = starting;
	}

	public String getGameRule() {
		return gameRule;
	}

	public void setGameRule(String gameRule) {
		this.gameRule = gameRule;
	}

	@Override
	public String toString() {
		return new StringJoiner(", ", CreateGameResult.class.getSimpleName() + "[", "]").add("userId='" + userId + "'")
				.add("fullName='" + fullName + "'").add("gameId='" + gameId + "'").add("starting='" + starting + "'")
				.add("gameRule='" + gameRule + "'").toString();
	}
}
