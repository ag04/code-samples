package org.xebialabs.assessment.spaceship.application.implementation;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.xebialabs.assessment.spaceship.domain.model.game.Game;
import org.xebialabs.assessment.spaceship.domain.model.game.GameRule;
import org.xebialabs.assessment.spaceship.domain.model.game.grid.Grid;
import org.xebialabs.assessment.spaceship.domain.model.spaceship.Spaceship;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.when;
import static org.mockito.Mockito.mock;

@RunWith(MockitoJUnitRunner.class)
public class DefaultGameStrategyServiceTest {

	DefaultGameStrategyService gameStrategyService;
	Game game;
	Grid myGrid;
	Grid opponentGrid;

	@Before
	public void setUp() throws Exception {
		this.gameStrategyService = new DefaultGameStrategyService();
		this.game = mock(Game.class);
		this.myGrid = mock(Grid.class);
		this.opponentGrid = mock(Grid.class);
	}

	@Test
	public void calculateSalvoSize_nullRule() {
		Spaceship one = mock(Spaceship.class);
		Spaceship two = mock(Spaceship.class);
		Spaceship three = mock(Spaceship.class);
		Spaceship four = mock(Spaceship.class);
		Spaceship five = mock(Spaceship.class);
		Set<Spaceship> spaceshipSet = new HashSet<>();
		spaceshipSet.add(one);
		spaceshipSet.add(two);
		spaceshipSet.add(three);
		spaceshipSet.add(four);
		spaceshipSet.add(five);
		given(myGrid.getSpaceships()).willReturn(spaceshipSet);

		Map<String, String> shotResult = new HashMap<>();
		shotResult.put("0x0", "miss");
		shotResult.put("0x1", "hit");
		shotResult.put("0x2", "hit");
		shotResult.put("0x3", "miss");
		shotResult.put("0x4", "miss");

		when(opponentGrid.getShotResults()).thenReturn(shotResult);
		when(game.getGameRule()).thenReturn(null);
		when(game.getMyGrid()).thenReturn(myGrid);
		when(game.getOpponentGrid()).thenReturn(opponentGrid);

		int opponentTrunResult = gameStrategyService.calculateSalvoSize(game);
		Assert.assertEquals(5, opponentTrunResult);

		when(game.isMyTurn()).thenReturn(true);
		int myTrunResult = gameStrategyService.calculateSalvoSize(game);
		Assert.assertEquals(5, opponentTrunResult);
	}

	@Test
	public void calculateSalvoSize_standardRule_noKills() {
		Spaceship one = mock(Spaceship.class);
		Spaceship two = mock(Spaceship.class);
		Spaceship three = mock(Spaceship.class);
		Spaceship four = mock(Spaceship.class);
		Spaceship five = mock(Spaceship.class);
		Set<Spaceship> spaceshipSet = new HashSet<>();
		spaceshipSet.add(one);
		spaceshipSet.add(two);
		spaceshipSet.add(three);
		spaceshipSet.add(four);
		spaceshipSet.add(five);
		given(myGrid.getSpaceships()).willReturn(spaceshipSet);

		Map<String, String> shotResult = new HashMap<>();
		shotResult.put("0x0", "miss");
		shotResult.put("0x1", "hit");
		shotResult.put("0x2", "hit");
		shotResult.put("0x3", "miss");
		shotResult.put("0x4", "miss");

		when(opponentGrid.getShotResults()).thenReturn(shotResult);
		when(game.getGameRule()).thenReturn(GameRule.standard);
		when(game.getMyGrid()).thenReturn(myGrid);
		when(game.getOpponentGrid()).thenReturn(opponentGrid);

		int opponentTrunResult = gameStrategyService.calculateSalvoSize(game);
		Assert.assertEquals(5, opponentTrunResult);

		when(game.isMyTurn()).thenReturn(true);
		int myTrunResult = gameStrategyService.calculateSalvoSize(game);
		Assert.assertEquals(5, opponentTrunResult);
	}

	@Test
	public void calculateSalvoSize_standardRule_threeKills() {
		Spaceship one = mock(Spaceship.class);
		Spaceship two = mock(Spaceship.class);
		Spaceship three = mock(Spaceship.class);
		Spaceship four = mock(Spaceship.class);
		Spaceship five = mock(Spaceship.class);
		when(three.isDead()).thenReturn(true);
		when(two.isDead()).thenReturn(true);
		when(one.isDead()).thenReturn(true);
		Set<Spaceship> spaceshipSet = new HashSet<>();
		spaceshipSet.add(one);
		spaceshipSet.add(two);
		spaceshipSet.add(three);
		spaceshipSet.add(four);
		spaceshipSet.add(five);
		given(myGrid.getSpaceships()).willReturn(spaceshipSet);

		Map<String, String> shotResult = new HashMap<>();
		shotResult.put("0x0", "kill");
		shotResult.put("0x1", "hit");
		shotResult.put("0x2", "hit");
		shotResult.put("0x3", "kill");
		shotResult.put("0x4", "kill");

		when(opponentGrid.getShotResults()).thenReturn(shotResult);
		when(game.getGameRule()).thenReturn(GameRule.standard);
		when(game.getMyGrid()).thenReturn(myGrid);
		when(game.getOpponentGrid()).thenReturn(opponentGrid);

		int opponentTrunResult = gameStrategyService.calculateSalvoSize(game);
		Assert.assertEquals(2, opponentTrunResult);

		when(game.isMyTurn()).thenReturn(true);
		int myTrunResult = gameStrategyService.calculateSalvoSize(game);
		Assert.assertEquals(2, opponentTrunResult);
	}

	@Test
	public void calculateSalvoSize_7shotRule() {
		Spaceship one = mock(Spaceship.class);
		Spaceship two = mock(Spaceship.class);
		Spaceship three = mock(Spaceship.class);
		Spaceship four = mock(Spaceship.class);
		Spaceship five = mock(Spaceship.class);
		when(three.isDead()).thenReturn(true);
		Set<Spaceship> spaceshipSet = new HashSet<>();
		spaceshipSet.add(one);
		spaceshipSet.add(two);
		spaceshipSet.add(three);
		spaceshipSet.add(four);
		spaceshipSet.add(five);
		given(myGrid.getSpaceships()).willReturn(spaceshipSet);

		Map<String, String> shotResult = new HashMap<>();
		shotResult.put("0x0", "kill");
		shotResult.put("0x1", "hit");
		shotResult.put("0x2", "hit");
		shotResult.put("0x3", "miss");
		shotResult.put("0x4", "miss");

		when(opponentGrid.getShotResults()).thenReturn(shotResult);
		when(game.getGameRule()).thenReturn(GameRule.Xshot);
		when(game.getAllowedXShots()).thenReturn(7);
		when(game.getMyGrid()).thenReturn(myGrid);
		when(game.getOpponentGrid()).thenReturn(opponentGrid);

		int opponentTrunResult = gameStrategyService.calculateSalvoSize(game);
		Assert.assertEquals(7, opponentTrunResult);

		when(game.isMyTurn()).thenReturn(true);
		int myTrunResult = gameStrategyService.calculateSalvoSize(game);
		Assert.assertEquals(7, opponentTrunResult);
	}

	@Test
	public void calculateSalvoSize_superChargeRule() {
		Spaceship one = mock(Spaceship.class);
		Spaceship two = mock(Spaceship.class);
		Spaceship three = mock(Spaceship.class);
		Spaceship four = mock(Spaceship.class);
		Spaceship five = mock(Spaceship.class);
		when(three.isDead()).thenReturn(true);
		Set<Spaceship> spaceshipSet = new HashSet<>();
		spaceshipSet.add(one);
		spaceshipSet.add(two);
		spaceshipSet.add(three);
		spaceshipSet.add(four);
		spaceshipSet.add(five);
		given(myGrid.getSpaceships()).willReturn(spaceshipSet);

		Map<String, String> shotResult = new HashMap<>();
		shotResult.put("0x0", "kill");
		shotResult.put("0x1", "hit");
		shotResult.put("0x2", "hit");
		shotResult.put("0x3", "miss");
		shotResult.put("0x4", "miss");

		when(opponentGrid.getShotResults()).thenReturn(shotResult);
		when(game.getGameRule()).thenReturn(GameRule.super_charge);
		when(game.getMyGrid()).thenReturn(myGrid);
		when(game.getOpponentGrid()).thenReturn(opponentGrid);

		int opponentTrunResult = gameStrategyService.calculateSalvoSize(game);
		Assert.assertEquals(4, opponentTrunResult);

		when(game.isMyTurn()).thenReturn(true);
		int myTrunResult = gameStrategyService.calculateSalvoSize(game);
		Assert.assertEquals(4, opponentTrunResult);
	}

	@Test
	public void calculateSalvoSize_desperationRule_withKills() {
		Spaceship one = mock(Spaceship.class);
		Spaceship two = mock(Spaceship.class);
		Spaceship three = mock(Spaceship.class);
		Spaceship four = mock(Spaceship.class);
		Spaceship five = mock(Spaceship.class);

		when(three.isDead()).thenReturn(true);
		when(five.isDead()).thenReturn(true);
		Set<Spaceship> spaceshipSet = new HashSet<>();
		spaceshipSet.add(one);
		spaceshipSet.add(two);
		spaceshipSet.add(three);
		spaceshipSet.add(four);
		spaceshipSet.add(five);
		given(myGrid.getSpaceships()).willReturn(spaceshipSet);

		Map<String, String> shotResult = new HashMap<>();
		shotResult.put("0x0", "kill");
		shotResult.put("0x1", "hit");
		shotResult.put("0x2", "hit");
		shotResult.put("0x3", "miss");
		shotResult.put("0x4", "kill");

		when(opponentGrid.getShotResults()).thenReturn(shotResult);
		when(game.getGameRule()).thenReturn(GameRule.desperation);
		when(game.getMyGrid()).thenReturn(myGrid);
		when(game.getOpponentGrid()).thenReturn(opponentGrid);

		int opponentTrunResult = gameStrategyService.calculateSalvoSize(game);
		Assert.assertEquals(3, opponentTrunResult);

		when(game.isMyTurn()).thenReturn(true);
		int myTrunResult = gameStrategyService.calculateSalvoSize(game);
		Assert.assertEquals(3, opponentTrunResult);
	}

	@Test
	public void calculateSalvoSize_desperationRule_noKills() {
		Spaceship one = mock(Spaceship.class);
		Spaceship two = mock(Spaceship.class);
		Spaceship three = mock(Spaceship.class);
		Spaceship four = mock(Spaceship.class);
		Spaceship five = mock(Spaceship.class);
		Set<Spaceship> spaceshipSet = new HashSet<>();
		spaceshipSet.add(one);
		spaceshipSet.add(two);
		spaceshipSet.add(three);
		spaceshipSet.add(four);
		spaceshipSet.add(five);
		given(myGrid.getSpaceships()).willReturn(spaceshipSet);

		Map<String, String> shotResult = new HashMap<>();
		shotResult.put("0x0", "hit");

		when(opponentGrid.getShotResults()).thenReturn(shotResult);
		when(game.getGameRule()).thenReturn(GameRule.desperation);
		when(game.getMyGrid()).thenReturn(myGrid);
		when(game.getOpponentGrid()).thenReturn(opponentGrid);

		int opponentTrunResult = gameStrategyService.calculateSalvoSize(game);
		Assert.assertEquals(1, opponentTrunResult);

		when(game.isMyTurn()).thenReturn(true);
		int myTrunResult = gameStrategyService.calculateSalvoSize(game);
		Assert.assertEquals(1, opponentTrunResult);
	}

	@Test
	public void calculateUserTurn_normal() {
		when(game.getGameRule()).thenReturn(GameRule.standard);
		Assert.assertTrue(gameStrategyService.calculateUserTurn(game, 2));
	}

	@Test
	public void calculateUserTurn_superCharge_noKills() {
		when(game.getGameRule()).thenReturn(GameRule.super_charge);
		Assert.assertTrue(gameStrategyService.calculateUserTurn(game, 0));
	}

	@Test
	public void calculateUserTurn_superCharge_withKills() {
		when(game.getGameRule()).thenReturn(GameRule.super_charge);
		Assert.assertFalse(gameStrategyService.calculateUserTurn(game, 2));
	}
}
