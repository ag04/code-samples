package org.xebialabs.assessment.spaceship.infrastructure.generator.salvo;

import org.xebialabs.assessment.spaceship.application.GameStrategyProvider;
import org.xebialabs.assessment.spaceship.domain.model.game.Game;
import org.xebialabs.assessment.spaceship.domain.shared.GridPosition;
import org.xebialabs.assessment.spaceship.infrastructure.generator.parameter.ParametersGenerator;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public abstract class AbstractSalvoGenerator implements SalvoGenerator {
	private final GameStrategyProvider gameStrategyProvider;
	protected final ParametersGenerator parametersGenerator;

	public AbstractSalvoGenerator(GameStrategyProvider gameStrategyProvider, ParametersGenerator parametersGenerator) {
		this.gameStrategyProvider = gameStrategyProvider;
		this.parametersGenerator = parametersGenerator;
	}

	@Override
	public Set<GridPosition> generateSalvo(Game game, GridPosition lastHit) {
		int salvoSize = gameStrategyProvider.calculateSalvoSize(game);

		Map<String, String> shotResults = game.getOpponentGrid().getShotResults();
		Set<GridPosition> salvo = new HashSet<GridPosition>(salvoSize);
		while (salvo.size() < salvoSize) {
			GridPosition newShot = nextShotPosition(shotResults, salvo, lastHit);
			salvo.add(newShot);
		}

		return salvo;
	}

	protected GridPosition getSequentialGridPosition(Map<String, String> shotResults, Set<GridPosition> currentSalvo) {
		for (int i = 0; i < 16; i++) {
			for (int j = 0; j < 16; j++) {
				GridPosition currentPosition = GridPosition.create(i, j);
				if (shotResults.get(currentPosition.toString()) == null && !currentSalvo.contains(currentPosition)) {
					return currentPosition;
				}
			}
		}
		throw new IllegalStateException("This should not reach this point if game is valid");
	}

	protected abstract GridPosition nextShotPosition(Map<String, String> shotResults, Set<GridPosition> currentSalvo, GridPosition lastHit);
}
