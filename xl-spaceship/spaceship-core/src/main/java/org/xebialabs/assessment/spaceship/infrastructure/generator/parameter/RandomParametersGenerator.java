package org.xebialabs.assessment.spaceship.infrastructure.generator.parameter;

import org.springframework.stereotype.Component;
import org.xebialabs.assessment.spaceship.domain.model.spaceship.Rotation;
import org.xebialabs.assessment.spaceship.domain.shared.GridPosition;
import org.xebialabs.assessment.spaceship.util.GridSpecification;

@Component
public class RandomParametersGenerator implements ParametersGenerator {

	@Override
	public Rotation getRotation() {
		int rotation = (int) (Math.random() * 4);
		return Rotation.values()[rotation];
	}

	@Override
	public GridPosition getPosition() {
		int x = (int) (Math.random() * GridSpecification.GRID_HEIGHT);
		int y = (int) (Math.random() * GridSpecification.GRID_WIDTH);
		return GridPosition.create(x, y);
	}

	@Override
	public boolean getMyTurn() {
		return (Math.round(Math.random()) == 0L);
	}
}
