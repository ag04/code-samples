package org.xebialabs.assessment.spaceship.application;

import org.xebialabs.assessment.spaceship.domain.model.spaceship.Spaceship;
import org.xebialabs.assessment.spaceship.domain.shared.GridPosition;

import java.util.Set;

public interface SpaceshipService {

	Set<GridPosition> getSpaceshipFootprint(Spaceship spaceship);

}
