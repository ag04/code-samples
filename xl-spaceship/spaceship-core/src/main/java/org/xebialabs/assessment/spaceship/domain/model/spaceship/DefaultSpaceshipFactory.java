package org.xebialabs.assessment.spaceship.domain.model.spaceship;

import org.springframework.data.util.Pair;
import org.xebialabs.assessment.spaceship.domain.shared.GridPosition;
import org.xebialabs.assessment.spaceship.util.GridSpecification;
import org.xebialabs.assessment.spaceship.util.SpaceshipSpecification;

import java.util.Set;
import java.util.stream.Collectors;

public class DefaultSpaceshipFactory implements SpaceshipFactory {

	@Override
	public Spaceship create(SpaceshipType type, Rotation rotation, GridPosition position) throws SpaceshipPositionOutOfBounds {
		if (!validateSpaceshipPosition(type, rotation, position)) {
			throw new SpaceshipPositionOutOfBounds();
		}
		return Spaceship.create(type, rotation, position);
	}

	@Override
	public Set<GridPosition> calculateSpaceshipCoordinates(Spaceship spaceship) {
		return SpaceshipSpecification.getSpaceshipSpecification(spaceship.getType()).stream().map(
						position -> GridPosition.create(
								spaceship.getRotation().calculateX(position.getX(), position.getY()) + spaceship.getPosition().getX(),
							spaceship.getRotation().calculateY(position.getX(), position.getY()) + spaceship.getPosition().getY()))
						.collect(Collectors.toSet());
	}

	private boolean validateSpaceshipPosition(SpaceshipType type, Rotation rotation, GridPosition gridPosition) {
		Pair<Integer, Integer> dimensions = SpaceshipSpecification.getSpaceshipDimensions(type);
		int diagonalXPosition = rotation.calculateX(dimensions.getFirst(), dimensions.getSecond()) + gridPosition.getX();
		int diagonalYPosition = rotation.calculateY(dimensions.getFirst(), dimensions.getSecond()) + gridPosition.getY();

		return diagonalXPosition >= 0 && diagonalXPosition < GridSpecification.GRID_HEIGHT && diagonalYPosition >= 0
				&& diagonalYPosition < GridSpecification.GRID_WIDTH;
	}
}
