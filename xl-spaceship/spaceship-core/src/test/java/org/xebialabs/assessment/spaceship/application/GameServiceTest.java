package org.xebialabs.assessment.spaceship.application;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.core.env.Environment;
import org.xebialabs.assessment.spaceship.application.implementation.DefaultGridService;
import org.xebialabs.assessment.spaceship.application.implementation.DefaultSalvoService;
import org.xebialabs.assessment.spaceship.application.implementation.DefaultSpaceshipService;
import org.xebialabs.assessment.spaceship.application.implementation.RuntimeGameService;
import org.xebialabs.assessment.spaceship.application.implementation.dto.GameDTO;
import org.xebialabs.assessment.spaceship.application.implementation.dto.SalvoDTO;
import org.xebialabs.assessment.spaceship.application.mapper.GameDTOMapper;
import org.xebialabs.assessment.spaceship.application.mapper.GameDTOMapperImpl;
import org.xebialabs.assessment.spaceship.configuration.ApplicationProperties;
import org.xebialabs.assessment.spaceship.domain.model.game.DefaultGameFactory;
import org.xebialabs.assessment.spaceship.domain.model.game.Game;
import org.xebialabs.assessment.spaceship.domain.model.game.GameFactory;
import org.xebialabs.assessment.spaceship.domain.model.game.Opponent;
import org.xebialabs.assessment.spaceship.domain.model.game.ShotResult;
import org.xebialabs.assessment.spaceship.domain.model.game.grid.GridFactory;
import org.xebialabs.assessment.spaceship.domain.model.game.grid.RandomSetGridFactory;
import org.xebialabs.assessment.spaceship.domain.model.spaceship.DefaultSpaceshipFactory;
import org.xebialabs.assessment.spaceship.domain.model.spaceship.Rotation;
import org.xebialabs.assessment.spaceship.domain.model.spaceship.SpaceshipFactory;
import org.xebialabs.assessment.spaceship.domain.repository.GameRepository;
import org.xebialabs.assessment.spaceship.domain.shared.GridPosition;
import org.xebialabs.assessment.spaceship.infrastructure.connector.ProtocolConnector;
import org.xebialabs.assessment.spaceship.infrastructure.generator.parameter.ParametersGenerator;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.when;
import static org.mockito.Mockito.mock;

@RunWith(MockitoJUnitRunner.class)
public class GameServiceTest {

	GridFactory gridFactory;
	SpaceshipFactory spaceshipFactory;

	GameRepository gameRepository;
	GameDTOMapper gameDTOMapper;
	GameFactory gameFactory;

	SpaceshipService spaceshipService;
	OpponentService opponentService;
	GridService gridService;
	SalvoService salvoService;

	ParametersGenerator parametersGenerator;
	ApplicationProperties applicationProperties;

	GameService gameService;
	ProtocolConnector protocolConnector;
	GameStrategyProvider gameStrategyProvider;

	Environment environment;

	@Before
	public void setUp() {
		parametersGenerator = mock(ParametersGenerator.class);
		gameRepository = mock(GameRepository.class);
		gameDTOMapper = new GameDTOMapperImpl();

		gameFactory = new DefaultGameFactory(parametersGenerator);
		opponentService = mock(OpponentService.class);

		spaceshipFactory = new DefaultSpaceshipFactory();
		spaceshipService = new DefaultSpaceshipService(spaceshipFactory);

		gridFactory = new RandomSetGridFactory(spaceshipFactory, parametersGenerator);
		gridService = new DefaultGridService(gridFactory);
		applicationProperties = mock(ApplicationProperties.class);

		environment = mock(Environment.class);

		gameStrategyProvider = mock(GameStrategyProvider.class);

		salvoService = new DefaultSalvoService(spaceshipService, gameStrategyProvider, protocolConnector, applicationProperties);

		gameService = new RuntimeGameService(environment, gameRepository, gameFactory, opponentService, gridService, spaceshipService, salvoService,
				protocolConnector, gameStrategyProvider, applicationProperties);
	}

	private void prepareGame() {
		Opponent opponent = new Opponent();
		opponent.setUsername("opponent-user-id");
		opponent.setFullName("Might Opponent");

		GridPosition wingerPosition = GridPosition.create(0, 0);
		GridPosition anglePosition = GridPosition.create(12, 0);
		GridPosition aClassPosition = GridPosition.create(12, 13);
		GridPosition bClassPosition = GridPosition.create(0, 13);
		GridPosition sClassPosition = GridPosition.create(7, 7);

		given(parametersGenerator.getRotation()).willReturn(Rotation.STRAIGHT);
		given(parametersGenerator.getPosition()).willReturn(bClassPosition, sClassPosition, wingerPosition, aClassPosition, anglePosition);
		when(parametersGenerator.getMyTurn()).thenReturn(false);

		when(opponentService.fetchOrCreateOpponent(any(String.class), any(String.class))).thenReturn(opponent);
		when(gameRepository.save(any(Game.class))).then(org.mockito.AdditionalAnswers.returnsFirstArg());
		ApplicationProperties.LocalUser localUser = new ApplicationProperties.LocalUser();
		localUser.setUserId("mine-user");
		when(applicationProperties.getLocalUser()).thenReturn(localUser);
	}

	@Test
	public void testCreate() {
		prepareGame();
		GameDTO dto = new GameDTO();
		dto.setOpponentFullName("Might Opponent");
		dto.setOpponentId("opponent-user-id");
		dto.setHostname("localhost");
		dto.setPort(5555L);

		GameDTO result = gameService.create(dto);

		Assert.assertEquals(result.getStarting(), "opponent-user-id");
		Assert.assertEquals(result.getOwnFullName(), null);
		Assert.assertEquals(result.getOwnUserId(), "mine-user");
	}

	@Test
	public void testFireOpponentSalvo_firstShot() throws GameFinishedException {
		prepareGame();
		Game game = gameFactory.create("localhost", 8080L, opponentService.fetchOrCreateOpponent("any", "anny"), null, gridService.create(),
				gridService.createEmptyGrid());
		when(gameRepository.findGameByUuid(any(String.class))).thenReturn(Optional.of(game));
		when(gameStrategyProvider.calculateSalvoSize(any(Game.class))).thenReturn(5);
		when(gameStrategyProvider.calculateUserTurn(any(Game.class), any(Integer.class))).thenReturn(true);

		Set<GridPosition> positions = new HashSet<>();

		positions.add(GridPosition.create(0, 0));
		positions.add(GridPosition.create(12, 0));
		positions.add(GridPosition.create(13, 13));
		positions.add(GridPosition.create(0, 13));
		positions.add(GridPosition.create(8, 7));

		try {
			SalvoDTO dto = gameService.fireOpponentSalvo("any-game", positions);

			Assert.assertEquals(dto.getShotResults().get("0x0"), ShotResult.hit.toString());
			Assert.assertEquals(dto.getShotResults().get("Cx0"), ShotResult.hit.toString());
			Assert.assertEquals(dto.getShotResults().get("DxD"), ShotResult.hit.toString());
			Assert.assertEquals(dto.getShotResults().get("0xD"), ShotResult.hit.toString());
			Assert.assertEquals(dto.getShotResults().get("8x7"), ShotResult.hit.toString());
			Assert.assertEquals(dto.getPlayerTurn(), "mine-user");
			Assert.assertNull(dto.getWon());
		}
		catch (IllegalMoveByOpponentException | GameInstanceNotFound e) {
			Assert.fail("Not expected error on this test");
		}
	}

	@Test(expected = IllegalArgumentException.class)
	public void testFireOpponentSalvo_repeatedShot() throws IllegalMoveByOpponentException, GameInstanceNotFound, GameFinishedException {
		prepareGame();
		Game game = gameFactory.create("localhost", 8080L, opponentService.fetchOrCreateOpponent("any", "anny"), null, gridService.create(),
				gridService.createEmptyGrid());
		when(gameRepository.findGameByUuid(any(String.class))).thenReturn(Optional.of(game));
		when(gameStrategyProvider.calculateSalvoSize(any(Game.class))).thenReturn(5);

		Set<GridPosition> positions = new HashSet<>();

		positions.add(GridPosition.create(0, 0));
		positions.add(GridPosition.create(12, 0));
		positions.add(GridPosition.create(0, 13));
		positions.add(GridPosition.create(0, 13));
		positions.add(GridPosition.create(8, 7));

		SalvoDTO dto = gameService.fireOpponentSalvo("any-game", positions);
		Assert.fail("Not expected error to be here");
	}

	@Test(expected = GameInstanceNotFound.class)
	public void testFireOpponentSalvo_noGameInstance() throws IllegalMoveByOpponentException, GameInstanceNotFound, GameFinishedException {
		prepareGame();
		Game game = gameFactory.create("localhost", 8080L, opponentService.fetchOrCreateOpponent("any", "anny"), null, gridService.create(),
				gridService.createEmptyGrid());
		game.setMyTurn(true);
		when(gameRepository.findGameByUuid(any(String.class))).thenReturn(Optional.empty());

		Set<GridPosition> positions = new HashSet<>();

		positions.add(GridPosition.create(0, 0));
		positions.add(GridPosition.create(12, 0));
		positions.add(GridPosition.create(13, 13));
		positions.add(GridPosition.create(0, 13));
		positions.add(GridPosition.create(8, 7));

		SalvoDTO dto = gameService.fireOpponentSalvo("any-game", positions);
		Assert.fail("Not expected error to be here");
	}

	@Test(expected = IllegalMoveByOpponentException.class)
	public void testFireOpponentSalvo_notPlayersTurn() throws IllegalMoveByOpponentException, GameInstanceNotFound, GameFinishedException {
		prepareGame();
		Game game = gameFactory.create("localhost", 8080L, opponentService.fetchOrCreateOpponent("any", "anny"), null, gridService.create(),
				gridService.createEmptyGrid());
		game.setMyTurn(true);
		when(gameRepository.findGameByUuid(any(String.class))).thenReturn(Optional.of(game));

		Set<GridPosition> positions = new HashSet<>();

		positions.add(GridPosition.create(0, 0));
		positions.add(GridPosition.create(12, 0));
		positions.add(GridPosition.create(13, 13));
		positions.add(GridPosition.create(0, 13));
		positions.add(GridPosition.create(8, 7));

		SalvoDTO dto = gameService.fireOpponentSalvo("any-game", positions);
		Assert.fail("Not expected error to be here");
	}

	@Test(expected = GameFinishedException.class)
	public void testFireOpponentSalvo_gameFinished() throws IllegalMoveByOpponentException, GameInstanceNotFound, GameFinishedException {
		prepareGame();
		Game game = gameFactory.create("localhost", 8080L, opponentService.fetchOrCreateOpponent("any", "anny"), null, gridService.create(),
				gridService.createEmptyGrid());
		game.setWinner("someone");
		when(gameRepository.findGameByUuid(any(String.class))).thenReturn(Optional.of(game));

		Set<GridPosition> positions = new HashSet<>();

		positions.add(GridPosition.create(0, 0));
		positions.add(GridPosition.create(12, 0));
		positions.add(GridPosition.create(13, 13));
		positions.add(GridPosition.create(0, 13));
		positions.add(GridPosition.create(8, 7));

		SalvoDTO dto = gameService.fireOpponentSalvo("any-game", positions);
		Assert.fail("Not expected error to be here");
	}

}
