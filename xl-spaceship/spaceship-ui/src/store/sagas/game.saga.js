import { call, put, select } from 'redux-saga/effects';

import {
  createGameFailed,
  createGameFailedOnRemote,
  displayGame,
  displayGameFailed,
  displayingGame,
  resetCreateForm,
  setGameLocation
} from '../actions/game.actions';
import { initGameList } from '../actions/game-list.actions';
import {
  createGame,
  fetchGameData,
  getGameRuleSet,
  shootSalvo,
  turnAutopilotOn
} from '../services/game.service';

const getGameLocation = state => state.game.gameLocation;

export function* createGameSaga(action) {
  try {
    const command = {
      spaceship_protocol: {
        hostname: action.game.hostname,
        port: action.game.port
      },
      rules: action.game.rule
    };
    const createGameResponse = yield call(createGame, command);
    yield put(resetCreateForm());
    yield put(setGameLocation(createGameResponse.gameLocation));
    const gameLocation = yield select(getGameLocation);
    const ruleSetResponse = yield call(getGameRuleSet, gameLocation);
    yield put(
      displayingGame({
        gameDetails: createGameResponse.data,
        ruleSet: ruleSetResponse
      })
    );
    yield put(initGameList());
  } catch (error) {
    if (error.response && error.response.status === 400) {
      yield put(
        createGameFailedOnRemote(
          'Error calling remote ' +
            action.game.hostname +
            ':' +
            action.game.port
        )
      );
    } else {
      yield put(createGameFailed(error.message));
    }
  }
}

export function* shootSelectedSalvoSaga(action) {
  try {
    yield call(shootSalvo, action.salvo.gameId, action.salvo.selectedSalvo);
    yield put(displayGame(action.salvo.gameId));
  } catch (error) {
    yield put(displayGameFailed('Shoot salvo error: ' + error.message));
  }
}

export function* turnAutopilotOnSaga(action) {
  try {
    yield call(turnAutopilotOn, action.gameId);
    yield put(displayGame(action.gameId));
  } catch (error) {
    yield put(displayGameFailed('Turn autopilot on: ' + error.message));
  }
}

export function* displayGameSaga(action) {
  try {
    let payload = {
      gameDetails: null,
      ruleSet: null
    };
    const response = yield call(fetchGameData, action.gameLocation);
    payload.gameDetails = response;
    const ruleSetResponse = yield call(getGameRuleSet, action.gameLocation);
    payload.ruleSet = ruleSetResponse;
    yield put(displayingGame(payload));
  } catch (error) {
    yield put(displayGameFailed('Error fetching game: ' + error.message));
  }
}
