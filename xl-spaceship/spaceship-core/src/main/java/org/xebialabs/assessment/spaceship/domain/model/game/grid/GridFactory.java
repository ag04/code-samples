package org.xebialabs.assessment.spaceship.domain.model.game.grid;

public interface GridFactory {

	Grid create();

	default Grid createEmptyGrid() {
		return Grid.createEmptyGrid();
	}
}
