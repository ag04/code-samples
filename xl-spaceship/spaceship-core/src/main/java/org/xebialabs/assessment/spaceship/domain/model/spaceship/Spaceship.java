package org.xebialabs.assessment.spaceship.domain.model.spaceship;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;
import org.xebialabs.assessment.spaceship.domain.model.game.grid.Grid;
import org.xebialabs.assessment.spaceship.domain.shared.Coordinate;
import org.xebialabs.assessment.spaceship.domain.shared.GridPosition;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "spaceship")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "spaceship_type", discriminatorType = DiscriminatorType.STRING)
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public abstract class Spaceship implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	private SpaceshipType type;

	@NotNull
	@Type(type = "org.xebialabs.assessment.spaceship.util.hbm.JsonStringType")
	@Column(name = "position", columnDefinition = "clob")
	private Coordinate position;

	@Column(name = "rotation", length = 20, nullable = false)
	@Enumerated(EnumType.STRING)
	private Rotation rotation;

	@NotNull
	@Type(type = "org.xebialabs.assessment.spaceship.util.hbm.JsonStringType")
	@Column(name = "damaged_positions", columnDefinition = "clob")
	private Set<GridPosition> damagedPositions;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "grid_id", referencedColumnName = "id")
	private Grid grid;

	@Column(name = "dead")
	private boolean dead;

	protected Spaceship() { }

	protected Spaceship(SpaceshipType type) {
		this.type = type;
	}

	static Spaceship create(@NotNull SpaceshipType type, Rotation rotation, GridPosition position) {
		Spaceship spaceship;
		switch (type) {
		case WINGER:
			spaceship = new Winger();
			break;
		case ANGLE:
			spaceship = new Angle();
			break;
		case A_CLASS:
			spaceship = new AClass();
			break;
		case B_CLASS:
			spaceship = new BClass();
			break;
		case S_CLASS:
			spaceship = new SClass();
			break;
		default:
			throw new IllegalArgumentException("SpacheshipType cannot be null or undefined");
		}
		spaceship.rotation = rotation;
		spaceship.position = Coordinate.fromGridPoistion(position);
		spaceship.damagedPositions = new HashSet<>();
		return spaceship;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public SpaceshipType getType() {
		return type;
	}

	public Coordinate getPosition() {
		return position;
	}

	public void setPosition(Coordinate position) {
		this.position = position;
	}

	public Rotation getRotation() {
		return rotation;
	}

	public void setRotation(Rotation rotation) {
		this.rotation = rotation;
	}

	public Set<GridPosition> getDamagedPositions() {
		return damagedPositions;
	}

	public void setDamagedPositions(Set<GridPosition> damagedPositions) {
		this.damagedPositions = damagedPositions;
	}

	public Grid getGrid() {
		return grid;
	}

	public void setGrid(Grid grid) {
		this.grid = grid;
	}

	public boolean isDead() {
		return dead;
	}

	public void setDead(boolean dead) {
		this.dead = dead;
	}

	public void addDamagedPositions(GridPosition shot) {
		if (this.damagedPositions == null) {
			this.damagedPositions = new HashSet<>();
		}
		this.damagedPositions.add(shot);
	}
}
