import * as actionTypes from './actionTypes';

export const initGameList = () => {
  return {
    type: actionTypes.INIT_GAME_LIST
  };
};

export const setGameList = gameList => {
  return {
    type: actionTypes.SET_GAME_LIST,
    games: gameList
  };
};

export const initGameListFailed = () => {
  return {
    type: actionTypes.INIT_GAME_LIST_FAILED
  };
};
