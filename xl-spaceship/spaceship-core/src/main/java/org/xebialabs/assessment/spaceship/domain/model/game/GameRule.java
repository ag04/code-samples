package org.xebialabs.assessment.spaceship.domain.model.game;

public enum GameRule {

	/***
	 standard : Each player is allowed to shoot a salvo of shots equalling the number of spaceships
	            he controls that have not yet been destroyed.
	 X-shot : A player is allowed a salvo of X shots, where X is a number between 1
				and 10 . There is no deduction for lost ships. For instance 4-shot
	 super-charge : If you destroy a ship this turn, get awarded another salvo of shots.
	 desperation : Start with 1 shot salvos and for each of your ships destroyed, you are
				allowed an extra shot per salvo.
	***/
	standard, Xshot, super_charge, desperation
}
