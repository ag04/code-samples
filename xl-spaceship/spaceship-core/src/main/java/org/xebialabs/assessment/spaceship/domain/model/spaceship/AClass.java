package org.xebialabs.assessment.spaceship.domain.model.spaceship;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("A_CLASS")
public class AClass extends Spaceship {

	public AClass() {
		super(SpaceshipType.A_CLASS);
	}

}
