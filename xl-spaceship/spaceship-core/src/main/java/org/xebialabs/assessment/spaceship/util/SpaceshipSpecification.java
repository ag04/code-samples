package org.xebialabs.assessment.spaceship.util;

import org.springframework.data.util.Pair;
import org.xebialabs.assessment.spaceship.domain.model.spaceship.SpaceshipType;
import org.xebialabs.assessment.spaceship.domain.shared.GridPosition;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public final class SpaceshipSpecification {

	private static Map<SpaceshipType, Set<GridPosition>> specificationRegister;
	private static Map<SpaceshipType, Pair<Integer, Integer>> dimensionsRegister;

	static {
		specificationRegister = new HashMap<>();
		dimensionsRegister = new HashMap<>();
		specificationRegister.put(SpaceshipType.WINGER, createWinger());
		dimensionsRegister.put(SpaceshipType.WINGER, Pair.of(4, 2));
		specificationRegister.put(SpaceshipType.ANGLE, createAngle());
		dimensionsRegister.put(SpaceshipType.ANGLE, Pair.of(3, 2));
		specificationRegister.put(SpaceshipType.A_CLASS, createAClass());
		dimensionsRegister.put(SpaceshipType.A_CLASS, Pair.of(3, 2));
		specificationRegister.put(SpaceshipType.B_CLASS, createBClass());
		dimensionsRegister.put(SpaceshipType.B_CLASS, Pair.of(4, 2));
		specificationRegister.put(SpaceshipType.S_CLASS, createSClass());
		dimensionsRegister.put(SpaceshipType.S_CLASS, Pair.of(4, 3));
	}

	private SpaceshipSpecification() {
	}

	private static Set<GridPosition> createSClass() {
		Set<GridPosition> positions = new HashSet<>();
		positions.add(GridPosition.create(0, 1));
		positions.add(GridPosition.create(0, 2));
		positions.add(GridPosition.create(1, 0));
		positions.add(GridPosition.create(2, 1));
		positions.add(GridPosition.create(2, 2));
		positions.add(GridPosition.create(3, 3));
		positions.add(GridPosition.create(4, 1));
		positions.add(GridPosition.create(4, 2));
		return positions;
	}

	private static Set<GridPosition> createBClass() {
		Set<GridPosition> positions = new HashSet<>();
		positions.add(GridPosition.create(0, 0));
		positions.add(GridPosition.create(0, 1));
		positions.add(GridPosition.create(1, 0));
		positions.add(GridPosition.create(1, 2));
		positions.add(GridPosition.create(2, 0));
		positions.add(GridPosition.create(2, 1));
		positions.add(GridPosition.create(3, 0));
		positions.add(GridPosition.create(3, 2));
		positions.add(GridPosition.create(4, 0));
		positions.add(GridPosition.create(4, 1));
		return positions;

	}

	private static Set<GridPosition> createAClass() {
		Set<GridPosition> positions = new HashSet<>();
		positions.add(GridPosition.create(0, 1));
		positions.add(GridPosition.create(1, 0));
		positions.add(GridPosition.create(1, 2));
		positions.add(GridPosition.create(2, 0));
		positions.add(GridPosition.create(2, 1));
		positions.add(GridPosition.create(2, 2));
		positions.add(GridPosition.create(3, 0));
		positions.add(GridPosition.create(3, 2));
		return positions;

	}

	private static Set<GridPosition> createWinger() {
		Set<GridPosition> positions = new HashSet<>();
		positions.add(GridPosition.create(0, 0));
		positions.add(GridPosition.create(0, 2));
		positions.add(GridPosition.create(1, 0));
		positions.add(GridPosition.create(1, 2));
		positions.add(GridPosition.create(2, 1));
		positions.add(GridPosition.create(3, 0));
		positions.add(GridPosition.create(3, 2));
		positions.add(GridPosition.create(4, 0));
		positions.add(GridPosition.create(4, 2));
		return positions;
	}

	private static Set<GridPosition> createAngle() {
		Set<GridPosition> positions = new HashSet<>();
		positions.add(GridPosition.create(0, 0));
		positions.add(GridPosition.create(1, 0));
		positions.add(GridPosition.create(2, 0));
		positions.add(GridPosition.create(3, 0));
		positions.add(GridPosition.create(3, 1));
		positions.add(GridPosition.create(3, 2));
		return positions;
	}

	public static Set<GridPosition> getSpaceshipSpecification(SpaceshipType type) {
		return specificationRegister.get(type);
	}

	public static Pair<Integer, Integer> getSpaceshipDimensions(SpaceshipType type) {
		return dimensionsRegister.get(type);
	}

}
