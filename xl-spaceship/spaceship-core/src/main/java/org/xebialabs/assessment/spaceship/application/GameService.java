package org.xebialabs.assessment.spaceship.application;

import org.xebialabs.assessment.spaceship.application.implementation.dto.GameDTO;
import org.xebialabs.assessment.spaceship.application.implementation.dto.SalvoDTO;
import org.xebialabs.assessment.spaceship.domain.shared.GridPosition;
import org.xebialabs.assessment.spaceship.infrastructure.GameProtocolError;
import org.xebialabs.assessment.spaceship.infrastructure.command.RuleSetResult;

import java.util.List;
import java.util.Set;

public interface GameService {

	GameDTO create(GameDTO command);

	SalvoDTO fireOpponentSalvo(String gameId, Set<GridPosition> positions)
			throws IllegalMoveByOpponentException, GameInstanceNotFound, GameFinishedException;

	SalvoDTO fireUserSalvo(String gameId, Set<GridPosition> positions)
			throws IllegalMoveByOpponentException, GameInstanceNotFound, GameFinishedException;

	GameDTO getGameState(String gameId) throws GameInstanceNotFound;

	String createUserGame(GameDTO gameDTO) throws GameProtocolError;

	List<String> getGameIds();

	RuleSetResult getGameRuleSet(String gameId) throws GameInstanceNotFound;
}
