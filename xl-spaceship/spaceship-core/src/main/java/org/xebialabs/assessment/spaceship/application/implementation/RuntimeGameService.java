package org.xebialabs.assessment.spaceship.application.implementation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.xebialabs.assessment.spaceship.application.GameFinishedException;
import org.xebialabs.assessment.spaceship.application.GameInstanceNotFound;
import org.xebialabs.assessment.spaceship.application.GameService;
import org.xebialabs.assessment.spaceship.application.GameStrategyProvider;
import org.xebialabs.assessment.spaceship.application.GridService;
import org.xebialabs.assessment.spaceship.application.IllegalMoveByOpponentException;
import org.xebialabs.assessment.spaceship.application.OpponentService;
import org.xebialabs.assessment.spaceship.application.SalvoService;
import org.xebialabs.assessment.spaceship.application.SpaceshipService;
import org.xebialabs.assessment.spaceship.application.implementation.dto.FireResultDTO;
import org.xebialabs.assessment.spaceship.application.implementation.dto.GameDTO;
import org.xebialabs.assessment.spaceship.application.implementation.dto.SalvoDTO;
import org.xebialabs.assessment.spaceship.application.validator.GameStateValidator;
import org.xebialabs.assessment.spaceship.configuration.ApplicationProperties;
import org.xebialabs.assessment.spaceship.domain.model.game.Game;
import org.xebialabs.assessment.spaceship.domain.model.game.GameFactory;
import org.xebialabs.assessment.spaceship.domain.model.game.Opponent;
import org.xebialabs.assessment.spaceship.domain.model.game.grid.Grid;
import org.xebialabs.assessment.spaceship.domain.repository.GameRepository;
import org.xebialabs.assessment.spaceship.domain.shared.GridPosition;
import org.xebialabs.assessment.spaceship.infrastructure.GameProtocolError;
import org.xebialabs.assessment.spaceship.infrastructure.command.RuleSetResult;
import org.xebialabs.assessment.spaceship.infrastructure.connector.ProtocolConnector;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

@Component
public class RuntimeGameService implements GameService {

	private static final Logger LOGGER = LoggerFactory.getLogger(RuntimeGameService.class);

	private final Environment environment;
	private final GameRepository gameRepository;
	private final GameFactory gameFactory;

	private final OpponentService opponentService;
	private final GridService gridService;
	private final SpaceshipService spaceshipService;
	private final SalvoService salvoService;

	private final ProtocolConnector protocolConnector;
	private final GameStrategyProvider gameStrategyProvider;

	private final ApplicationProperties applicationProperties;

	private final GameStateValidator gameStateValidator;

	private final Map<String, Lock> salvoLock = new ConcurrentHashMap<>();

	public RuntimeGameService(Environment environment, GameRepository gameRepository, GameFactory gameFactory, OpponentService opponentService,
			GridService gridService, SpaceshipService spaceshipService, SalvoService salvoService, ProtocolConnector protocolConnector, GameStrategyProvider gameStrategyProvider, ApplicationProperties applicationProperties) {
		this.environment = environment;
		this.gameRepository = gameRepository;
		this.spaceshipService = spaceshipService;
		this.gameFactory = gameFactory;
		this.opponentService = opponentService;
		this.gridService = gridService;
		this.salvoService = salvoService;
		this.protocolConnector = protocolConnector;
		this.gameStrategyProvider = gameStrategyProvider;
		this.applicationProperties = applicationProperties;
		gameStateValidator = new GameStateValidator();
	}

	@Override
	public GameDTO create(GameDTO gameDTO) {
		Grid myGrid = gridService.create();
		Grid opponentGrid = gridService.createEmptyGrid();

		Opponent opponent = opponentService.fetchOrCreateOpponent(gameDTO.getOpponentId(), gameDTO.getOpponentFullName());

		Game game = gameFactory.create(gameDTO.getHostname(), gameDTO.getPort(), opponent, gameDTO.getGameRule(), myGrid, opponentGrid);
		game = gameRepository.save(game);

		String userTurn;

		if (game.isMyTurn()) {
			userTurn = applicationProperties.getLocalUser().getUserId();
		}
		else {
			userTurn = opponent.getUsername();
		}

		gameDTO.setOwnFullName(applicationProperties.getLocalUser().getUsername());
		gameDTO.setOwnUserId(applicationProperties.getLocalUser().getUserId());

		return gameDTO.createdGameDto(game.getUuid(), userTurn, gameDTO.getGameRule());
	}

	@Override
	public String createUserGame(GameDTO gameDTO) throws GameProtocolError {
		Grid myGrid = gridService.create();
		Grid opponentGrid = gridService.createEmptyGrid();

		String remoteHostname = gameDTO.getHostname();
		Long remotePort = gameDTO.getPort();

		gameDTO.setOpponentId(applicationProperties.getLocalUser().getUserId());
		gameDTO.setOpponentFullName(applicationProperties.getLocalUser().getUsername());
		gameDTO.setHostname(applicationProperties.getLocalUser().getHostname());
		gameDTO.setPort(Long.valueOf(environment.getProperty("local.server.port")));
		gameDTO = protocolConnector.createGame(remoteHostname, remotePort, gameDTO);

		Opponent opponent = opponentService.fetchOrCreateOpponent(gameDTO.getOpponentId(), gameDTO.getOpponentFullName());

		Game game = gameFactory.create(remoteHostname, remotePort, opponent, gameDTO.getGameRule(), myGrid, opponentGrid);
		// override gameId and user turn
		game.setUuid(gameDTO.getGameId());
		if (gameDTO.getStarting() != null && gameDTO.getStarting().equals(applicationProperties.getLocalUser().getUserId())) {
			game.setMyTurn(true);
		}
		else {
			// override it
			game.setMyTurn(false);
		}
		game = gameRepository.save(game);

		return game.getUuid();
	}

	@Override
	public List<String> getGameIds() {
		List<String> gameIds = new ArrayList<>();

		gameRepository.findAll().forEach(game -> gameIds.add(game.getUuid()));

		return gameIds;
	}

	@Override
	public RuleSetResult getGameRuleSet(String gameId) throws GameInstanceNotFound {
		Optional<Game> gameOptional = gameRepository.findGameByUuid(gameId);
		Game game = gameOptional.orElseThrow(GameInstanceNotFound::new);

		RuleSetResult result = new RuleSetResult();
		result.setGameId(gameId);
		result.setSalvoSize(gameStrategyProvider.calculateSalvoSize(game));
		result.setRules(game.getGameRule().name());
		result.setAutopilotOn(game.isOnAutopilot());
		return result;
	}

	@Override
	@Transactional
	public SalvoDTO fireOpponentSalvo(String gameId, Set<GridPosition> salvo)
			throws IllegalMoveByOpponentException, GameInstanceNotFound, GameFinishedException {
		// first we shoot each spaceship on the grid
		// spaceship are in the
		Optional<Game> gameOptional = gameRepository.findGameByUuid(gameId);
		Game game = gameOptional.orElseThrow(GameInstanceNotFound::new);

		Lock lock = salvoLock.computeIfAbsent(gameId, l -> new ReentrantLock());

		boolean finished = false;
		try {
			lock.lock();
			try {
				validateGameFinished(game, salvo);
			}
			catch (GameFinishedException e) {
				finished = true;
				throw e;
			}
			gameStateValidator.validate(game);

			FireResultDTO fireResult = salvoService.fireOpponentSalvo(game, salvo);
			finished = fireResult.isFinished();
			game = gameRepository.save(fireResult.getGame());

			return fireResult.getSalvoDTO();
		}
		finally {
			if (finished) {
				salvoLock.remove(lock);
			}
			lock.unlock();
		}
	}

	@Override
	public SalvoDTO fireUserSalvo(String gameId, Set<GridPosition> salvo)
			throws IllegalMoveByOpponentException, GameInstanceNotFound, GameFinishedException {

		Lock lock = salvoLock.computeIfAbsent(gameId, l -> new ReentrantLock());
		boolean finished = false;
		try {
			lock.lock();
			Optional<Game> gameOptional = gameRepository.findGameByUuid(gameId);
			Game game = gameOptional.orElseThrow(GameInstanceNotFound::new);

			try {
				validateGameFinished(game, salvo);
			}
			catch (GameFinishedException e) {
				finished = true;
				throw e;
			}

			if (!game.isMyTurn()) {
				throw new IllegalMoveByOpponentException();
			}

			FireResultDTO fireResultDTO = salvoService.fireUserSalvo(game, salvo);

			finished = fireResultDTO.isFinished();
			gameRepository.save(fireResultDTO.getGame());

			return fireResultDTO.getSalvoDTO();
		}
		finally {
			if (finished) {
				salvoLock.remove(lock);
			}
			lock.unlock();
		}
	}

	@Override
	public GameDTO getGameState(String gameId) throws GameInstanceNotFound {
		Optional<Game> gameOptional = gameRepository.findGameByUuid(gameId);
		Game game = gameOptional.orElseThrow(GameInstanceNotFound::new);

		GameDTO gameDTO = new GameDTO();
		gameDTO.setOpponentId(game.getOpponent().getUsername());
		gameDTO.setOwnUserId(applicationProperties.getLocalUser().getUserId());
		gameDTO.setOwnBoardState(game.getMyGrid().getShotResults());

		Set<GridPosition> startshipPositions =
				game.getMyGrid().getSpaceships().stream().map(spaceshipService::getSpaceshipFootprint)
								.flatMap(Collection::stream).collect(Collectors.toSet());

		gameDTO.setStarshipPositions(startshipPositions);
		gameDTO.setOpponentBoardState(game.getOpponentGrid().getShotResults());

		if (game.getWinner() != null) {
			gameDTO.setWon(game.getWinner());
		}
		else if (game.isMyTurn()) {
			gameDTO.setPlayerTurn(applicationProperties.getLocalUser().getUserId());
		}
		else {
			gameDTO.setPlayerTurn(game.getOpponent().getUsername());
		}
		return gameDTO;
	}

	private void validateGameFinished(Game game, Set<GridPosition> salvo) throws GameFinishedException {
		if (game.getWinner() != null) {
			Map<String, String> salvoResult = new HashMap<>();
			salvo.stream().map(GridPosition::toString).forEach(coordinate -> {
						salvoResult.put(coordinate, "miss");
					});

			throw new GameFinishedException(salvoService.createSalvoResponse(game, salvoResult));
		}
	}


}
