package org.xebialabs.assessment.spaceship.domain.model.game.grid;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;
import org.xebialabs.assessment.spaceship.domain.model.spaceship.Spaceship;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "grid")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Grid implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@OneToMany(fetch = FetchType.EAGER, cascade = {CascadeType.ALL})
	@JoinColumn(name = "grid_id")
	private Set<Spaceship> spaceships;

	@NotNull
	@Type(type = "org.xebialabs.assessment.spaceship.util.hbm.JsonStringType")
	@Column(name = "shot_results", columnDefinition = "clob")
	private Map<String, String> shotResults;

	protected Grid() { }

	static Grid createEmptyGrid() {
		Grid grid = new Grid();
		grid.shotResults = new HashMap<>();
		grid.setSpaceships(new HashSet<>());
		return grid;
	}

	static Grid createGrid(Set<Spaceship> spaceships) {
		Grid grid = createEmptyGrid();
		grid.setSpaceships(spaceships);
		return grid;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setSpaceships(Set<Spaceship> spaceships) {
		this.spaceships = spaceships;
	}

	public Set<Spaceship> getSpaceships() {
		return spaceships;
	}

	public Map<String, String> getShotResults() {
		return shotResults;
	}

	public void setShotResults(Map<String, String> shotResults) {
		this.shotResults = shotResults;
	}

	public void addShotPositions(Map<String, String> shotPositionResults) {
		if (this.shotResults == null) {
			this.shotResults = new HashMap<>();
		}
		shotPositionResults.forEach((key, value) -> this.shotResults.put(key, value));
	}
}
