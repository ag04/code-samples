package org.xebialabs.assessment.spaceship.interfaces.protocol;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.xebialabs.assessment.spaceship.application.GameFinishedException;
import org.xebialabs.assessment.spaceship.application.GameInstanceNotFound;
import org.xebialabs.assessment.spaceship.application.GameService;
import org.xebialabs.assessment.spaceship.application.IllegalMoveByOpponentException;
import org.xebialabs.assessment.spaceship.application.implementation.dto.GameDTO;
import org.xebialabs.assessment.spaceship.application.implementation.dto.SalvoDTO;
import org.xebialabs.assessment.spaceship.application.mapper.GameDTOMapper;
import org.xebialabs.assessment.spaceship.application.mapper.GameDTOMapperImpl;
import org.xebialabs.assessment.spaceship.application.mapper.SalvoMapper;
import org.xebialabs.assessment.spaceship.infrastructure.LocalProtocolResourceProxy;
import org.xebialabs.assessment.spaceship.infrastructure.ProtocolResourceProxy;
import org.xebialabs.assessment.spaceship.util.ApplicationConstants;

import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.when;
import static org.mockito.Mockito.mock;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { GameDTOMapperImpl.class, SalvoMapper.class })
@WebAppConfiguration
public class ProtocolResourceTest {
	public static final MediaType APPLICATION_JSON_UTF8 = new MediaType(MediaType.APPLICATION_JSON.getType(), MediaType.APPLICATION_JSON.getSubtype(),
			Charset.forName("utf8"));

	MockMvc mockMvc;

	GameService gameService;

	@Autowired
	GameDTOMapper gameDTOMapper;

	@Autowired
	SalvoMapper salvoMapper;

	ProtocolResourceProxy protocolResourceProxy;
	ProtocolResource protocolResource;

	@Before
	public void setUp() throws Exception {

		gameService = mock(GameService.class);
		protocolResourceProxy = new LocalProtocolResourceProxy(gameDTOMapper, salvoMapper, gameService);

		protocolResource = new ProtocolResource(protocolResourceProxy);
		this.mockMvc = MockMvcBuilders.standaloneSetup(protocolResource).build();
	}

	@Test
	public void createGame_created() throws Exception {

		GameDTO gameDTO = new GameDTO();
		gameDTO.createdGameDto("uuid", "mmartian", "standard");
		gameDTO.setOwnUserId("zeko");
		gameDTO.setOwnFullName("Zekoslav Mrkva");
		when(gameService.create(any(GameDTO.class))).thenReturn(gameDTO);
		mockMvc.perform(post(ApplicationConstants.PATH_PREFIX + "/protocol/game/new").contentType(APPLICATION_JSON_UTF8).content(
				"{\n" + "\t\"user_id\": \"xebialabs-1\",\n" + "\t\"full_name\": \"XebiaLabs Opponent\",\n"
						+ "\t\"spaceship_protocol\": {\n\t\t\"hostname\": \"127.0.0.1\",\n" + "\t\t\"port\": 9001\n" + "\t}\n" + "}"))
				.andExpect(status().isCreated()).andExpect(content().contentType(APPLICATION_JSON_UTF8)).andExpect(jsonPath("$.game_id", is("uuid")))
				.andExpect(jsonPath("$.starting", is("mmartian"))).andExpect(jsonPath("$.user_id", is("zeko")))
				.andExpect(jsonPath("$.full_name", is("Zekoslav Mrkva")));

	}

	@Test
	public void fireSalvo_aSuccessShot() throws Exception {
		SalvoDTO salvoDTO = new SalvoDTO();
		Map<String, String> shotResults = new HashMap<>();
		shotResults.put("0x0", "hit");
		salvoDTO.setShotResults(shotResults);
		salvoDTO.setPlayerTurn("zeko");
		when(gameService.fireOpponentSalvo(any(String.class), any(Set.class))).thenReturn(salvoDTO);
		mockMvc.perform(put(ApplicationConstants.PATH_PREFIX + "/protocol/game/game-id-goes-here").contentType(APPLICATION_JSON_UTF8)
				.content("{\"salvo\": [\"0x0\"]}")).andExpect(status().isOk()).andExpect(content().contentType(APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("$.salvo.0x0", is("hit"))).andExpect(jsonPath("$.game.player_turn", is("zeko")));
	}

	@Test
	public void fireSalvo_gameFinished() throws Exception {
		SalvoDTO salvoDTO = new SalvoDTO();
		Map<String, String> shotResults = new HashMap<>();
		shotResults.put("0x0", "miss");
		salvoDTO.setShotResults(shotResults);
		salvoDTO.setWon("zeko");
		GameFinishedException exception = new GameFinishedException(salvoDTO);

		when(gameService.fireOpponentSalvo(any(String.class), any(Set.class))).thenThrow(exception);
		mockMvc.perform(put(ApplicationConstants.PATH_PREFIX + "/protocol/game/game-id-goes-here").contentType(APPLICATION_JSON_UTF8)
				.content("{\"salvo\": [\"0x0\"]}")).andExpect(status().isNotFound()).andExpect(content().contentType(APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("$.salvo.0x0", is("miss"))).andExpect(jsonPath("$.game.won", is("zeko")));
	}

	@Test
	public void fireSalvo_gameInstanceNotFound() throws Exception {
		GameInstanceNotFound exception = new GameInstanceNotFound();

		when(gameService.fireOpponentSalvo(any(String.class), any(Set.class))).thenThrow(exception);
		mockMvc.perform(put(ApplicationConstants.PATH_PREFIX + "/protocol/game/game-id-goes-here").contentType(APPLICATION_JSON_UTF8)
				.content("{\"salvo\": [\"0x0\"]}")).andExpect(status().isInternalServerError());
	}

	@Test
	public void fireSalvo_illegalMoveByOpponentException() throws Exception {
		IllegalMoveByOpponentException exception = new IllegalMoveByOpponentException();

		when(gameService.fireOpponentSalvo(any(String.class), any(Set.class))).thenThrow(exception);
		mockMvc.perform(put(ApplicationConstants.PATH_PREFIX + "/protocol/game/game-id-goes-here").contentType(APPLICATION_JSON_UTF8)
				.content("{\"salvo\": [\"0x0\"]}")).andExpect(status().isConflict());
	}

	@Test
	public void fireSalvo_illegalArgumentException() throws Exception {
		IllegalArgumentException exception = new IllegalArgumentException();

		when(gameService.fireOpponentSalvo(any(String.class), any(Set.class))).thenThrow(exception);
		mockMvc.perform(put(ApplicationConstants.PATH_PREFIX + "/protocol/game/game-id-goes-here").contentType(APPLICATION_JSON_UTF8)
				.content("{\"salvo\": [\"0x0\"]}")).andExpect(status().isExpectationFailed());
	}
}
