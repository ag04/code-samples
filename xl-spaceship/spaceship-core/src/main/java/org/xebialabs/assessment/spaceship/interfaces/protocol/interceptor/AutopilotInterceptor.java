package org.xebialabs.assessment.spaceship.interfaces.protocol.interceptor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import org.xebialabs.assessment.spaceship.infrastructure.UserResourceProxy;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AutopilotInterceptor  extends HandlerInterceptorAdapter {
	public static final String X_AUTOPILOTED_GAME_ID_HEADER = "X-Autopiloted-Game-ID";

	private static final Logger LOGGER = LoggerFactory.getLogger(AutopilotInterceptor.class);

	private final UserResourceProxy userResourceProxy;

	public AutopilotInterceptor(UserResourceProxy userResourceProxy) {
		this.userResourceProxy = userResourceProxy;
	}

	@Override
	@Async
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception exception) throws Exception {
		String gameId = response.getHeader(X_AUTOPILOTED_GAME_ID_HEADER);
		if (exception == null && gameId != null) {
			LOGGER.debug("Triggering autopilot game");
			userResourceProxy.triggerAutopilotSalvo(gameId);
		}
	}
}
